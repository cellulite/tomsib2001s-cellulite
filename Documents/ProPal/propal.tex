\documentclass{article}
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=3.6cm]{geometry}
\usepackage{tikz}
\usepackage{verbatim}


\usetikzlibrary{shapes}
\usepackage{amsmath}
\usepackage{xspace}

\title{Proposition de Projet Intégré:\\Grain de Cell}
\author{Thomas Sibut-Pinote, Vincent Cohen-Addad, Ann Johansson, Lucca
  Hirschi,\\Antoine Plet et Guillaume Lagarde}
\begin{document}
\maketitle

\begin{abstract}
Notre projet est un simulateur d'organismes vivant dans un univers persistant. Ces organismes sont composés de briques de bases, les cellules, qui possèdent un ADN.

Les utilisateurs ont accès à une interface de création d'organismes, sur laquelle ils peuvent programmer leur ADN dans un langage simple mais expressif. Ensuite, ils peuvent ``injecter'' leurs créations dans l'univers, et observer leur évolution vis-à-vis du milieu et des autres espèces, sans possibilité d'interagir avec elles.
\end{abstract}

\tableofcontents

\section{Description de l'objectif}

\subsection{La simulation}
Le terrain est une grille 2D dans lequel les cellules occupent certaines cases. La simulation se déroule sur un serveur qui calcule les modifications à apporter au terrain tour après tour, selon les règles prédéfinies et les actions induites par l'ADN de chaque cellule. À tout moment, un utilisateur peut introduire une nouvelle cellule qui est alors placée aléatoirement sur le terrain. 

La quantité déterminante de la simulation est l'énérgie. Chaque cellule en possède, en reçoit ou en dépense une certaine quantité. Elle en a besoin pour survivre et pour effectuer toute action. Pendant un tour, une cellule peut :
\begin{itemize}
\item envoyer un message qui se propage autour d'elle (le coût est fonction du nombre de bits et de la distance de propagation);
\item envoyer de l'énérgie qui se propage autour d'elle;
\item se dupliquer pour créer une nouvelle cellule héritant d'une partie ---\, déterminée par la mère \,--- de l'ADN et de l'énérgie (le coût est fonction de la taille de l'ADN copié et de l'énérgie injectée dans la nouvelle cellule);
\item savoir si une cellule occupe une case adjacente.
\end{itemize}
Un cellule entourée de trop d'enérgie de celulles adverses meurt.
Nous voudrions atteindre une cadence de l'ordre de 10 tours par seconde pour pouvoir observer raisonnablement l'évolution en temps réel.


\subsection{Langage}
L'ADN des cellules est écrit dans un langage haut niveau et relativement simple. A chaque tour, la cellule exécute son ADN.
Une cellule a à sa disposition des variables (sa ``mémoire''), qui sont de deux types:
\begin{itemize}
\item des paramètres de la simulation (temps de vie restant, énergie stockée, nombre de génération depuis la cellule souche);
\item des variables librement manipulables pour le stockage et l'affectation (un compteur servant à temporiser par exemple).
\end{itemize}
Le langage est basée sur des branchements \textit{Si $cond$ alors $op$ sinon $op$} avec $cond$ une condition pouvant impliquer les messages reçus pendant le tour précédent, la mémoire de la cellule (les variables).

\subsection{Interface}

Le serveur supportant la simulation est doublement interfacé avec l'utilisateur:
\begin{itemize}
\item l'utilisateur peut visualiser le terrain \og en temps réel\fg{}
  grâce à un client graphique;
\item l'utilisateur peut coder l'ADN d'une cellule est l'injecter
  dans le terrain à l'aide d'un client graphique ou en ligne de
  commande (dans un premier temps).
\end{itemize}

\paragraph{Interface pour la simulation}
\label{sec:interface-pour-la}
Un client graphique permet de visualiser le terrain avec les cellules
et leur auteur (en les colorants par exemple) ainsi que l'énérgie et les messages
qui transitent.

\paragraph{Interface pour la création d'ADN}
\label{sec:interface-pour-la-1}
La modularité doit être une composante importante de cette
interface. L'utilisateur doit pouvoir utiliser du code déjà écrit pour
concevoir de l'ADN de plus en plus complexe.

Dans un premier temps, un client en ligne de commande permet de
rentrer le code textuellement et de l'envoyer au serveur. On peut
imaginer ensuite, une aide à la rédaction d'ADN basée sur des menus
déroulants et l'ouverture et l'enregistrement de bouts d'ADN.

\section{Intérêt scientifique}

Le but de ce projet est pédagogique : il doit permettre aux utilisateurs de comprendre comment un code ADN identique pour toutes les cellules peut permettre de fabriquer un organisme complexe, capable de survivre et de se perpétuer dans le temps. En manipulant les instructions et en changeant les arbres de décision, ils pourront voir comment les comportements évoluent.

Il est habituel de voir dans ce but des simulations basées sur des mutations aléatoires et des croisements. Même si ce type d'opération pourrait être intégré ultérieurement à notre projet, ce n'est pas le but principal. Le but est de "mettre les mains dans le cambouis", pas juste d'observer.
Dans l'optique de la pédagogie, nous nous efforcerons de rendre l'interface de codage des cellules la plus intuitive possible, une fois le code "bas niveau" établi et compilé.

\section{Tâches à effectuer}
Nous avons répartit le travail en 7 \textit{work-packages} différents
de taille comparables. La section~\ref{sec:cal} définira les objectifs en
terme de calendrier.

\subsection{Description technique}
\label{sec:descr-techn}

Le serveur fait tourner plusieurs routines:
\begin{itemize}
\item un environnement qui calcule la simulation pas à pas en exploitant les codes d'ADN compilés
au niveau utilisateur et écrit un flot de modifications apportées au
terrain dans un dépot public du serveur (accessible en lecture seule de l'extérieur);
\item le compilateur qui compile les codes d'ADN
déposés par des utilisateurs dans un dépôt public du serveur
(accessible en écriture seule de l'extérieur) en un code
niveau interpréteur;
\item un programme qui authentifie les nouveaux utilisateurs.
\end{itemize}
Le client se connecte au serveur (plus précisément aux dossiers
publics du serveur), envoie le code d'une nouvelle cellule de
l'utilisateur sur le dépôt des codes de cellules du serveur et copie
localement le flot de transformations du terrain. Le client peut alors
afficher le terrain avec ce flot et un état initial (donné par le
serveur à une fréquence faible).

\subsection{Les \textit{work-packages}}
\label{sec:les-text-pack}

\paragraph{Théorique}
\label{sec:theorie}
Nous devons définir les spécifications du langage codant l'ADN (le
langage niveau utilisateur) ainsi que le langage niveau interpréteur
(exploité par l'environnement). Nous devons également définir la forme
que prendra le flot de modifications du terrain.

\paragraph{Environnement}
\label{sec:environnement}
Nous écrivons ici la routine qui calcule la simulation et qui écrit le
flot de modifications.


\paragraph{Compilation}
\label{sec:compilation}
Dans ce \textit{work-package}, il faut écrire un compilateur du
langage niveau utilisateur vers le langage niveau-interpréteur. Il est
important de préserver la structure de ``sous ADN'' utile à la
duplication avec spécialisation de la cellule.

\paragraph{Connexions réseaux}
\label{sec:connexions-reseaux}
Concernant les connexions réseaux, il faut fixer un protocle de
communication entre serveur et client (SSH, SCP, etc.), écrire le code
necéssaire pour la connexion au serveur, l'écriture et la lecture de
fichiers publics. Ensuite, il faut instaurer un système
d'authentification: il faut pouvoir se créer un compte et
s'authentifier depuis le client.
Il est important de vérifier que le système est ``à peu près
sûr''. Par exemple: on ne doit pas pouvoir lire le code de l'ADN de
cellules ennemies.

\paragraph{Interface}
\label{sec:interface}
L'interface s'appuie sur le travail effectué sur les connexions réseaux. Il faut prévoir:
\begin{itemize}
\item un programme qui permet d'envoyer un code d'ADN au serveur
  (suivant l'ambition: un envoi simple de fichier texte ou une aide à
  la saisie);
\item un programme qui pour un flot de modifications et un état
  initial accessibles localement affiche l'état du terrain pas à pas
  (ici aussi, l'affichage peut se limiter à du simple ``ASCII Art''
  mais peut aussi être intégré à une interface graphique avec zoom etc.).
\end{itemize}


\paragraph{Communication}
\label{sec:com}
Il faut demander un emplacement sur l'infrastructure de l'ENS pour le
serveur ainsi qu'un nom de domaine. Déployer un site WEB pour décrire
et promouvoir le projet et proposer aux $\beta$-testeurs les sources du client. Il
faut également écrire le ``mode d'emploi'' (règles et spécifications
du langage niveau utilisateur).

\paragraph{$\beta$-test}
\label{sec:beta-test}
Ce travail consiste à re-équilibrer le jeu en le testant. Il faut par
exemple fixer les constantes des règles du jeu. Bien sûr, n'importe
qui peut participer à ce travail.


\subsection{Dépendances}
\label{sec:dependances}
Nous représentons les dépendances entre les différents
\textit{work-packages} dans la figure~\ref{fig:dep}.

\begin{figure}[H]
  \centering
  \begin{minipage}[H]{200pt}
    \input{dep}
  \end{minipage}
  \caption{Dépendances des \textit{work-packages}}
  \label{fig:dep}
\end{figure}


\section{Calendrier}
\label{sec:cal}

Nous avons réparti la force de travail sur tous les
\textit{work-packages} en respectant les dépendances dans le diagramme
de la figure~\ref{fig:calen}. 


\begin{figure}[H]
  \centering
\includegraphics[width=20cm]{cal.pdf}
\vspace{-260pt}
  \caption{Diagramme de Gantt ($S_i$ représente la $i^{\textrm{ème}}$ semaine)}
  \label{fig:calen}
\end{figure}

On peut résumer quelques \textit{deadlines} importantes:
\begin{enumerate}
\item dès le début du projet, nous mettons en place un dépot GIT (sur Gitorious), nous
  demandons un serveur et un nom de domaine;
\item à la fin de la semaine 2, dans le \textit{work-package}
  connexions réseaux (Antoine et Vincent), il ne reste que la partie
  authentification à faire et le \textit{work-package} Théorique
  (Thomas, Lucca et Guillaume) est achevé;
\item à la fin de la semaine 4, l'environnement, la compilation et les
  connexions réseaux fonctionnent tous dans une version allégée (tout
  n'est pas encore integré --\,uniquement les déplacements par exemple\,-- mais les interfaces entre programmes fonctionnent);
\item à la fin de la semaine 6, nous devons être capable de fournir
  une simulation, une compilation et les fonctions réseaux (sans l'authentification)
  fonctionnels avec toutes les actions possibles (il ne reste plus que
  l'authentification et les interfaces utilisateurs);
\item à la fin de la semaine 8, si l'interface pose des problèmes,
  nous nous contentons d'une interface en ASCII art. De la semaine 8 à
  10, 2 personnes se mettent à re-travailler sur l'environnement pour
  intégrer les fonctions réseaux;
\item à la fin de la semaine 10, tout doit fonctionner côté
  programmes. Il ne reste plus qu'a équilibrer les règles: nous
  rentrons tous dans une phase de $\beta$-test.
\item à la fin de la semaine 16, nous pouvons rendre la version finale.
\end{enumerate}
\end{document}
