#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "config_comm_s.h"
#define ACK_SIZE 7
#define LONGUEUR 1024
#define FILE_COMP "compile_report.txt"

int main (int argc, char *argv[]){
    
//     struct hostent *hostinfo = NULL;
    struct sockaddr_in addr;
    int  nb_lu; // adresse du serveur
    char buffer[TAILLE_BUFF] = "";
    char buffer_send[TAILLE_BUFF] = "";
    char buffer_read[LONGUEUR];
    int stream;
    int sum=0;
    int sock = socket(PF_INET, SOCK_STREAM, 0);
    fd_set ens_rec;
    struct stat fstat;
/*    struct timeval timeout; //quelle valeur mettre ??
    timeout.tv_sec = 10; //secondes
    timeout.tv_usec = 0; //microsecondes*/

    if(argc != 4)
    {
	fprintf(stderr, "Usage : %s [nom_du_fichier]\n", argv[0]);
	exit(-1);
    }
    if(lstat(argv[1], &fstat)){
	printf("No file\n");
	close(sock);
	exit(-1);
    }
    
    memset(& addr, 0, sizeof(struct sockaddr_in));
    addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
    addr.sin_port = htons(PORT_SERVEUR);
    addr.sin_family = AF_INET;
    
    if(connect(sock,(struct sockaddr *) &addr, sizeof(struct sockaddr))){
	fprintf (stderr, "Connexion failed.\n");
	exit(-1);
    }

    FD_ZERO(&ens_rec);
    FD_SET(sock, &ens_rec);
    
    // Argument 1 : chemin du fichier à envoyer
    stream = open(argv[1], O_RDONLY);
    
    //fprintf(stderr, "--> %s -- %s\n", argv[2], argv[3]);    

//    nb_wr = write(sock, buffer, nb_lu);

    do{
	buffer_read[0] = '\0';
	nb_lu = read(stream, buffer_read, LONGUEUR);
	buffer_read[nb_lu] = '\0';
	fprintf(stderr, "dans le while %s\n", buffer_read);
	fflush(stderr);
	strcat(buffer_send, buffer_read);
	if(nb_lu < 0)
	    return -1;
	sum+=nb_lu;
//	fprintf(stderr,"je pass dans le while\n");
    }while(nb_lu == LONGUEUR);

    
    close(stream);  
    fprintf(stderr, "%d caracteres sent\n", sum);
    sprintf(buffer, "adding %s %s\n%d\n%s", argv[2], argv[3], sum, buffer_send);
    write(sock, buffer, strlen(buffer));
    
//    sleep(2);
/*
=======
    sleep(2);

>>>>>>> 6f1df71b82f76a3a0b45b470b359402bc037db78
    if((err = select(sock + 1, &ens_rec, NULL, NULL, &timeout)) <= 0){
	fprintf(stderr, "Erreur dans l'envoi (acceptation) : %d --> \n", err);
	exit(EXIT_FAILURE);
    }
    if(!FD_ISSET(sock, &ens_rec)){
	fprintf(stderr, "On reçoit de quelqu'un d'autre ???\n");
	exit(EXIT_FAILURE);
    }

    length = read(sock, buffer, ACK_SIZE);
    if(length != ACK_SIZE){
	fprintf(stderr, "Sending failed.\n");
	exit(EXIT_FAILURE);
  stream = open(FILE_COMP, O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
    
    nb_lu = read(sock, buffer, LONGUEUR);
    if(!strcmp(buffer, "COMPILE SUCCESSFULL\n")){
	close(stream);
	close(sock);
	fprintf(stderr,"Compile successfull\n");
	return 0;
    }
    nb_wr = write(stream, buffer, nb_lu);
    
    while(nb_lu == LONGUEUR){
	nb_lu = read(sock, buffer, LONGUEUR);
	if(nb_lu < 0)
	    return -1;
	nb_wr = write(stream, buffer, nb_lu);
	if(nb_wr != nb_lu)
	    return -1;
	sum+=nb_lu;
//	fprintf(stderr,"je pass dans le while\n");
    }
*/    

    close(stream);
    close(sock);
    //  fprintf(stderr, "Compile failed\n");
    return 0; 
}
