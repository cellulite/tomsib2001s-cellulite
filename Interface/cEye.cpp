#include <iostream>
#include <cmath>
#include <SDL/SDL.h>
#if defined(__linux__)
	#include <GL/gl.h>
	#include <GL/glu.h>
#elif defined(__APPLE__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#error "OS non supportée\n"
#endif
#include "cEye.h"
using namespace std;

#define _POSX -5.0
#define _POSY -5.0
#define _POSZ 12.0
#define _DISTANCE sqrt((_POSX * _POSX) + (_POSY * _POSY) + (_POSZ * _POSZ))
#define _BASE_ANGLE_X 0.78539816 // pi/4

cEye::cEye()
: _position_sensibility(0.02)
, _angle_sensibility(0.1)
, _distance_sensibility(0.2)
, _centerX(0.0)
, _centerY(0.0)
, _posX(_POSX)
, _posY(_POSY)
, _posZ(_POSZ)
, _hold_left(false)
, _hold_right(false)
, _distance(_DISTANCE)
, _angleZ(0.0)
, _angleX(0.0)
{
}

cEye::cEye(int mapX, int mapY)
: _position_sensibility(0.02)
, _angle_sensibility(0.1)
, _distance_sensibility(0.2)
, _centerX(0.0)
, _centerY(0.0)
, _posX(_POSX)
, _posY(_POSY)
, _posZ(_POSZ)
, _hold_left(false)
, _hold_right(false)
, _distance(_DISTANCE)
, _angleZ(0.0)
, _angleX(0.0)
, _mapX(mapX)
, _mapY(mapY)
{
}

cEye::~cEye()
{
}

cEye& cEye::look()
{
	gluLookAt(_posX, _posY, _posZ, _centerX, _centerY, _centerZ, _upX, _upY, _upZ);

	glTranslated(_centerX, _centerY, _centerZ);
	glRotated(_angleZ, -1, 1, 0);
	glRotated(_angleX, 0, 0, 1);
	glTranslated(-1 * _centerX, -1 * _centerY, -1 * _centerZ);
	return *this;
}

cEye& cEye::on_mouse_motion(const SDL_MouseMotionEvent &event)
{
	if(_hold_left)
	{
		double cosX = cos(_BASE_ANGLE_X * (1 + _angleX / 45)), sinX = sin(_BASE_ANGLE_X * (1 + _angleX / 45));
		double depX = (-1 * cosX * event.xrel + sinX * event.yrel) * _position_sensibility;
		double depY = (sinX * event.xrel + cosX * event.yrel) * _position_sensibility;
//		double depX = -1 * event.xrel * _position_sensibility;
//		double depY = event.yrel * _position_sensibility;
		double oldX = _centerX, oldY = _centerY;
		_centerX = _centerX + depX;
		if(_centerX > 3 * _mapX / 2)
			_centerX = 3 * _mapX / 2;
		if(_centerX < -(_mapX / 2)) 
			_centerX = -(_mapX / 2);
		_centerY = _centerY + depY;
		if(_centerY > 3 * _mapY / 2) 
			_centerY = 3 * _mapY / 2;
		if(_centerY < -(_mapY / 2)) 
			_centerY = -(_mapY / 2);
		_posX = _centerX + (_posX - oldX);
		_posY = _centerY + (_posY - oldY);
	}
	else if(_hold_right)
	{
		_angleX = _angleX + event.xrel * _angle_sensibility;
		while(_angleX > 180)
			_angleX -= 360;
		while(_angleX < -180)
			_angleX += 360;
		_angleZ = _angleZ - event.yrel * _angle_sensibility;
		if(_angleZ < -30)
			_angleZ = -30;
		if(_angleZ > 50)
			_angleZ = 50;
	}
	return *this;
}

cEye& cEye::on_mouse_button(const SDL_MouseButtonEvent &event)
{
	switch(event.button)
	{
		case SDL_BUTTON_LEFT:
			if(_hold_left && event.type == SDL_MOUSEBUTTONUP)
			{
				_hold_left = false;
			}
			else if (!_hold_left && event.type == SDL_MOUSEBUTTONDOWN)
			{
				_hold_left = true;
			}
			break;
		case SDL_BUTTON_RIGHT:
			if(_hold_right && event.type == SDL_MOUSEBUTTONUP)
			{
				_hold_right = false;
			}
			else if (!_hold_right && event.type == SDL_MOUSEBUTTONDOWN)
			{
				_hold_right = true;
			}
			break;
		case SDL_BUTTON_WHEELUP:
		{
			double tmpX = (_posX - _centerX) / _distance;
			double tmpY = (_posY - _centerY) / _distance;
			double tmpZ = (_posZ - _centerZ) / _distance;
			_distance += _distance_sensibility;
			if(_distance > 50)
				_distance = 50;
			_posX = _centerX + _distance * tmpX;
			_posY = _centerY + _distance * tmpY;
			_posZ = _centerZ + _distance * tmpZ;
			break;
		}
		case SDL_BUTTON_WHEELDOWN:
		{
			double tmpX = (_posX - _centerX) / _distance;
			double tmpY = (_posY - _centerY) / _distance;
			double tmpZ = (_posZ - _centerZ) / _distance;
			_distance -= _distance_sensibility;
			if(_distance < 1)
				_distance = 1;
			_posX = _centerX + _distance * tmpX;
			_posY = _centerY + _distance * tmpY;
			_posZ = _centerZ + _distance * tmpZ;
			break;
		}
		default:
			break;
	}
	return *this;
}

cEye& cEye::on_keyboard(const SDL_KeyboardEvent &event)
{
	_posX = _POSX;
	_posY = _POSY;
	_posZ = _POSZ;
	_centerX = 0.0;
	_centerY = 0.0;
	_distance = _DISTANCE;
	_angleX = 0.0;
	_angleZ = 0.0;
	_hold_left = false;
	_hold_right = false;
	return *this;
}
