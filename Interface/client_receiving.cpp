#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "interface_client.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
#include <pthread.h>
#include <SDL/SDL.h>
#include <pthread.h>
#include "cRenderer.h"

using namespace std;

static int nb_cells = -1;
static int nb_cells2 = -1;
static int (*cells)[5] = NULL;
static int (*cells2)[5] = NULL;
static char *login;
static char *pass;  

static pthread_mutex_t 	mutex_aff;
static pthread_mutex_t 	mutex_mourrant;
static pthread_mutex_t 	mutex_cells;
static pthread_mutex_t 	mutex_depart;
static pthread_mutex_t 	mutex_aff2;
static pthread_cond_t 	cond_depart;
static pthread_cond_t 	cond_aff;
static pthread_cond_t 	cond_aff2;
static pthread_cond_t 	cond_mourrant;
static pthread_t id ;

static void init_mutex(){
    pthread_mutex_init(&mutex_cells, NULL);
    pthread_mutex_init(&mutex_aff, NULL);
    pthread_mutex_init(&mutex_aff2, NULL);
    pthread_mutex_init(&mutex_mourrant, NULL);
    pthread_mutex_init(&mutex_depart, NULL);
    pthread_cond_init(&cond_depart, NULL);
    pthread_cond_init(&cond_aff, NULL);
    pthread_cond_init(&cond_aff2, NULL);
    pthread_cond_init(&cond_mourrant, NULL);
}

static void destroy_mutex(){
    pthread_mutex_destroy(&mutex_cells);
    pthread_mutex_destroy(&mutex_aff);
    pthread_mutex_destroy(&mutex_aff2);
    pthread_mutex_destroy(&mutex_mourrant);
    pthread_mutex_destroy(&mutex_depart);
    pthread_cond_destroy(&cond_depart);
    pthread_cond_destroy(&cond_aff);
    pthread_cond_destroy(&cond_aff2);
    pthread_cond_destroy(&cond_mourrant);
}

/*
*/


void load_array(){
//	delete [] cells;
    cells = cells2;
//	cells2 = NULL;
    nb_cells = nb_cells2;
}


void *thread_afficheur (void *arg){

    pthread_mutex_lock(&mutex_depart);
    // There are some magic cheats there but unremovable!  
    pthread_mutex_unlock(&mutex_depart);
    pthread_cond_broadcast(&cond_depart);
    SDL_Event event;
    cRenderer *render = new cRenderer(800, 600);
    render->set_user(*((int*)arg));
    render->set_mapX(30);
    render->set_mapY(30);
    bool stop = false;

    while(!stop) {
	// Updating Map
	//fprintf(stderr, "Nouveau tour d'update\n");
	fflush(stderr);
	for(int i = 0 ; i < 20 && SDL_PollEvent(&event) ; ++i)
	{
	    switch(event.type)
	    {
	    case SDL_QUIT:
		stop = true;
		break;
	    case SDL_MOUSEMOTION:
		render->get_mouse_motion(event.motion);
		break;
	    case SDL_MOUSEBUTTONUP:
	    case SDL_MOUSEBUTTONDOWN:
		render->get_mouse_button(event.button);
		break;
	    case SDL_KEYDOWN:
		switch(event.key.keysym.sym)
		{
		case SDLK_ESCAPE:
		    stop = true;
		    break;
		default:
		    render->get_keyboard(event.key);
		    break;
		}
		break;
	    }
	}

//       	pthread_mutex_lock(&mutex_aff);
	//cerr << "affichage locking" << endl;
	pthread_mutex_lock(&mutex_aff);
	
	//cerr << "Mutex 1" << endl;
	if(nb_cells != -1){
	    render->update(cells, nb_cells);
	    delete [] cells;
	}
	pthread_cond_wait(&cond_aff, &mutex_aff);
	
	pthread_mutex_unlock(&mutex_aff);

	//cerr << "affichage signal" << endl;
//	pthread_mutex_unlock(&mutex_aff);    
	//cerr << "affichage unlocking" << endl;
    }
    pthread_cond_broadcast(&cond_mourrant);
    return NULL;
}




void *thread_recepteur(void *arg){
    pthread_mutex_lock(&mutex_depart);
    pthread_create(&id, NULL, thread_afficheur, arg);
    //cerr << "Lecture va attendre" << endl;
    pthread_cond_wait(&cond_depart, &mutex_depart);
    //cerr << "Lecture n'attends plus" << endl;
    pthread_mutex_unlock(&mutex_depart); 
    nb_cells = recoit_carte(&cells);
    pthread_cond_broadcast(&cond_aff);

    //cerr << "lecture a signalé" << endl;
   
    while(1){
//	fprintf(stderr, "Nouveau tour de lecture\n");
//	pthread_mutex_lock(&mutex_aff2);

	nb_cells2 = recoit_carte(&cells2);	    
	//cerr << "lecture locking" << endl;
//	pthread_mutex_unlock(&mutex_aff2);
	//cerr << "lecture signalin" << endl;
	
	pthread_mutex_lock(&mutex_aff);

	load_array();

	pthread_cond_broadcast(&cond_aff);
	pthread_mutex_unlock(&mutex_aff);

	//cerr << "lecture unlocking" << endl;

    }
    pthread_cond_broadcast(&cond_mourrant);
    return NULL;
}




int main(int argc, char **argv){
    init_mutex();
    int return_value;

    if(argc != 3)
	return 1;
    cerr << "connecting...";

    login = new char[strlen(argv[1])];
    pass = new char[strlen(argv[2])];
    strcpy(login, argv[1]);
    strcpy(pass, argv[2]);
    return_value = connecte_serveur(argv[1], argv[2]);
    
    if(return_value <= 0)
    {
	fprintf(stderr, "Veuillez nous excuser pour l'indisponibilité du serveur. Vous pouvez vous reporter à notre site internet pour plus d'informations.\n");
	fprintf(stderr, "failedconnect\n");
	return return_value;
    }
    fprintf(stdout, "%d\n", return_value); // on informe python que tout va bien     
    fflush(stdout);
    cerr << "connected!\n";
//    pthread_mutex_lock(&mutex_mourrant);
    pthread_t id2;
    pthread_create(&id2, NULL, thread_recepteur, &return_value);
  
    fprintf(stderr, "main waiting\n");
    sleep(3);
    pthread_cond_wait(&cond_mourrant, &mutex_mourrant);
    fprintf(stderr, "main killing\n");    
    pthread_cancel(id);
    pthread_cancel(id2);

//    pthread_mutex_unlock(&mutex_mourrant);
    destroy_mutex();
    deconnecte_serveur();
    return 0;
}
