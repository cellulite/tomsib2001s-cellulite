#include <cstdlib>
#include <iostream>
#include <cmath>
#include <SDL/SDL.h>
#if defined(__linux__)
	#include <GL/gl.h>
	#include <GL/glu.h>
#elif defined(__APPLE__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#error "OS non supportée\n"
#endif
#include "cRenderer.h"
#include "cEye.h"
#include "ellipsoide/cEllipsoid.h"
using namespace std;

bool cRenderer::_init = false;

static int _init_sdl()
{
	if(!(cRenderer::_init))
	{
		//Initialisation de la SDL
		if(SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			return 1;
		}
		//Assure le mode double buffer
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_WM_SetCaption("GrainDeCell",NULL);
		cRenderer::_init = true;
	}
	return 0;
}

cRenderer::cRenderer(int width, int heigth)
: _user (-1)
, _width (width)
, _heigth (heigth)
{
//	cout << "Nouveau Renderer\n";
	_init_sdl();
	_screen = SDL_SetVideoMode(_width, _heigth, 32, SDL_OPENGL);
	if(!_screen)
	{
		cerr << "Echec de la création de la fenêtre : " << SDL_GetError() << endl;
		exit(EXIT_FAILURE);
	}
	//Activation de la profondeur
	glEnable(GL_DEPTH_TEST);
	//Projection:
	fovy = 70.0;
	aspect = (double)_width / heigth;
	zNear = 0.05;
	zFar = 40.0;
	//Point de vue
	_eye = new cEye ();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fovy, aspect, zNear, zFar);
}

cRenderer::~cRenderer()
{
	delete _eye;
	SDL_Quit();
}

void cRenderer::update(int cells[][5], int nb_cells)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	_eye->look();

	dessin(cells, nb_cells);

	glFlush();
	SDL_GL_SwapBuffers();
}

cRenderer& cRenderer::get_mouse_motion(SDL_MouseMotionEvent &event)
{
	_eye->on_mouse_motion(event);
	return *this;
}

cRenderer& cRenderer::get_mouse_button(SDL_MouseButtonEvent &event)
{
	_eye->on_mouse_button(event);
	return *this;
}

cRenderer& cRenderer::get_keyboard(SDL_KeyboardEvent &event)
{
	_eye->on_keyboard(event);
	return *this;
}

void cRenderer::dessin_limites(double z_min, double z_max)
{
	glVertex3f(0.5, 0.5, z_min);
	glVertex3f(0.5, _mapY + 0.5, z_min);
	glVertex3f(0.5, _mapY + 0.5, z_max);
	glVertex3f(0.5, 0.5, z_max);

	glVertex3f(0.5, _mapY + 0.5, z_min);
	glVertex3f(_mapX + 0.5, _mapY + 0.5, z_min);
	glVertex3f(_mapX + 0.5, _mapY + 0.5, z_max);
	glVertex3f(0.5, _mapY + 0.5, z_max);

	glVertex3f(_mapX + 0.5, _mapY + 0.5, z_min);
	glVertex3f(_mapX + 0.5, 0.5, z_min);
	glVertex3f(_mapX + 0.5, 0.5, z_max);
	glVertex3f(_mapX + 0.5, _mapY + 0.5, z_max);

	glVertex3f(_mapX + 0.5, 0.5, z_min);
	glVertex3f(0.5, 0.5, z_min);
	glVertex3f(0.5, 0.5, z_max);
	glVertex3f(_mapX + 0.5, 0.5, z_max);
}

void cRenderer::dessin(int cells[][5], int nb_cells)
{
	glBegin(GL_QUADS);

//	glColor4ub(89, 228, 114, 5);
//	glColor3ub(107, 169, 118);
	glColor3ub(0, 100, 0);
	glVertex3i(-(_mapX / 2 + 1), -(_mapY / 2 + 1), 0);
	glVertex3i(-(_mapX / 2 + 1), 3 * _mapY / 2, 0);
	glVertex3i(3 * _mapX / 2, 3 * _mapY / 2, 0);
	glVertex3i(3 * _mapX / 2, -(_mapY / 2 + 1), 0);

	glColor3ub(59, 25, 0);
	dessin_limites(0.25, 0.4);

	glEnd();

	int owner, id, x, y, nrj;
	double c;

	for(int i = 0 ; i < nb_cells ; ++i)
	{
		owner = cells[i][0];
		id = cells[i][1];
		nrj = cells[i][2];
		x = cells[i][3];
		y = cells[i][4];
		c = 0.5 + (double)nrj / 250.0;
		cEllipsoid cell (_rad, _rad, c);
		cell.generate(15, 5);
		if(owner == _user)
			glColor3ub((20 * id) % 256, (20 * id) % 256, 255);
		else
			glColor3ub(225, 20, 20);
		cell.draw(x - _mapX / 2, y - _mapY / 2, 0.0);
		cell.draw(x + _mapX / 2, y - _mapY / 2, 0.0);
		cell.draw(x + _mapX / 2, y + _mapY / 2, 0.0);
		cell.draw(x - _mapX / 2, y + _mapY / 2, 0.0);
//		cell.draw(x, y, 0.0);

/*		dessin_cell(x, y, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
		if(x < (_mapX / 2))
		{
			dessin_cell(x + _mapX, y, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
			if(y < (_mapY / 2))
			{
				dessin_cell(x + _mapX, y + _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
				dessin_cell(x, y + _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
			}
			else
			{
				dessin_cell(x + _mapX, y - _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
				dessin_cell(x, y - _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
			}
		}
		else
		{
			dessin_cell(x - _mapX, y, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
			if(y < (_mapY / 2))
			{
				dessin_cell(x - _mapX, y + _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
				dessin_cell(x, y + _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
			}
			else
			{
				dessin_cell(x - _mapX, y - _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
				dessin_cell(x, y - _mapY, z, rad, (57 * owner) % 256, 250, (10 * id) % 256);
			}
		}
*/	}
}

void cRenderer::dessin_cell(double x, double y, double z, double rad, int r, int v, int b)
{
	glPushMatrix();
	GLUquadric* params = gluNewQuadric();
//	gluQuadricTexture(params, GL_TRUE);
//	glBindTexture(GL_TEXTURE_2D, earth);
	gluQuadricDrawStyle(params, GLU_FILL);
	glTranslated(x, y, z);
	glColor3ub(r, b, v);
	gluSphere(params, rad, 8, 8);
	glPopMatrix();
}

