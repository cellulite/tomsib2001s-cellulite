#include <iostream>
#include <queue>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>


#include "cRenderer.h"
#include "environnement/env.h"
#include "environnement/Log.h"

#define FILE_RES "compile_report_BaS.txt"

using namespace std;

Log log_er;
fstream filestr;

Env environnement;

/* _nouvelles_cellules contient les noms des librairies dynamiques 
 * des cellules à ajouter.
 * entourée du mutex _mutex_cellules
 */
static queue < string > _nouvelles_cellules; 
/* _clients_connectes contient les clients "connectés".
 * entourée par _mutex_connectes
 */
//static queue < Client* > _clients_connectes; 
/* _go est à true si les données sont prêtes à être envoyées.
 * entouré de _mutex_go
 */
static volatile bool _go;
/* compte le nombre de clients qui sont encore en cours de traitement
 * entouré de _mutex_occupes
 */
static volatile unsigned int _occupes;

/* pour les cout << ...
 */
static pthread_mutex_t 	mutex_aff, mutex_depart;

/* _cond_go signale aux threads émetteurs que les données sont prêtes.
 * _cond_stop signale au serveur que les données ont été envoyées.
 */
static pthread_cond_t cond_depart, cond_aff;
static int go_read = 1;

static void _initialisation()
{
    //initialisation des mutex et des conditions
    pthread_mutex_init(&mutex_aff, NULL);
    pthread_mutex_init(&mutex_depart, NULL);
    pthread_cond_init(&cond_depart, NULL);
    pthread_cond_init(&cond_aff, NULL);
}

static void _finalisation()
{
    //destruction des mutex et des conditions
    pthread_mutex_destroy(&mutex_aff);
    pthread_mutex_destroy(&mutex_depart);
    pthread_cond_destroy(&cond_depart);
    pthread_cond_destroy(&cond_aff);
}

/* Données chargées du .config :
 * _nb_emetteurs contient le nombre de threads émetteurs lancés.
 * _port_acceptation contient le numéro du port sur lequel les 
 * nouveaux clients se connectent pour recevoir la carte.
 * _port_reception contient le numéro du port sur lequel les codes
 * des nouvelles cellules sont reçues.
 * _delta_t est le nombre de tours que l'environnement fait entre
 * chaque émission de données
 */

static int nb_cells = -1, _delta_t;
static int (*cells)[5] = NULL;

static string _adresse;

void update(string s_){
    stringstream to_parse;
    string temp;
    to_parse << s_;
    to_parse >> nb_cells;
    to_parse >> temp;
//    cerr << "--->"<< nb_cells << "<---"<< temp << "----" << endl;
    cells = new int[nb_cells][5];
    for(int i = 0; i<nb_cells; i++){
	to_parse >> cells[i][0] >> cells[i][1] >> cells[i][2] >> cells[i][3] >> cells[i][4];
//	cerr << cells[i][0] << " " << cells[i][1] << " " << cells[i][2] << " " << cells[i][3] << " " << cells[i][4]<< endl;
    }
}


void *thread_afficheur (void *arg){
    // There are some magic cheats there but unremovable!  
    pthread_mutex_lock(&mutex_depart);
    SDL_Event event;
    cRenderer *render = new cRenderer(800, 600);
    
    render->set_user(2);
    render->set_mapX(50);
    render->set_mapY(50);
    bool stop = false;
    bool begin = true;

    while(!stop) {

	fflush(stderr);
	for(int i = 0 ; i < 20 && SDL_PollEvent(&event) ; ++i)
	{
	    switch(event.type)
	    {
	    case SDL_QUIT:
		go_read = 0;
		stop = true;
		break;
	    case SDL_MOUSEMOTION:
		render->get_mouse_motion(event.motion);
		break;
	    case SDL_MOUSEBUTTONUP:
	    case SDL_MOUSEBUTTONDOWN:
		render->get_mouse_button(event.button);
		break;
	    case SDL_KEYDOWN:
		switch(event.key.keysym.sym)
		{
		case SDLK_ESCAPE:
		    go_read = 0;   
		    stop = true;
		    break;
		default:
		    render->get_keyboard(event.key);
		    break;
		}
		break;
	    }
	}
	pthread_mutex_lock(&mutex_aff);
	if(begin){
	    pthread_cond_broadcast(&cond_depart);
	    pthread_mutex_unlock(&mutex_depart);
	    begin = false;
	}
	

	pthread_cond_wait(&cond_aff, &mutex_aff);
	
	render->update(cells, nb_cells);
	if(nb_cells != -1)
	    delete [] cells;
	
	pthread_mutex_unlock(&mutex_aff);

    }
    return NULL;
}


int main(int argc, char *argv[])
{
    _delta_t = 1;
    if(argc != 2){
	cerr << "Usage :"<< argv[0] << " [File_to_test.ex]" << endl;
	exit(EXIT_FAILURE);
    }

    _initialisation();
    pthread_t *thread_id = new pthread_t [1];

    sigset_t set;
    sigaddset(&set, SIGPIPE);
    sigprocmask(SIG_BLOCK, &set, NULL); //masque le signal qui peut être reçu avec read/write
    
    

    char* filename = (char*)"codes/mon_code.so";
    char* buff = (char*) malloc((1000 + strlen(filename)) * sizeof(char));
    cerr << "compiling to C++...";
    sprintf( buff, "./graindecell %s > codes/code__.cpp", argv[1]);
    cerr << "compiled!" << endl;
    int _t_e = system(buff);
    if(!_t_e)
    {
	sprintf(buff, "g++ -o %s -shared codes/code__.cpp 2> %s", filename, FILE_RES);
	cerr << "inserting...";

	if(system(buff) == 0){
	    cerr << "inserted!" << endl;

	    environnement.add_cell_auto(filename, 1);
	}
	else{
	    cerr << "the C++ code is corrupted, please report the"<< FILE_RES <<"file" << endl;
	    return -1;
	}
    }
    else{
	cerr << "failed to compile to .cpp, please check the file " << FILE_RES << endl;
	sprintf(buff, "cp codes/code__.cpp %s", FILE_RES);
	system(buff);
	return -1;
    }
    pthread_mutex_lock(&mutex_depart);
    pthread_create(thread_id, NULL, thread_afficheur, NULL);
    pthread_cond_wait(&cond_depart, &mutex_depart);
    pthread_mutex_unlock(&mutex_depart);

    while(go_read) {
//	cerr << "boucle while" << endl;
	for(int i = 0 ; i < _delta_t ; ++i)
	{
	    usleep(20000);
	    
	    environnement.calc_tour();	
	
	}
	fflush(stderr);
	stringstream f ;
	f << affiche_terrain(environnement);
//	cerr << "--------" << f.str() << endl << "-----------------"<< endl;
	
	pthread_mutex_lock(&mutex_aff);
	update(f.str());
//	cerr << "waiting here thread recept" << endl;

	pthread_cond_broadcast(&cond_aff);
	pthread_mutex_unlock(&mutex_aff);
    
    }

    _finalisation();
    pthread_exit(EXIT_SUCCESS);
    return 0;
}
