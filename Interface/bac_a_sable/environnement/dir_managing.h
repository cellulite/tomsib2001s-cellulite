#ifndef _DEF_DIR_MAN
#define _DEF_DIR_MAN

#include "direction.h"

bool is_dir(int n)
{
  return (n > 0) && (n < 256/*2^8 */);
}

int NextDir(int n)
{
  int m = (2*n) % 256;
  if (m==0)
    {
      return 1;
    }
  else
    return m;
}

int PrevDir(int n)
{
  if (n==1)
    return 128;
  else
    return n/2;
}

int OpDir(int n)
{
  return (NextDir(NextDir(NextDir(n))));
}



int int_to_dir(int n)
{
	for(int i = 0 ; i < 8 ; ++i)
	{
		if(n % 2)
			return i;
		n /= 2;
	}
	return -1;
}

int dir_to_int(Direction d)
{
	switch(d)
	{
		case N:
			return 1;
		case N_E:
			return 2;
		case E:
			return 4;
		case S_E:
			return 8;
		case S:
			return 16;
		case S_O:
			return 32;
		case O:
			return 64;
		case N_O:
			return 128;
		default:
			//Ne devrait jamais arriver !!!
			return -1;
	}
}

bool* int_to_dir_vect(int n)
{
	n = n % 128;
	if(n <= 0)
		return NULL;
	bool *res = new bool [8];
	for(int i = 0 ; i < 8 ; ++i)
	{
		if(n % 2)
			res[i] = true;
		else
			res[i] = false;
		n /= 2;
	}
	return res;
}

int dir_vect_to_int(bool* d_v)
{
	if(!d_v) // Ne devrait jamais arriver !!!
		return -1;
	int res = 0;
	for(int i = 7 ; i >= 0 ; --i)
	{
		res = 2 * res + (d_v[i] ? 1 : 0);
	}
	delete [] d_v;
	return res;
}

int costSendMessage(Env& env, int dir, Message mess)
{
	int res = 0;
	bool* vectDir = int_to_dir_vect(dir);
	if(vectDir)
	{
		res = env.costSendMessage(vectDir, mess);
		delete [] vectDir;
	}
	return res;
}

int costSendEnergy(Env& env, int dir, int nrj)
{
	int res = 0;
	bool* vectDir = int_to_dir_vect(dir);
	if(vectDir)
	{
		res = env.costSendEnergy(vectDir, nrj);
		delete [] vectDir;
	}
	return res;
}

#endif
