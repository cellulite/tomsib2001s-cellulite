#include <iostream>
#include "position.h"
using namespace std;

int main()
{
	Position pos1(5, 6), pos2(7, 9);
	cout << pos1 << " + " << pos2 << " + " << "N_O = ";
	Position pos3 = pos1 + pos2 + N_O;
	cout << pos3 << endl;
	cout << pos3 << " + N_O + N_O = ";
	(pos3 += N_O) += N_O;
	cout << pos3 << endl;
	return 0;
}
