#include <iostream>
#include <fstream>
#include <cstdlib>
#include <csignal>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/param.h>
#include <cstring>
#include <set>
#include <vector>
#include "cell.h"
#include "Log.h"
using namespace std;


/***********/
/* CONFIG  */
/***********/
//issu d'un .config

// -- l'nrj que perd une cellule (nrj = 0 si elle a même pas cette quantité) quand elle veut fair une action qu'elle ne peut pas payer) -- //
#define perte_nrj_essai_ 2
#define _MAX_NRJ 16384

// -- Biblio. dynamiques -- //
static char lib_so[MAXPATHLEN];
static int __fin_path;


/************/
/* Méthodes */
/************/

// -- Objets pour logger les erreurs déclarés dans serveur.cpp: -- //
extern Log log_er;
extern fstream filestr;


void init_path()		// todo; voir comment faire sous MACOS
{
	getcwd(lib_so, MAXPATHLEN);
	__fin_path = strlen(lib_so);
	lib_so[__fin_path] = '/';
	++__fin_path;
}

Cell::Cell(const char *nom_so_, int nrj_, int proprietaire_, int numero_famille_)
: p_proprietaire 	(proprietaire_)
, p_numero_famille 	(numero_famille_)
, p_numero_individu     (0)	// première cellule de cette famille 
, p_nom_so		(nom_so_)
, p_nrj			(nrj_)
, p_gen			(0)
, p_age			(0)
, p_nrj_buff            (0)
, p_mere                (NULL)	// cette cellule n'a pas de mère !
, p_a_bouge             (0)
, p_nb_mort             (0)
{
	char* error;
	char* nom_main;			   // contiendra le nom de la fonction à la racine
	void (*init_mem)(Memoire&, Cell*); // contiendra la fonction qui initialise la cellule
	//il faut ajouter le nom de la bibliothèque à partir d'ici
	lib_so[__fin_path] = '\0'; 
	log_er << "Path : " << lib_so << " " << p_nom_so << "\n";
	void *handle = dlopen(strcat(lib_so, p_nom_so), RTLD_LAZY);
	if(!handle)
	{
		log_er << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	}
	p_taille_code = *(int*) dlsym(handle, "taille_code");
	
	nom_main = (char*) dlsym(handle, "nom_main");
	if((error =  dlerror()) != NULL)
	  {
	    log_er << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	  }
	p_action = (void(*)(Cell*, Env&, int*, int*, int)) dlsym(handle, nom_main);
	if((error =  dlerror()) != NULL)
	  {
		log_er << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	}
	init_mem = (void(*)(Memoire&, Cell*))dlsym(handle, "mem_init");
	if((error =  dlerror()) != NULL)
	{
		log_er << "Erreur\n" << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	}
	p_tableau_valeurs_args = (int*) dlsym(handle, "tableau_valeurs_args");
	if((error =  dlerror()) != NULL)
	  {
	    log_er << "Erreur dlopen()\n" << dlerror() << "\n";
	    raise(SIGILL);
	  }
	p_tableau_indices_vars = (int*) dlsym(handle, "tableau_indices_vars");
	if((error =  dlerror()) != NULL)
	  {
	    log_er << "Erreur dlopen()\n" << dlerror() << "\n";
	    raise(SIGILL);
	  }
	//	p_debut = (int) dlsym(handle, "debut");
	if((error =  dlerror()) != NULL)
	  {
	    log_er << "Erreur dlopen()\n" << dlerror() << "\n";
	    raise(SIGILL);
	  }

	(*init_mem)(p_mem, NULL);    // on initialise la mémoire
	p_taille_famille = new int(1);  // on a une nouvelle famille de taille 1
}

Cell::Cell(Cell *mother_, const char* racine, int *tableau_valeurs_args, int *tableau_indices_vars, int debut, assign ass, int taille_code_, int nrj_)
: p_proprietaire 	(mother_->p_proprietaire)
, p_numero_famille 	(mother_->p_numero_famille)
, p_numero_individu 	(*(mother_->p_taille_famille))	// on numérote les cellules au sein d'une famille de 0 à (*p_taille_famille) - 1
, p_taille_famille	(mother_->p_taille_famille)
, p_nom_so		(mother_->p_nom_so)
, p_nrj			(nrj_)
, p_gen			(mother_->p_gen + 1)
, p_age			(0)
, p_taille_code		(taille_code_)
, p_nrj_buff            (0)
, p_tableau_valeurs_args (tableau_valeurs_args)
, p_tableau_indices_vars (tableau_indices_vars)
, p_mere                (mother_)
, p_a_bouge             (0)
, p_nb_mort             (0)
{
        char* error;
 	//il faut ajouter le nom de la bibliothèque à partir d'ici
	lib_so[__fin_path] = '\0'; 
	log_er << "Path : " << lib_so << " " << p_nom_so << "\n";
	void *handle = dlopen(strcat(lib_so, p_nom_so), RTLD_LAZY);
	if(!handle)
	{
		log_er << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	}
	p_action = (void(*)(Cell*, Env&, int*, int*, int)) dlsym(handle, racine);
	if((error =  dlerror()) != NULL)
	{
		log_er << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	}
	if((error =  dlerror()) != NULL)
	{
		log_er << "Erreur\n" << "Erreur dlopen()\n" << dlerror() << "\n";
		raise(SIGILL);
	}
	// -- intialisation de la mémoire de la fille -- //
	int nb_vars = mother_->p_mem.nb_vars();  // la taille de la mémoire de la fille = celle de la mère
	p_mem.set_taille(nb_vars);
	// par défaut (sans assignation) mémoire de la fille = INT_MIN (trouver vite une vraie solution)
	for (int i=0; i<nb_vars; i++)
	  {
	    p_mem[i] = INT_MIN; // TRES TRES MOCHE
	  }
	// on applique l'assignation:
	for (int i=0; i<(int)ass.size(); i++)
	  {
	    int rang;
	    rang = ass[i].real();
	    p_mem[rang] = (ass[i]).imag();
	  }
	++(*p_taille_famille);		  // un enfant de plus dans la famille ;)
	(mother_->p_filles).insert(this); // on rajoute cette cellule en tant que fille de sa mère
}

Cell::~Cell()
{
  // -- Supression des tableaux des arguments et mémoire initiale -- //
  //  delete p_tableau_valeurs_args;  TODO : à rétablir quansd la compil ne créera que des tableaux avec new avant les duplications
  //  delete p_tableau_indices_vars;

  // -- Gestion des familles -- //
  --(*p_taille_famille);
  if(*p_taille_famille == 0)	// alors on a plus besoin de la bibliothèque dynamique de cette cellule (fammile décimée)
    {
      //il faut ajouter le nom de la bibliothèque à partir d'ici
      lib_so[__fin_path] = '\0'; 
      void *handle = dlopen(strcat(lib_so, p_nom_so), RTLD_LAZY);
      dlclose(handle);
      delete p_taille_famille;
    }
  if (p_mere)			// si elle a une mère (pas LA cellule souche)
    {
      // la mère n'a plus cette fille
      p_mere->p_filles.erase(this);
    }
  // on va détruire le lien de parenté entre cette cellule et ses filles
  if (p_filles.size() > 0)
    {
      for (int i=0; i<(int)p_filles.size(); i++)
	{
	  fille(i)->init_mere(); // on détruit le lien de parenté (la cellule devient sans mère (comme une cellule souche))
	}
    }
}

int Cell::nrj() const  { return p_nrj; };
int Cell::gen() const { return p_gen; };
int Cell::age() const { return p_age; };
int Cell::taille_code() const {return p_taille_code; };

void Cell::nrj_update () {
  p_nrj += p_nrj_buff;		// l'nrj reçue au tour précédent est ajoutée
  if(p_nrj > _MAX_NRJ) p_nrj = _MAX_NRJ;
  p_nrj_buff = 0;		// l'nrj reçue : y en a plus ;)
}

void Cell::nrj_receive (int quantite) {
  p_nrj_buff += quantite;
};

bool Cell::nrj_load(int nrj_)
{
  if(p_nrj < nrj_) {		// si la cellule n'a pas assez d'nrj
    p_nrj = max(0, p_nrj - perte_nrj_essai_); // on lui enlève perte_nrj_essai_ parce qu'elle n'a pas vérifié avant
    return false;			      // elle n'a pas assez
  }
  else {
    p_nrj -= nrj_;		// on lui soustrait cette quantité
    return true;		// elle a assez
  }
}

int& Cell::mem(int indice_)
{
	return (p_mem[indice_]);
}

void Cell::mess_update_end()
{
	p_buf.update();
}

void Cell::mess_store(const Message msg, Direction dir)
{
	p_buf.store(msg, dir);
}

Message Cell::mess_load(Direction dir)
{
	return p_buf.load(dir);
}

void Cell::action(Env& env)
{
  (*p_action)(this, env, p_tableau_indices_vars, p_tableau_valeurs_args, p_debut); // c'est action avec la mémoire et les arguments de cette cellule
}

void Cell::grow(int E_soleil) {
  p_age ++;			// elle grandit
  p_nrj += E_soleil;		// elle recevra l'nrj du soleil
}

Cell* Cell::mere () {
  return (p_mere);
}

int Cell::nb_filles () {
  return (p_filles.size());
}

void Cell::init_mere() {
  p_mere = NULL;		// plus de mère (pour couper le lien de parenté)
}

Cell* Cell::fille (int i) {	// ATTENTION cette méthode est lente (mais utilisée seulement pour débugger)
  if ((i >= 0) && (i<(int)p_filles.size())) {
    set<Cell*>::iterator iterateur;
    iterateur=p_filles.begin();
    for (int j=0; j<i; iterateur++) j++;
    return *(iterateur);
    // Attention: si une cellule est la 3-eme fille, rajoutée une fille peut la faire passer à 0,1,2,3 etc... pas de contrôle sur l'ordre !!
    // Par exemple le 1er élément peut changer si on rajoute un 76eme élément
    }
  else {		       
    log_er << "Rang interdit dans le tableau des filles d'une cellule. SegFault en vue! Cellule concernée: prop = " << (*this).proprietaire() << " , famille = " << (*this).numero_famille() << "\n"; // super verbeux  :)
    raise(SIGSEGV);
    return NULL;
  }
}

void Cell::tue (int nombre_tuees) {
  p_nb_mort += nombre_tuees;
}

int Cell::a_tue () const { return p_nb_mort; };
int Cell::proprietaire () const { return p_proprietaire; };
int Cell::numero_famille () const { return p_numero_famille; };
int Cell::taille_famille () const { return *(p_taille_famille); };
int Cell::pos_x () const { return p_pos.x(); };
int Cell::pos_y () const { return p_pos.y(); };

const char* Cell::get_nom_so() const { return p_nom_so; };


ostream& operator<< (ostream& output, const Cell& cell)
{
	cout 	<< "// ************************* Cellule ************************* \\\\" << endl
		<< "\tPropriétaire : " << cell.p_proprietaire << endl
		<< "\tFamille : " << cell.p_numero_famille << endl
		<< "\tIndividu : " << cell.p_nom_so << endl
		<< "\tMembres de la famille : " << *(cell.p_taille_famille) << endl
		<< "\tTaille du code : " << cell.p_taille_code << endl
		<< "\tEnergie : " << cell.p_nrj << endl
		<< "\tGénération : " << cell.p_gen << endl
		<< "\tAge : " << cell.p_age << endl
		<< endl
		<< "\tMémoire : " << cell.p_mem << endl
		<< endl
		<< "\tBuffer : " << endl
		<< cell.p_buf
		<< endl
		<< "\tPosition : " << cell.p_pos << endl
		<< endl
		<< "\tPosition : " << cell.p_pos << endl
		<< endl
		<< "\tCellule souche? " << not(cell.p_mere) << endl
		<< endl
		<< "\tNombres de filles: " << (cell.p_filles.size()) << endl
		<< endl
		<< "\\\\ *********************************************************** //" << endl
		<< endl;
	return output;
}

// -- Fétails d'implem Pour l'affichage de l'environnement -- //

// non présent dans la classe (outil pour affiche_cell_decalage)
ostream& affichage_decalage(ostream& output, vector<int>* nb_decalage, int* dec, int flag) {
  // flag: il faut décaler d'un espace l'affchage
  string decalage = "    ";
  if (0 < nb_decalage->size()) {
    for (int i=1; i<(int)nb_decalage->size(); i++)
      {
	int dec = (*nb_decalage)[i] - (*nb_decalage)[i-1];
	for (int j=0; j<dec; j++)
	  {
	    cout << decalage;
	  }
	cout << "|";
      }
    for (int i=0; i < (*dec) - (*nb_decalage)[(int)(*nb_decalage).size() - 1]; i++)
      {
	cout << decalage;
      }  
  }
  else {
    for (int i=0; i < (*dec); i++)
      {
	cout << decalage;
      }
  }
  if (flag) {
    cout << " ";
  }
  cout << "|";
  return output;
};

// présent dans la classe (et friend)
ostream& affiche_cell_decalage (ostream& output, Cell* cell, vector<int>* nb_decalage, int* dec, int flag)
// flag: il faut décaler d'un espace l'affichage
{
  // il faudrait aussi décaler l'affichage de la mémoire et du buffer (pour le moment on ne l'affiche pas)
  affichage_decalage(output, nb_decalage, dec, flag);
  cout 	<< "// ************************* Cellule ************************* \\\\" << endl;
  affichage_decalage(output, nb_decalage, dec, flag);
  cout << "\tPropriétaire : " << cell->p_proprietaire << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tFamille : " << cell->p_numero_famille << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tIndividu : " << cell->p_nom_so << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tMembres de la famille : " << *(cell->p_taille_famille) << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tTaille du code : " << cell->p_taille_code << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tEnergie : " << cell->p_nrj << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tGénération : " << cell->p_gen << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tAge : " << cell->p_age << endl;
  
  string reponse;
  if (not(cell->p_mere)) { reponse = "Oui"; } else { reponse = "Non"; }
  affichage_decalage(output, nb_decalage, dec, flag);
  cout << "\tCellule souche? " << reponse << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tNombres de filles: " << (cell->p_filles.size()) << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << endl;
  
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tMémoire : " << cell->p_mem  << endl;
  
  //affichage_decalage(output, nb_decalage, dec, flag); 
  //cout << "\tBuffer : " << endl;
  //affichage_decalage(output, nb_decalage, dec, flag); 
  //cout << cell->p_buf << endl;
  
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\tPosition : " << cell->p_pos << endl;
  
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << "\\\\ *********************************************************** //" << endl;
  affichage_decalage(output, nb_decalage, dec, flag); 
  cout << endl;;
  return output;
}
