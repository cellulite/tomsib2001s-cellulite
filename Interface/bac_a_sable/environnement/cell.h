#ifndef DEF_CELL		/* pour ne charger ce .h qu'une fois */
#define DEF_CELL

#include <iostream>
#include <vector>
#include <complex>
#include <set>
#include <limits.h>
#include "memoire.h"
#include "buffer.h"
#include "position.h"

//pour initialiser le début du chemin vers les bibliothèques dynamiques
//en fonction du chemin d'exécution
void init_path();
/*lors des appels à dlopen, si le char* n'est pas un chemin absolu, dlopen va chercher 
 * dans les endroit spécifiés par variable du shell qui cible les librairies classiques. 
 * J'ai donc choisi de chercher le chemin absolu indépendemment de manière dynamique pour 
 * que cela fonctionne chez tout le monde mais on pourrait aussi vraisemblablement modifier 
 * la variable du shell. Au final, init_path remplit le char* static concerné avec le chemin 
 * du répertoire de l'exécutable dans lequel le .so doit se trouver.
 */

/***********************/
/*       OUTILS        */
/***********************/

/* le type de la structure symbolisant une assignation (x,y) : rang x dans la mémoire -> valeur nouvelle y */
// Attention: dans le code on pourra écrire (0,mem[1] -1) mais un assign contient des entiers donc il faudra évaluer ce mem[1])
typedef std::vector<std::complex <int> > assign;

class Env;  /* pour pouvoir parler de Env (on ne peut pas include à cause de dépendances circulaires) */


/***************/
/*    CELL     */
/***************/

class Cell
{
	//pour afficher une cellule sans se casser la tête, simplement via `cout << ma_cellule`
	//le friend est nécessaire pour permettre à la fonction d'accéder à tous les champs de la cellule
	friend std::ostream& operator<< (std::ostream& output, const Cell& c);
	/* pour pouvoir afficher un vrai arbre généalogique */
	friend std::ostream& affiche_cell_decalage (std::ostream& output, Cell* c, std::vector<int>* nb_decalage, int* dec, int flag);
public:
	//constructeur pour la première cellule (cellule souche)
	Cell(const char *nom_so_, int nrj_, int proprietaire_, int numero_famille_);
	//constructeur pour les cellules filles
	/* la mémoire de la fille sera celle de sa mère sur laquelle on applique l'assignement ass */
	Cell(Cell* mother_, const char* racine, int *tableau_valeurs_args, int *tableau_indices_vars, int deb, assign ass, int taille_code_, int nrj_);
	~Cell();	/* déconstructeur */

	//accesseurs pour les variables de bases
	int nrj() const;	/* on indique avec le const que la fonction ne touche pas aux attributs */
	int gen() const;
	int age() const;
	
	// Mise à jour de l'nrj (ce qui a été envoyé arrive)
	void nrj_update();
	// Pour rececevoir de l'nrj (soleil + envoi) (c'est bufferisé)
	void nrj_receive(int quantite);

	// accesseur pour la taille du code
	int taille_code() const;

	//pour l'environnement, accès en lectue/écriture à la position d'une cellule (via les méthodes de Position seulement))
	Position& pos() { return p_pos; };

	//pour la demande d'énergie des actions:
	//renvoit true si la cellule a assez d'énergie et lui enlève l'énergie chargée
	//renvoit false sinon et le comportement est à définir (si p_nrj < nrj_) :
	//	la cellule peut donner toute son énergie ou rien (par exemple)
	//	comportement actuel : donne rien mais perd nrj_ (fixé dans le cell.cpp))
	bool nrj_load(int nrj_); 

	//gestion de la mémoire : permet le lecture et l'écriture
	int& mem(int indice_);

	//gestion du buffer
	void mess_update_end();	/* met à jour le buffer des messages (fin de tour) */
	void mess_store(const Message mess, Direction dir);
	/* permet d'enregistrer un message selon une direction qui pourra être lu le tour suivant*/

	Message mess_load(Direction dir);
	/* permet de lire un message arrivé pendant le tour précédent selon une direction*/

	//action
	void action(Env& env);	/* permet d'exécuter le code la cellule */
	void grow(int E_soleil); /* permet de faire grandir la cellule */
	
	// Arbre généalogique
	/* il faut noter que la méthode fille est lente (linéaire en i parce que les filles sont dans un set)
	   mais on ne l'utilisera que pour débugger */
	Cell* mere();		/* donne un pointeur vers la mère */
	int nb_filles();  	/* donne le nombre de filles */
	Cell* fille(int i);	/* donne l'adresse de la i-eme fille */
	void init_mere();	/* permet de détruire le lien de parenté quand la mère meurt */

	// Ajouter des cellules tuées par celle-ci
	void tue (int nombre_tuees);		/* rajoute  nombre_tuees à p_nb_mort */
	// Donne le nombre de cellules tuées par celle-ci
	int a_tue () const;

	// Accesseurs pour "carte de visite"
	int proprietaire() const;
	int numero_famille() const;
	int taille_famille() const;
	int pos_x() const;
	int pos_y() const;
	
	// Pour la sauvegarde de l'environnement
	const char* get_nom_so() const;

	// Détails d'implem pour bufferisé les déplacements
	int& a_bouge() { return p_a_bouge; };		/* permet l'accès et l'écriture de p_a_bouge */

private:
	int p_proprietaire;     /* pour identifier le possesseur de la cellule */
	int p_numero_famille;   /* pour identifier la famille de cellules */
	int p_numero_individu;	/* pour identifier la cellule au sein de sa famille */
	int *p_taille_famille;	/* un pointeur connu de tous les individus d'une famille contenant la taille de la famille */
	const char *p_nom_so;		/* le nom de la librairie dynamique contenant la cellule */
	int p_nrj, p_gen, p_age, p_taille_code; /* les variables de bases */
	int p_nrj_buff;				/* c'est l'nrj que recevra la cellule au prochain tour */
	/* Attention: quand une cellule exécute une action, elle perd directement son nrj (non "bufferisé" ici) */
	Memoire p_mem;		/* la mémoire suplémentaire */
	Buffer p_buf;		/* les buffers de message */
	Position p_pos; 	/* la position de la cellule */
	/* le code à exécuter de la cellule */
	void (*p_action)(Cell*, Env&, int *tableau_valeurs_args, int *tableau_indices_vars, int debut);
	int *p_tableau_valeurs_args; /* les arguments de p_action qui vont bien (la compil) */
	int *p_tableau_indices_vars;  /* les indices des variables locales de p_action qui vont bien (la compil) */
	int p_debut;		      /* l'indice de début de mémoire pour l'action */
	// Pour un "arbre généalogique" de l'organisme
	Cell *p_mere;		/* une pointeur vers la cellule mère (NULL pour la première) */
	std::set<Cell*> p_filles; /* une liste de pointeurs vers les filles */
	int p_a_bouge;		  /* un petit flag pour savoir si cette cellule est en train de bouger ou qu'elle va rester là (détails d'implem) */
	// Pour les scores
	int p_nb_mort;		/* nb de cellules écrasées par celle-ci */
};

#endif //guard

// REMARQUES:
// --> pour p_nb_cousines:
	//il faudrait se pencher sur les pointeurs intelligents
	//pour savoir à quel moment faire dlclose()
	//cf. shared_ptr dans boost par exemple
	//ou peut-être seulement implémenter un truc rapide...
	//Solution : pour l'instant, il semblerait que cela puisse être géré
	//simplement avec le pointeur p_nb_cousines
