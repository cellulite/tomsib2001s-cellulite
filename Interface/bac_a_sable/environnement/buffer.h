#ifndef DEF_BUFFER		/* pour ne charger qu'une fois ce .h */
#define DEF_BUFFER

#include <iostream>
#include "direction.h"

typedef int Message;	/* le type des messages (à priori que des 0 et 1) */
/* on ne parlera que de messages de taille > 0 */

class Buffer
{
	//pour afficher simplement un buffer via cout << buffer
	friend std::ostream& operator<< (std::ostream& output, const Buffer& buf);

public:
	//constructeur
	Buffer();

	//destructeur
	~Buffer();

	//manipulation des buffers
	/* on lit un message venant d'une direction, s'il n'y en a pas on reçoit un string de taille 0 sinon le message*/
	Message load(Direction dir);

	/* on dépose un message dans la "boîte au lettres" (qui pourra être lu au prochain tour) */
	Buffer& store(const Message msg, Direction dir);
	
	//transfert de data_temp vers data  (de la boîte aux lettres à la cellule))
	Buffer& update();
	
 private:
	//on utilise des pointeurs même si on connaît la taille à l'avance
	//pour "faciliter" la fonction update
	//en espérant que l'allocation dynamique n'est pas trop lente 
	//par rapport à la copie de 8 messages ^^
	Message *data_temp;	/* le tableau recevant les messages qui pourront être lu le tour suivant */
	Message *data;		/* le tableau des messages prets à être lu */

};

#endif //guard
