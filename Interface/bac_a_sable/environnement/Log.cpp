#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <sstream>
#include <time.h>
#include "Log.h"

using namespace std;		// ici on a le droit aux espaces de noms

/**********************************************/
/* Ce fichier définit les implémentations des */
/* objets utilisés par le log des erreurs     */
/*                                            */
/**********************************************/

Log::Log()
  : message ("")
{};

Log::~Log() {};


void Log::rm () {
  message = "";

};

void Log::add (string mess) {
  message += mess;

};

Log& operator<< (Log& log, string mess) {
  log.message += mess;
  return log;
};

Log& operator<< (Log& log, int entier) {
  stringstream ss; //create a stringstream
  ss << entier; //add number to the stream
  log.message += ss.str();//return a string with the contents of the stream
  return log;
};

ostream& operator<< (ostream& output, const Log& log) {
  time_t seconds;
  seconds = time (NULL);
  output << "############################## LOG ERREUR ########################################" << endl;
  output << "### Temps: " << (1+(seconds/3600)) %24  << " heures et " << (seconds/60)%60 << " minutes et " << (seconds)%60<< " secondes." <<endl;
  output << "Et "<< (seconds/(3600)- ((41)*(24*365)))/24 - 6 << " jours depuis 1 janvier 2011." << endl;
  output << "### Log de l'erreur:" << endl;
  output << log.message << endl;
  return output;
};
