#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <stropts.h>
#include <signal.h>
#include "config_comm_r.h" //Contient le port et l'adresse du serveur
#include "interface_client.h"

#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <sstream>

using namespace std;

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

static SOCKET Ma_socket;
static SOCKADDR_IN Serveur;
static char Buffer[TAILLE_BUFF];
static char Trash_buff[MAX_SOCKET_SIZE];
static char *login;
static char *password;
//static FILE *stream;

void send_signal(){
    kill(getppid(), SIGHUP);
} 

int connecte_serveur(char *_login, char *_password){
//	struct hostent* info_serveur;
//	struct hostent* info_serveur = gethostbyname("localhost");
//	if(!info_serveur)
//		printf("Failure\n");
    char ack[8];
    int ack_length;
    int resize = MAX_SOCKET_SIZE;
    login = (char *)malloc(strlen(_login) * sizeof(char));
    password = (char *)malloc(strlen(_password) * sizeof(char));
    strcpy(password, _password);
    strcpy(login, _login);

    if((Ma_socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
	fprintf(stderr, "binding socket failed\n");
	return -1;
    }
    setsockopt(Ma_socket, SOL_SOCKET, SO_RCVBUF, &resize /*rmem_max*/, sizeof(int));
    Serveur.sin_family = AF_INET;
    Serveur.sin_port = htons(PORT_SERVEUR);
    Serveur.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
    if(connect(Ma_socket, (SOCKADDR*) &Serveur, sizeof(SOCKADDR)))
    {
	fprintf(stderr, "Connection socket failed\n");
	return -1;
    }

    if(write(Ma_socket, (void*)"connect\n", 8) < 0){
	fprintf(stderr, "Write socket failed\n");
	return -1;
    }

    strcat(strcat(strcat(strcat(Buffer, login), " "), password), "\n");
    if(write(Ma_socket, Buffer, strlen(Buffer)) < 0){
	fprintf(stderr, "Write socket failed\n");
	return -1;
    }
	    
    ack_length = read(Ma_socket, ack, 50);


    if(ack_length < 0)
	return -2;
    ack[ack_length] = '\0';
    if(!strcmp(ack, "badauth")){
	fprintf(stdout, "%s\n", ack);
	return -2;
    }
    return atoi(ack);
}

int recoit_carte(int (**cells)[5])
{
    int nb_cells;
    fd_set ens_rec;

    FD_ZERO(&ens_rec);
    FD_SET(Ma_socket, &ens_rec);
    if(select(FD_SETSIZE, &ens_rec, NULL, NULL, NULL) < 0)
    {
	cerr << "Erreur ici\n" << endl;
	return -1;
    }

    if(FD_ISSET(Ma_socket, &ens_rec))
    {		
	stringstream to_parse (stringstream::out | stringstream::in);
	int nb_p, nb_f;
	

	// On commence à récupérer un bout de carte
	int nb_lu = recv(Ma_socket, Trash_buff, MAX_SOCKET_SIZE, 0);
	if(nb_lu == 0){
	    shutdown(Ma_socket, 2);
	    connecte_serveur(login, password);
	    return recoit_carte(cells);
	}


	char Buffer[256];
	
	Trash_buff[nb_lu] = '\0';
	to_parse << Trash_buff;
	while(to_parse.getline(Buffer, 256) && Buffer[strlen(Buffer) -1] != '&');
	
	if(Buffer[strlen(Buffer) -1] != '&'){
	    *cells = new int [1][5];
	    return -1;
	}
	Buffer[strlen(Buffer) -1] = '\0';
//	cerr << endl << endl << "caracteres a lire:"<< Buffer << endl;
	int nb_caract_to_read = atoi(Buffer);
	// IL nous faut lire nb_caract_to_read pour chopper la carte et le score en entier

	nb_lu -= strlen(Buffer) +2;
	while(nb_lu < nb_caract_to_read){
	    FD_ZERO(&ens_rec);
	    FD_SET(Ma_socket, &ens_rec);
	    if(select(FD_SETSIZE, &ens_rec, NULL, NULL, NULL) < 0)
	    {
		cerr << "Erreur ici\n" << endl;
		return -1;
	    }
	    if(FD_ISSET(Ma_socket, &ens_rec))
	    {
		int nb_lu_temp = recv(Ma_socket, Trash_buff, MAX_SOCKET_SIZE, 0);
		nb_lu += nb_lu_temp;
		Trash_buff[nb_lu_temp] = '\0';
		to_parse << Trash_buff;	   
	    }
	}
	to_parse.getline(Buffer, 256);
	Buffer[strlen(Buffer) -1] = '\0';
	nb_cells = atoi(Buffer);
//	cerr << nb_line_trashed << " ::: nb_line to read:" << nb_cells << endl;
	*cells = new int [nb_cells<1?1:nb_cells][5];

	for(int i=0; i<nb_cells * 5; i++){
	    if(!(to_parse >> (*cells)[i/5][i%5])){
		cerr << "étrangeté!" << endl;
		delete [] cells;
		shutdown(Ma_socket, 2);
		connecte_serveur(login, password);
		return recoit_carte(cells);
	    }
	}	
	if(!(to_parse >> nb_p >> nb_f)){
	    // Tampis pas de score, peut être au prochain coup
	    cerr << "pas de score" << endl;
	    return nb_cells;
	}
//	to_parse >> nb_f;
//	cerr << "----------------------" << endl ;
	cout << nb_p << " " << nb_f << endl;
//	cerr << "::::::::::::::::::::::" << endl ;

	cout.flush();
	for(int i = 0; i<nb_p ; i++){
	    string s_1, s_2, s_4, s_3, s_5, s_6;
	    if(!(to_parse >> s_1 >> s_2 >> s_3 >> s_4 >> s_5 >> s_6)){
		cerr << "probleme dans scores!" << endl;
		return nb_cells;
	    }
	    cout << s_1 << endl << s_2 << " " << s_3 << " " << s_4 << " " << s_5 << " " << s_6 << endl;
	    cout.flush();
	}
//	cerr << "----------------------" << endl ;
	
    }
    return nb_cells;
}

int deconnecte_serveur()
{
//	printf("ERREUR\n");
    if(shutdown(Ma_socket, 2))
	fprintf(stderr, "ERREUR -------------------- OOO\n");
    return 0;
}



/* 
int read_score(stringstream S, int taille_score){
    int nb_p, nb_f, temp;
    nb_p = taille_score;
    if(taille_score == -1){
	if(!(S >> nb_p >> nb_f)){
	    return -1;
	}
    }
    for(int i=0; i<nb_p; i++){
	for(int j=0; j<nb_f; j++){
	    if(!(S >> temp)){
		return ;
	    }
	    cout << temp << " ";
	}
	cout << endl;
    }
    return 0;
}
int read_map(stringstream S, int taille_map, int (**cells)[5]){
    int nb_c, temp;
    nb_cells_buff = taille_map
    if(taille_map == -1){
	if(!(S >> nb_cells_buff)){
	    return -1;
	}
	*cells =  new int [nb_cells_buff<1?1:nb_cells_buff][5];
    }
    for(int i=0; i<nb_cells_buff; i++){
	if(!(S >> (*cells)[curr_ligne][0] >> (*cells)[curr_ligne][1] >> (*cells)[curr_ligne][2] >> (*cells)[curr_ligne][3] >> (*cells)[curr_ligne][4])){
	    

	}
    }
    return 0;
}

*/
