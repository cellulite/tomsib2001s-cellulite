#ifndef DEF_INTERFACE_CLIENT
#define DEF_INTERFACE_CLIENT

//Correct return value is 0 !!!
//int init(); //pour Windows
//int term(); //pour Windows
//int premiere_connection();
int connecte_serveur(char *_login, char *_password);
//int demande_carte();
int recoit_carte(int (**cells)[5]);
//int envoie_cellule(char* fichier, int mon_id);
int deconnecte_serveur();

#endif //guard
