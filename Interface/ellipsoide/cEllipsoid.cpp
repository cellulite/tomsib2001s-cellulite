#include <iostream>
#include <cmath>
#if defined(__linux__)
	#include <GL/gl.h>
#elif defined(__APPLE__)
	#include <OpenGL/gl.h>
#else
	#error "OS non supportée\n"
#endif
#include "cEllipsoid.h"
using namespace std;

#define PI 3.14159265

cEllipsoid::cEllipsoid(double a, double b, double c)
: _x (NULL)
, _y (NULL)
, _z (NULL)
, _a (a)
, _b (b)
, _c (c)
, _nb_latitudes (0)
, _nb_longitudes (0)
{
};

cEllipsoid::~cEllipsoid()
{
	delete [] _x;
	delete [] _y;
	delete [] _z;
}

int cEllipsoid::generate(int nb_latitudes, int nb_longitudes)
{
	if(nb_latitudes < 1 || nb_longitudes < 1)
		return -1;
	_nb_latitudes = nb_latitudes;
	_nb_longitudes = nb_longitudes;
	_x = new double [_nb_latitudes * _nb_longitudes];
	_y = new double [_nb_latitudes * _nb_longitudes];
	_z = new double [_nb_latitudes * _nb_longitudes];

	double z;
	double sinZ;
	double delta_angleZ = PI / (2 * _nb_longitudes),
		delta_angleX = (2 * PI) / _nb_latitudes;
	double angleZ = delta_angleZ, angleX;

	int index = 0;
	for(int i = 0 ; i < _nb_longitudes ; ++i)
	{
		z = _c * cos(angleZ);
		sinZ = sin(angleZ);
		angleX = 0.0;
		for(int j = 0 ; j < _nb_latitudes ; ++j)
		{
			_x[index] = _a * sinZ * cos(angleX);
			_y[index] = _b * sinZ * sin(angleX);
			_z[index] = z;
			angleX += delta_angleX;
			++index;
		}
		angleZ += delta_angleZ;
	}

	return 0;

/*
	glBegin(GL_TRIANGLES);

	z2 = _centerZ + _c * cos(delta_angleZ);
	sinZ2 = sin(delta_angleZ);
	for(int i = 0 ; i < nb_longitudes ; ++i)
	{
		z1 = z2;
		z2 = _center.z() + _c * cos((i + 2) * delta_angleZ);
		sinZ1 = sinZ2;
		sinZ2 = sin((i + 2) * delta_angleZ);

		cosX2 = 1.0;
		sinX2 = 0.0;
		for(int j = 0 ; j < nb_latitudes ; ++j) 
		{
			cosX1 = cosX2;
			cosX2 = cos((j + 1) * delta_angleX);
			sinX1 = sinX2;
			sinX2 = sin((j + 1) * delta_angleX);
			double x1_1 = _center.x() + _a * sinZ1 * cosX1;
			double x1_2 = _center.x() + _a * sinZ1 * cosX2;
			double x2_1 = _center.x() + _a * sinZ2 * cosX1;
			double x2_2 = _center.x() + _a * sinZ2 * cosX2;
			double y1_1 = _center.y() + _b * sinZ1 * sinX1;
			double y1_2 = _center.y() + _b * sinZ1 * sinX2;
			double y2_1 = _center.y() + _b * sinZ2 * sinX1;
			double y2_2 = _center.y() + _b * sinZ2 * sinX2;

//			(2_1 - 2_2) vect (1_1 - 2_2)
			normalize_and_put((y2_1 - y2_2) * (z1 - z2) - 0, 
				0 - (z1 - z2) * (x2_1 - x2_2), 
				(x2_1 - x2_2) * (y1_1 - y2_2) - (x1_1 - x2_2) * (y2_1 - y2_2)
				);
			glVertex3d(x1_1, y1_1, z1);
			glVertex3d(x2_1, y2_1, z2);
			glVertex3d(x2_2, y2_2, z2);

//			(2_2 - 1_2) vect (1_1 - 1_2)
			normalize_and_put(0 - (y1_1 - y1_2) * (z2 - z1), 
				(z2 - z1) * (x1_1 - x1_2) - 0, 
				(x2_2 - x1_2) * (y1_1 - y1_2) - (x1_1 - x1_2) * (y2_2 - y1_2)
				);
			glVertex3d(x2_2, y2_2, z2);
			glVertex3d(x1_1, y1_1, z1);
			glVertex3d(x1_2, y1_2, z1);
		}
	}

	glEnd();
*/
}

int cEllipsoid::draw(double x, double y, double z)
{
	if(_nb_latitudes == 0)
		return -1;

	int index = 0;

	glBegin(GL_TRIANGLES);
	for(int j = 0 ; j < _nb_latitudes ; ++j)
	{
		glVertex3d(x, y, z + _c);
		glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
		++index;
		if(j < (_nb_latitudes - 1))
			glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
		else
			glVertex3d(x + _x[0], y + _y[0], z + _z[0]);
	}
	glEnd();

	glBegin(GL_QUADS);
	for(int i = 0 ; i < (_nb_longitudes - 1) ; ++i)
	{
		for(int j = 0 ; j < _nb_latitudes ; ++j)
		{
			int index2 = index - _nb_latitudes;
			glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
			glVertex3d(x + _x[index2], y + _y[index2], z + _z[index2]);
			++index;
			if(j < (_nb_latitudes - 1))
			{
				++index2;
				glVertex3d(x + _x[index2], y + _y[index2], z + _z[index2]);
				glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
			}
			else
			{
				index2 = index - _nb_latitudes;
				int index3 = index2 - _nb_latitudes;
				glVertex3d(x + _x[index3], y + _y[index3], z + _z[index3]);
				glVertex3d(x + _x[index2], y + _y[index2], z + _z[index2]);
			}
		}
	}
	glEnd();
/*
	index -= _nb_latitudes;

	glBegin(GL_TRIANGLES);

	for(int j = 0 ; j < _nb_latitudes ; ++j)
	{
		glVertex3d(x, y, z);
		glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
		++index;
		if(j < (_nb_latitudes - 1))
			glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
		else
		{
			index -= _nb_latitudes;
			glVertex3d(x + _x[index], y + _y[index], z + _z[index]);
		}
	}

	glEnd();
*/
	return 0;
}

void cEllipsoid::normalize_and_put(double x, double y, double z)
{
	double norm = sqrt(x * x + y * y + z * z);
	double newX = x / norm;
	double newY = y / norm;
	double newZ = z / norm;
	glNormal3d(newX, newY, newZ);
}
