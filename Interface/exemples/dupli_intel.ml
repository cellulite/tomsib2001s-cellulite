(* Duplications entrecoupées de pauses et de déplacements et de pauses *)
dupli : vars (dir,etat,compt_pas,compt_dupli) args (niv_nrj,nb_pas,nb_dupli,nb_skip)
  (* #ARGS
     niv_nrj : seuil au desous duquel on ne se duplique pas + la moitié = nrj cédée
     nb_pas : nombre de pas entre les duplications
     nb_dupli : nombre de duplications avant de se reposer
     nb_skip : durée du repos
     #VARS
     dir : direction visée courante
     etat : zero  duplication, un  déplacement, deux repos
     compt_pas, compt_dupli : compteurs (O)
  *)
  if (etat = 0)
  then
    if (compt_dupli = nb_dupli)
    then {
      compt_dupli := 0;
      etat := 2;
      compt_pas := 0;
      Skip
    }
    else
      if (energy > niv_nrj)
      then {
	let next_dir = NextDir(dir) in	(* sens horaire *)
	if not (next_dir band See()) 	(* si il n y a personne sur next_dir *)
	then {
	  dir := next_dir;
	  etat := 1;
	  compt_dupli := compt_dupli + 1;
          Duplicate(niv_nrj/2, dir, dupli(dir,1,nb_pas,0)(niv_nrj,nb_pas+1,nb_dupli,nb_skip))
	}
	else  
	  Skip
      }
      else
	Skip
  else
    if (etat = 1)
    then
      if (compt_pas = 0)
      then {
	etat := 0;
	Skip
      }
      else {
	compt_pas := compt_pas - 1;	
	Move(dir)
      }
    else
      if compt_pas = nb_skip
      then {
	etat := 0;
	Skip
      }
      else Skip
;;

@dupli(0,0,0,0)(100,1,3,3)
