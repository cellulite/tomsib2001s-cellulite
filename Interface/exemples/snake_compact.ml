(* Forme un snake tournant en boucle *)
compact : vars (x,etat) args (lg)
  (* lg: longueur du snake *)
  if etat = 0 and energy > 100
 then     etat := 1;
 if x < lg
 then
   Duplicate(energy/2,N,compact(x+1,0)(lg))
 else
   if x = lg
   then
     Duplicate(energy/2,O,compact(x+1,0)(lg))
   else
     if x < 2*lg
     then
       Duplicate(energy/2,S,compact(x+1,0)(lg))
     else
       if x = 2*lg
       then
	 Duplicate(energy/2,E,compact(0,0)(lg))
       else
	 Skip
 else Skip;;
@compact(0,0)(5)
  
