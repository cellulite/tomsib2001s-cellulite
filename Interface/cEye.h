#ifndef _DEF_EYE
#define _DEF_EYE

#include <SDL/SDL.h>
#if defined(__linux__)
	#include <GL/gl.h>
#elif defined(__APPLE__)
	#include <OpenGL/gl.h>
#else
	#error "OS non supportée\n"
#endif

class cEye
{
public:
	cEye();
	cEye(int mapX, int mapY);
	~cEye();
	cEye& look();
	cEye& set_mapX(int mapX) { _mapX = mapX; return *this; };
	cEye& set_mapY(int mapY) { _mapY = mapY; return *this; };
	cEye& on_mouse_motion(const SDL_MouseMotionEvent &event);
	cEye& on_mouse_button(const SDL_MouseButtonEvent &event);
	cEye& on_keyboard(const SDL_KeyboardEvent &event);
	
private:
	double _position_sensibility, _angle_sensibility, _distance_sensibility;
	double _centerX, _centerY;
	double _posX, _posY, _posZ;
	const static double _upX = 0, _upY = 0, _upZ = 1;
	const static double _centerZ = 0.5;
	bool _hold_left, _hold_right;
	double _distance, _angleZ, _angleX;
	int _mapX, _mapY;

};

#endif //guard
