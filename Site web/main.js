$(document).ready(function(){
    
    
    var bcolor = $("#wrap > li").css('background-color');
    var tcolor = $("#wrap > li").css('color');
    var lang = "en";
    
    $("#wrap > li > a").css('color','white');
    
    $("#wrap > li.forum ").hover( function(){
	var listItem = $(this);	
	listItem.css('background-color', tcolor);
	listItem.children().css('color', bcolor);
    },
				  function(){	
				      var listItem = $(this);	
				      listItem.css('background-color', bcolor);
				      listItem.children().css('color', tcolor);	
				  });
    
    $("#wrap > li").hover( function(){
	var listItem = $(this);	
	listItem.css('background-color', tcolor);
	listItem.css('color', bcolor);
    },
			   function(){	
			       var listItem = $(this);	
			       listItem.css('background-color', bcolor);
			       listItem.css('color', tcolor);	
			   });
    
    $("#wrap > li").click(function(){
	var litext = $(this).text();
	var cl = $(this).attr("class");
	
	if(cl == "home"){
	    $("#mainheader").text(litext);
	    $("#maintext > div").css('display','none');	
	    $("#maintext > div.home").css('display','block');		
	}			
	else if(cl == "contact"){
	    $("#mainheader").text(litext);
	    $("#maintext > div").css('display','none');	
	    $("#maintext > div.contact").css('display','block');			
	}	
	else if(cl == "tutoriel"){
	    $("#mainheader").text(litext);
	    $("#maintext > div").css('display','none');	
	    $("#maintext > div.tutoriel").css('display','block');			
	}	
	/*else if(cl == "forum"){
	  window.location.href="forum";
	  $("#maintext > div").css('display','none');	
	  $("#maintext > div.forum").css('display','block');	
	  }*/	
	else if(cl == "downloads"){
	    $("#mainheader").text(litext);
	    $("#maintext > div").css('display','none');	
	    $("#maintext > div.downloads").css('display','block');			
	}	
	else if(cl == "documentation"){
	    $("#mainheader").text(litext);
	    $("#maintext > div").css('display','none');	
	    $("#maintext > div.documentation").css('display','block');			
	}
	else{}
    });
    
    $("#createtopiclink").click(function(){
	$("#maintext > div").css('display','none');	
	$("#maintext > div.forum2").css('display','block');	
	return false;});	
    
    $("#viewtopiclink").click(function(){
	$("#maintext > div").css('display','none');	
	$("#maintext > div.forum1").css('display','block');	
	return false;});			
    
    
    $("#fr").click(function(){
	
	window.location.href="index.php";
	/*lang = "fr";	
	  $(".fr").css('display','block');
	  $(".home > p").text("Bienvenue à Grain de Cell!");*/
    });
    
    $("#gb").click(function(){
	
	window.location.href="index2.php";
	/*lang = "en";	
	  $(".home > p").text("Welcome to Grain de Cell!"); */

    });
    
});


