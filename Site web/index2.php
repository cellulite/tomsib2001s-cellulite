<?php $js = (!isset($_GET['nojs'])); ?>
<!DOCTYPE html>
<html>
<head>
<title>Grain de Cell</title>
<meta charset="utf-8" />

   <link rel="stylesheet" href="style.css" /> 


   <script src="jquery-1.6.1.min.js" type="text/javascript"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
   <?php if ($js) { ?>
<script src="main2.js"></script>
   <?php } ?>
   <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  
   <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Reenie+Beanie" />
  

  
  
   </head>
   <body>
   <ul id="flags">
   <li id="fr"><img src="fr.png" alt="" ></li>
   <li id="gb"><img src="gb.png" alt="" ></li>
   </ul>
   <img id="logo" src="logo1.jpg" alt="Grain de Cell" >
   <div id="header">
   <img id="topheader" src="cooltext9.png" alt="Grain de Cell" >
   </div>

   <ul id="wrap">
   <li class="home">Home</li>
   <li class="tutorial">Tutorial </li>
   <li class="documentation">Documentation </li>
   <li class="contact">Contact</li>
   <li class="forum"><a id="forumanchor" href="forum">Forum</a> </li>
   <li class="downloads">Downloads  </li>
   </ul>
  
   <div id="main">
   <h1 id="mainheader">Home</h1>

   <div id="maintext">
   <div class="home">
   <h2>The project</h2>
   <p>Grain de Cell is a project in computer science conducted by six students at École Normale Supérieure 
   de Lyon during the first year of a masters degree. The project is in development since September 2011
   and will be completed in January 2012. You will find a git repository on
    <a href="https://gitorious.org/cellulite">Gitorious<a> as well as  <a href="mailto:projectcell@listes.ens-lyon.fr">a mailing list</a>. </p>
   <p>The opening for beta-testers is planned for the 17 December.</p>
   <div style="text-align:bottom;"> 
   <p ALIGN=center id="countdown">
   <iframe src="http://free.timeanddate.com/countdown/i2vc8leo/n333/cf111/cm0/cu4/ct0/cs1/ca0/co1/cr0/ss0/cac000/cpc000/pc66c/tc66c/fs100/szw320/szh135/tatTime%20left%20to%20Beta%20testing/tac000/tptThe%20Beta%20testing%20has%20started/tpc000/iso2011-12-17T23:00:00/bas5/pa5" frameborder="0" width="340" height="155"></iframe>
   </p>
   </div>
   <h2>The principle of the game</h2>
   <p>Grain de Cell is a game based on the coding of an artificial intelligence. You can code cells which are then injected into a persistent universe containing cells previously added by other players. You can then watch the universe evolve and observe how your cells maintain their presence in this universe, defend themselves and play an important role in the ecosystem through displacement and the exchange of messages and energy.
   All this coding is done in a simplified programming language.</p>
		
   <p>Players first code a DNA implementing a desired behavior. Once they think they have created an interesting DNA, they submit it. A cell with the submitted DNA is then added to the world. At any moment, the player can watch the state of the world, see his score and that of others through a 3D interface.</p>
			
   <p>
      <table class="image">
      <caption align="bottom">3D interface : the height of a cell represents the amount of energy.</caption>
  		<tr><td><img HEIGHT="400" src="Screenshot-2.png" alt="Did not find picture." >
  		</table>
   </p>
   <p>
      <table class="image">
      <caption align="bottom">Strategic view.</caption>
      <tr><td>		
      <img HEIGHT="400"src="Screenshot-3.png" alt="Did not find picture." ></td></tr>		
      </table>
 	</p>
 	</div>

   <div class="contact">
   <h2>You can contact us at the following address</h2>
   <p><a href="mailto:projectcell@listes.ens-lyon.fr">projectcell@listes.ens-lyon.fr</a></p>
   <p>Or send a personal mail with the structure firstname.lastname@ens-lyon.fr. (A double last name is written 
										  together without point or hyphen.)</p>
   <p>Ann Johansson</p>
   <p>Antoine Plet</p>
   <p>Guillaume Lagarde</p>
   <p>Lucca Hirshi</p>
   <p>Thomas Sibut-Pinote</p>
   <p>Vincent Cohen-Addad</p>
   </div>

   <div class="forum"></div>

   
   <div class="tutorial">
   <iframe src="http://graal.ens-lyon.fr/graindecell/tuto_en.html" width="1000" height="4000"></iframe>		
   </div>

   <div class="documentation">		
   <iframe src="http://graal.ens-lyon.fr/graindecell/mdp_en.html" width="1000" height="9700"></iframe>	
   </div>
   
   <div class="downloads">
	<h2>
	Phase of beta testing
	</h2>
	<p>
	The game is in beta testing. If you wish to try it out, you can download the last version 
	<a href="http://graal.ens-lyon.fr/graindecell/GdC_0_3.tar.gz">here<a>. If you wish to stay updated
	on changes and new properties, we advice you to register to 
	<a href="http://groups.google.com/group/gdc-public">public mailing list<a>. If you encounter any bugs,
	we are grateful if you let us know on <a href="http://graal.ens-lyon.fr/bugzilla/enter_bug.cgi?product=Grain%20de%20Cell">Bugzilla<a>.
	</p>  
	
	<p>
	The archive contains the main folder Interface/ including a README for the installation and some examples 
	of codes written in the language CEL in exemples/ however we advice you to take a look at the documentation 
	(english version in pdf <a href="http://graal.ens-lyon.fr/graindecell/mdp_en.pdf">here<a>).
	</p>
   
   </div>   
   
   
   </div>
   </div>

   <div id="footer"><p><a id="credit" href="http://www.freedigitalphotos.net/images/view_photog.php?photogid=1152">Image: jscreationzs / FreeDigitalPhotos.net</a></p></div>

   </body>
   </html>
