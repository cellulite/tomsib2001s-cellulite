<?php $js = (!isset($_GET['nojs'])); ?>
<!DOCTYPE html>
<html>
<head>
<title>Grain de Cell</title>
<meta charset="utf-8" />

   <link rel="stylesheet" href="style.css" /> 


   <script src="jquery-1.6.1.min.js" type="text/javascript"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
   <?php if ($js) { ?>
<script src="main.js"></script>
   <?php } ?>
   <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  
   <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Reenie+Beanie" />
  

  
  
   </head>
   <body>
   <ul id="flags">
   <li id="fr"><img src="fr.png" alt="" ></li>
   <li id="gb"><img src="gb.png" alt="" ></li>
   </ul>
   <img id="logo" src="logo1.jpg" alt="Grain de Cell" >
   <div id="header">
   <img id="topheader" src="cooltext9.png" alt="Grain de Cell" >
   </div>

   <ul id="wrap">
   <li class="home">Accueil</li>
   <li class="tutoriel">Tutoriel </li>
   <li class="documentation">Documentation </li>
   <li class="contact">Contacts</li>
   <li class="forum"><a id="forumanchor" href="forum">Forum</a> </li>
   <li class="downloads">Téléchargements</li>
   </ul>
  
   <div id="main">
   <h1 id="mainheader", align="right">Accueil</h1>

   <div id="maintext">
   <div class="home">
   <h2>Le projet</h2>
   <p>Grain de Cell est un projet informatique mené par six étudiants de l’ENS de Lyon durant l’année de M1. 
   Il est en développement depuis septembre 2011 et s'achèvera en janvier 2012. Vous trouverez un dépôt git 
		hébérgeant le projet sur <a href="https://gitorious.org/cellulite">Gitorious<a> 
		ainsi qu’une 
		<a href="http://groups.google.com/group/gdc-public">liste de diffusion publique</a>. </p>
		 <p>L'ouverture aux beta-testeurs est prévue pour le 17 décembre.</p>
   <p id="countdown">
   <iframe src="http://free.timeanddate.com/countdown/i2vc8leo/n333/cf111/cm0/cu4/ct0/cs1/ca0/co1/cr0/ss0/cac000/cpc000/pc66c/tc66c/fs100/szw320/szh135/tatTime%20left%20to%20Beta%20testing/tac000/tptThe%20Beta%20testing%20has%20started/tpc000/iso2011-12-17T23:00:00/bas5/pa5" frameborder="0" width="340" height="155"></iframe>
   </p>
   <h2>Le jeu en quelques lignes</h2>
   <p>Grain de Cell est un jeu basé sur la programmation d’une intelligence artificielle. Les joueurs 
   programment des cellules qui sont introduites dans un univers persistant contenant déja des cellules d’autres 
   joueurs. Nous pouvons alors regarder l’univers évoluer et voir comment nos cellules persistent dans cette 
   univers, se défendent et arrivent à prendre une place importante via des déplacements, des duplications, des 
   envois de messages et d’énergie. La programmation des cellules se fait dans un langage simplifié.</p>
		 

   <p>Le joueur commence donc par coder un ADN implémentant le comportement voulu. Une fois que le joueur pense avoir 
   créé un ADN intéressant, il soumet ce code. Une cellule implémentant ce code est alors ajoutée au monde. Le 
   joueur peut ensuite à tout moment regarder l’état du monde en temps réel et voir son score et celui des autres 
   joueurs via une interface 3D.</p>
  <p>
     <table class="image">
     <caption align="bottom">Vue 3D : la hauteur représente le niveau d'énergie.</caption>
  		<tr><td><img HEIGHT="400" src="Screenshot-2.png" alt="Did not find picture." >
  		</table>
  </p>
  <p>
     <table class="image">
     <caption align="bottom">Vue 3D stratégique.</caption>
     <tr><td>		
     <img HEIGHT="400"src="Screenshot-3.png" alt="Did not find picture." ></td></tr>		
     </table>
  </p>
</div>
   
   <div class="contact">
		<h2>La liste de diffusion</h2>
<p>Il est possible de nous joindre à l'adrese suivante: <a href="mailto:projectcell@listes.ens-lyon.fr">projectcell@listes.ens-lyon.fr</a>.</p>
<h2> L'équipe de développement</h2>   
<p>Vouz pouvez aussi envoyer un mail personnel à l'un d'entre nous avec la structure prenom.nom@ens-lyon.fr. (les prénoms composés sont séparés par des tirets):</p>
		<p>Ann Johansson</p>
		<p>Antoine Plet</p>
		<p>Guillaume Lagarde</p>
		<p>Lucca Hirshi</p>
		<p>Thomas Sibut-Pinote</p>
		<p>Vincent Cohen-Addad</p>
		</div>
      
   <div class="forum"></div>
   <div class="tutoriel">
   <iframe src="http://graal.ens-lyon.fr/graindecell/tuto_fr.html" width="1000" height="4050"></iframe>
		
   <!-- <object data="http://graal.ens-lyon.fr/graindecell/tuto_fr.html" width="1000" height="1000"> <embed src="http://graal.ens-lyon.fr/graindecell/tuto_fr.html" width="1000" height="1000"> </embed> Error: Embedded data could not be displayed. </object>		
   -->	</div>

   <div class="documentation">
   <iframe src="http://graal.ens-lyon.fr/graindecell/mdp_fr.html" width="1000" height="14580"></iframe>
		
   <!-- <object data="http://graal.ens-lyon.fr/graindecell/mdp_fr.html" width="1000" height="1000"> <embed src="http://graal.ens-lyon.fr/graindecell/mdp_fr.html" width="1000" height="1000"> </embed> Error: Embedded data could not be displayed. </object>		
   -->	</div>
 
  <div class="downloads">
<h2>
Phase de betat-test
</h2>
<p>
Le jeu est en betat-test. Si vous voulez essayer le jeu, vous pouvez télécharger la dernière version <a href="http://graal.ens-lyon.fr/graindecell/GdC_0_3.tar.gz">ici<a>. Si vous voulez vous maintenir à jour des modifications et nouveautées (l'équilibrage va évoluer), nous vous conseillons de vous inscrire sur la <a href="http://groups.google.com/group/gdc-public">liste de diffusion publique<a>. Si vous rencontrez des buggs, merci de nous les faire remonter sur notre <a href="http://graal.ens-lyon.fr/bugzilla/enter_bug.cgi?product=Grain%20de%20Cell">Bugzilla<a>.
</p>

<p>
L'archive contient le dossier princpal Interface/ incluant un README pour l'installation et quelques exemples de codes écrit en langage CEL dans exemples/ mais nous vous conseillons de jeter un coup d'oeil à la documentation (version française en pdf <a href="http://graal.ens-lyon.fr/graindecell/mdp_fr.pdf">ici<a>).
</p>
</div>
   </div>
   </div>
   </div>

   <div id="footer"><p><a id="credit" href="http://www.freedigitalphotos.net/images/view_photog.php?photogid=1152">Image: jscreationzs / FreeDigitalPhotos.net</a></p></div>

   </body>
   </html>
