#ifndef _DEF_CLIENT
#define _DEF_CLIENT

class Client
{
public:
	int id;
	int socket;
	Client(int _id, int _socket):id(_id), socket(_socket) {};
};

#endif //guard
