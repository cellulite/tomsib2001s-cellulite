#ifndef DEF_DIRECTION		/* pour ne charger qu'une fois ce .h */
#define DEF_DIRECTION

//Définition d'un type direction pour se repérer (on donne des noms explicites))
//=> N_O = 0, N = 1, ... jusqu'à 7
typedef enum { N, N_E, E, S_E, S, S_O, O, N_O } Direction;

#endif //guard
