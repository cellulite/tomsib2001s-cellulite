#include <iostream>
#include "memoire.h"
using namespace std;

int main()
{
	Memoire mem;
	cout << mem << endl;
	mem.set_taille(10);
	cout << mem << endl;
	mem[1] = 3;
	mem[5] = 12;
	mem[4] = mem[1] * mem[5] - 1;
	cout << mem << endl;
	mem[0] = mem[10];	// fait bugger 'et c'est normal) l'exécution
	return 0;
}
