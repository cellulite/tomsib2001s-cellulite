#include "env.h"

/*********************************************************/
/* Bouts de codes pour introduire une nouvelle cellule
/*     PLUS TROP D'ACTUALITE CF. TEST_CELL.CPP
/*********************************************************/
namespace using std;

int main () {
  // Entrées:
  string fichier = "cell_1_2" + ".so"; // on rajoute le .so à la fin
  Position pos;			       // la position de la nouvelle cellule
  // Sortie
  void *handle;			       // le pointeur vers le fichier
  handle = dlopen(fichier, RTLD_LAZY);
  if(!handle)
    {
      fputs(dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
  string *nom_main;
  nom_main = dlsym(handle, "main");
  if((erreur = dlerror()) != NULL)
    {
      fputs(dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
  void (*mon_action)(Cell *cell, Env *env);
  mon_action = dlsym(handle, *nom_main);
  // on récupère la fonction défini par l'utilisateur comme son main
  if((erreur = dlerror()) != NULL)
    {
      fputs(dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
  int *nb_vars;
  nb_vars = dlsym(handle, "nb_vars");
  if((erreur = dlerror()) != NULL)
    {
      fputs(dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
  int *memoire_init[];
  memoire_init = dlsym(handle, "memoire_init");
  if((erreur = dlerror()) != NULL)
    {
      fputs(dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
  int *taille_code;
  taille_code = dlsym(handle, "taille_code");
  if((erreur = dlerror()) != NULL)
    {
      fputs(dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
  Cell nv_cell (nb_vars, taille_code, position, fichier, mon_action);
  // VOilà, nv_cell est comme on veut !
  update_find_cell(pos_new, nv_cell);	 // met à jour find_cell
  

  delete nv_cell;		// chercher l'équivalent avec ~Cell();
  nv_cell = 0;
  // Plus de freeze (data, buffers etc.. -> fuite de mémoire)??



  /*************************************************/
  /* Bouts de codes pour dupliquer une cellule     */
  
  Duplication(cell *myself, int nrj_cede, char *nom_ss_fct, int taille_ss_fct, int dir, int *assign); // le tableau de la mémoire pour la nouvelle cellule
  
  /*************************************************/
  
  if (possible que myself se duplique) // A implem
    {
      Dlopen(myself.nom_so);	             // on ouvre le fichier contenant la bibliotheque dynamique de la bonne cellule
      cell nv_cell;			     // on déclare un nouvel objet cell
      nv_cell.action = dl(nom_ss_fct);     // on charge l'adn (le code c++ correspondant) !!
      dl("assign_init") (nv_cell);         // on initalise la mémoire et la taille du code
      nv_cell.taille_code = taille_ss_fct;
      nv_cell.nrj = nrj_cede;
      assign_mem (assign, nv_cell);	     // assigne la mémoire selon assign : A IMPLEM (en gros on recopie le tableau assign dans data de nv_cell.mem)
      nv_cell.position = pos_new;	     // on le place sur la grille
      update_find_cell(pos_new, nv_cell);  // met à jour find_cell
      
      // -- Definitions des méthodes -- //
      // ------------------------------ //
      void get_message (char *mess, int dir){ /* 0 <= dir <= 7 */
	// BLABLA
      };
      
      // -- Messages -- //
      // -------------- //
      nv_cell.buff.data_temp = new bool[8]; // les 8 pointeurs pointent vers NULL (todo: vérifier?
      nv_cell.buff.data = new bool[8]; // les 8 pointeurs pointent vers NULL (todo: vérifier?)
    }
  else
    {
      
    }
  
  return(0);
}

