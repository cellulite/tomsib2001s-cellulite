#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <queue>
#include <vector>
#include <complex>
#include <signal.h>
#include <map>
#include <math.h>
#include "env.h"
#include "dir_managing.h"
using namespace std;		// ici on a le droit aux espaces de noms

/**********************************************/
/* Ce fichier définit les implémentations des */
/* objets utilisés par la simulation en l'ADN */
/*                                            */
/**********************************************/


/*****************/
/* CONFIGURATION */
/*****************/
// Issu d'un .;config

// -- Constantes de l'environnement (dans un .config) -- //
#define E_init_ 300 // "énergie initiale"
#define E_sol_ 1 // "énergie du soleil"  (todo: en vrai mettre bcp plus petit)
#define E_mort_ 0 // seuil minimal
#define A_mort_ 1000 //âge maximal

// -- Cout du déplacement -- //
#define costMove_cste_ 10 // cout constant

// -- Cout de l'envoi de message -- //
#define costSendMessage_mess_taille_ 0.1 // coeff devant taille du message (deja *nb_directions)
#define costSendMessage_cste_ 5	 // + cout constant

// -- Cout de l'envoi d'nrj -- //
#define costSendEnergy_nrj_quantite_ 1.05  // coeff devant quantité d'nrj (deja *nb_directions)
#define costSendEnergy_cste_ 5		   // + cout constant

// -- Cout de la duplication -- //
#define costDuplicate_nrj_quantite_ 1.1  // coeff devant quantite d'nrj
#define costDuplicate_taille_code_ 10.5    // + coeff devant taille du code
#define costDuplicate_cste_ 30	    // + cout constant

// -- le pourcentage (entre 0 et 100) de l'nrj qu'on récupère des morts -- //
#define nrj_recup_pourcentage_ 50

// -- Taille du terrain -- //
// ATTENTION: vérifier la compatibilité avec position.cpp :!!
int x_max = 30;
int y_max = 30;

// -- Calcul des scores -- //
#define coeff_nrj 1		// fois l'nrj de chaque cellule
#define coeff_gene 2		// fois la génération de chaque cellule
#define coeff_tue 50		// fois le nombre de cellules tuées par une cellule de la famille
#define coeff_morte -49		// fois le nombre de cellules mortes dans la famille
#define coeff_nb 10		// fois le nombre de cellule dans la famille

// -- Constantes de l'implem -- //
#define nb_essai_random_ 20  // le nombre de fois que l'on essaye d'intégrer une cellule aléatoirement
#define nb_familles_par_proprietaire 5 // le nombre de familles que peut posséder un joueur


/**************************/
/* Un log d'erreur global */
/* et une fonction qui    */
/* rattrape les erreurs   */
/**************************/
// -- l'objet qui log les messages dans le code -- //
// Déclaré dans serveur.cpp
extern Log log_er;

// -- Vers le fichier qui contiendra le log des erreurs -- //
// Déclaré dans serveur.cpp
extern fstream filestr;

// -- Fonction qui rattrape l'erreur (log juste l'erreur) -- //
void err_ratrap (int err) {
  static string erreurs[] = {"", "", "SIGINT", "", "SIGILL", "", "SIGABRT", "", "SIGFPE", "", "", "SIGSEGV", "", "", "", "SIGTERM"};
  // Si l'erreur est différente d'un arrêt commandé par controle C
  if (err == 2)			// 2 = controle C
    {
      cout << "Arrêt demandé..." << endl;
      filestr << "Arrêt demandé..." << endl;      
      exit(0);
    }
  else
    {
      // On output le log de l'erreur 
      filestr << "||||||||||||||||||||||||||||||||||||||| ERREUR ||||||||||||||||||||||||||||||||||||||||" << endl;
      filestr << "|||||| Le code de l'erreur est: " << erreurs[err] <<"  ||||||||||||||||||||||||||||||||||||||||||||||||||";
      filestr << endl << log_er;
      filestr << endl;
      usleep(100000);
    }
};



/***********************/
/* OBJET ENVIRONNEMENT */
/***********************/

Env::Env()
  : p_E_init (E_init_),
    p_E_sol (E_sol_),
    p_E_mort (E_mort_),
    p_A_mort (A_mort_)
{
  // -- Ou ouvre le fichier du log des erreurs -- //
  filestr.open ("log_erreur", fstream::out /*| fstream::app*/);
  if ((not(filestr.is_open()))) cerr << "Attention, pas de log d'erreur" << endl;

  // -- On rattrape toutes les erreurs de l'environnement au sein de l'environnement en outputant dans filestr -- //
  filestr << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;
  filestr << "    DEBUT D'UNE EXECUTION" << endl;
  filestr << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;
/*  filestr << "Codes des erreurs: " << "0, 1, etc..." << endl << "SIGABRT" << endl << "SIGFPE" << endl << "SIGILL"
       << endl << "SIGINT" << endl << "SIGSEGV" << endl << "SIGTERM" << endl;
*/
  filestr << "Erreurs rattrapées : SIGINT, SIGILL, SIGABRT, SIGFPE, SIGSEGV, SIGTERM" << endl;  
  filestr << "Etat du log: " << endl;
  filestr << log_er;
  filestr << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;
  filestr << "$$ DEBUT DE L'EXECUTION $$ " << endl;
  signal(SIGABRT, err_ratrap);
  signal(SIGFPE, err_ratrap);
  signal(SIGILL, err_ratrap);
  signal(SIGINT, err_ratrap);
  signal(SIGSEGV, err_ratrap);
  signal(SIGTERM, err_ratrap);
};

Env::~Env() {			// déconstructeur
  filestr << endl << endl << endl;
  filestr.close();		// On ferme le fichier de log erreur
  // les objets sont freezés donc normalement tout est ok
};

// -- Numéro de famille disponible pour un client donné (-1 si aucun)
int Env::get_free_family (int proprietaire_) {
  map<int,queue<int> >::iterator familles_libres = joueur_fami_libres.find(proprietaire_); // on cherche le joueur dans le mapping
  if (familles_libres == joueur_fami_libres.end())
    return 0;
  if ((*familles_libres).second.empty()) // le joueur n'a plus le droit de rajouter une famille !
    {	      
      return -1;
    }
  else
    {
      return (*familles_libres).second.front(); // le numéro d'une famille qui n'existe pas sur le terrain
    }
}


// -- Ajout d'une cellule dans l'environnement (à terme, ne plus utilise) -- //
bool Env::add_cell(Cell* cell, Position pos) {
  if (map_pos.find(pos) == map_pos.end()) {
    // il n'y a personne sur cette case: c'est cool
    cell->pos() = pos;
    les_cell.push_back(cell);
    // mettre à jour findcell
    map_pos.insert(pair<Position,Cell*>(pos, cell));
    return true;
  }
  else {
    return false;
  }
};

// -- Ajout d'une cellule dans l'environnement avec chargement et placement aléatoire -- //
bool Env::add_cell_auto(const char *nom_fichier, int proprietaire_) {
  // Recherche d'une position libre (de façon "aléatoire")
  int flag = 1;
  int x, y;
  log_er << "Début de add_cell_auto: Recherche d'une position libre.\n";
  for (int j = 0; j < nb_essai_random_ && flag; j++)
    {
      x = rand() % x_max;
      y = rand() % y_max;
      Position pos(x,y);
      if (map_pos.find(pos) == map_pos.end()) {
	// il n'y a personne sur cette case: c'est cool
	// on a trouvé une case !
	flag = 0;
	break;
      }
    }
  if (flag) {			// alors malgrès tous les essais, on a pas trouvé de place
    return false;
  }
  else
    {
      log_er << "Position trouvée ... \n";
      // WARNING OPTIM : au lieu de find à chaque fois l'élément dans le mapping, il faudrait utiliser un itérateur et ne faire la recherche qu'une fois
      // Même warning pour rm_cell !!
      Position pos(x,y);      
      Cell *pointeur (0);		// un pointeur vers une cellule (que l'on va créer plus tard)
      // on test si le propriétaire a déjà des cellules
      map<int,queue<int> >::iterator familles_libres = joueur_fami_libres.find(proprietaire_); // on cherche le joueur dans le mapping
      if (familles_libres == joueur_fami_libres.end()) // si le propriétaire n'avait pas déjà des cellules
	{
	  log_er << "L'utilisateur n'avait pas de cellules auparavant.\n";
	  pointeur = new Cell(nom_fichier, p_E_init, proprietaire_, 0); // le numéro de la famille est 0
	  queue<int> q;
	  joueur_fami_libres.insert(pair<int,queue<int> >(proprietaire_, q)); // on rajoute le joueur au mapping des familles libres avec toutes les familles libres sauf 0
	  for (int i = 1; i < nb_familles_par_proprietaire; i++) // (0 est déjà pris)
	    {
	      joueur_fami_libres[proprietaire_].push(i);
	    }
	  joueur_scores.insert(pair<int,int**>(proprietaire_, NULL)); // on lui rajoute un tableau de score par famille
	  int** tab = new int*[nb_familles_par_proprietaire];	// on créer un vai pointeur vers un tableau de familles de tableaux
	  for (int i = 0; i < nb_familles_par_proprietaire; i++)
	    {
	      tab[i] = new int[2];
	      tab[i][0] = 0;
	      tab[i][1] = 0;
	    }
	  joueur_scores[proprietaire_] = tab; // on fait pointer notre structure sur le proprietaire vers ce tableau
	}
      else			// le joueur a déja  des cellules
	{
	  log_er << "Le joueur avait déjà des cellules auparavant.\n"; 
	  if ((*familles_libres).second.empty()) // le joueur n'a plus le droit de rajouter une famille !
	    {	      
	      return false;
	    }
	  else
	    {
	      log_er << "Le joueur a le droit de rajouter une cellule.\n";
	      // la file dans joueur_fami_libres n'est pas vide !!
	      int num_famille = joueur_fami_libres[proprietaire_].front(); // le numéro d'une famille qui n'existe pas sur le terrain
	      joueur_fami_libres[proprietaire_].pop(); // ce numéro n'est plus libre
	      log_er << "Extraction du num de famille, ok, on va créer la cellule.\n";
	      pointeur = new Cell(nom_fichier, p_E_init, proprietaire_, num_famille);
	      // on initialise score et nombre de mortes
	      joueur_scores[proprietaire_][num_famille][1] = 0;
	      joueur_scores[proprietaire_][num_famille][0] = 0;
	    }
	}      
      log_er << "Mise à jour de la pos, de les_cell et de map_pos.\n";
      // mettre à jour la position de la cellule (pour elle)
      pointeur->pos() = pos;	
      // ajouter cette cellule aux cellules connues de l'environnement
      les_cell.push_back(pointeur);
      // mettre à jour la position de la cellule (pour l'environnement)
      map_pos.insert(pair<Position,Cell*>(pos, pointeur));
      // on a trouvé une celule !
      return true;
    }
  log_er.rm();
};

// -- Ajout d'une cellule dans l'environnement avec chargement et info de la mere -- //
Cell* Env::add_cell_pos(Cell* mother, const char* nom_ss_fct, int *tableau_args, int* tableau_vars, int debut, assign ass, int taille_code, int nrj_, Position pos)
{
  if (map_pos.find(pos) == map_pos.end())
    {
      // il n'y a personne sur cette case: c'est cool
      Cell* pointeur = new Cell(mother, nom_ss_fct, tableau_args, tableau_vars, debut, ass, taille_code, nrj_);
      // ce constructeur rajoute tt ce qu'il faut, gère l'arbre généalogique, p_cousines (pour freeze) etc..
	// mettre à jour la position de la cellule (pour elle)
	pointeur->pos() = pos;	
	// ajouter cette cellule aux cellules connues de l'environnement
	les_cell.push_back(pointeur);
	// mettre à jour la position de la cellule (pour l'environnement)
	map_pos.insert(pair<Position,Cell*>(pos, pointeur));
      return pointeur;
    }
  else
    return NULL;		// sinon on renvoie le pointeur NULL (0) (on devra teser la valeur renvoyer par cette fonction !)
};


// -- Suppression d'une cellule de l'environnement-- //
// Attention, c'est à l'appellant de supprimer la cellule de next_pos !
bool Env::rm_cell(Cell* cell, int flag) {
  // Partout où cell à un pointeur (pour le moment dans les_cell: delete le pointeur)
  // suppression de les_cell (méthode actuelle est super lourde, voir commentaire plus bas)
  for (int i = 0; i < (int) (les_cell.size()); i++) //
    {
      if (cell == les_cell[i]) {
	if (flag)
	  les_cell.erase(les_cell.begin() + i); // on supprime carrément cet élément du vecteur. Attention: introduit un décalage pour les prochains !!!!
	else
	  les_cell[i] = NULL;    // on échange pointeur et NULL pour ne pas introduire de décalage dans le tableau (car on peut rm au milieu de calc_tour !!))
	break;			 // dans tous les cas, on s'arrête là !
      }
    }
  // suppression de map_pos
  map_pos.erase(cell->pos()); 	// on supprime avec la clé
  // suppression de joueur_scores
  if ((cell->taille_famille()) == 1) // si la cellule est la dernière de sa famille
    {
      if (joueur_fami_libres[cell->proprietaire()].size() == nb_familles_par_proprietaire - 1) // si le propriétaire n'avait plus qu'une famille
	{
	  joueur_fami_libres.erase(cell->proprietaire()); // on supprime cette famille du maping
	  delete joueur_scores[cell->proprietaire()];	  // on delete le tableau de taille 2
	  joueur_scores.erase(cell->proprietaire()); // on supprime cette famille du maping
	}
      else
	{
	  // il faut faire plusieurs choses: supprimer cette famille de joueur_scores mais en libérant ce numéro de famille
	  map<int,int**>::iterator score = joueur_scores.find(cell->proprietaire());
	  (*score).second[cell->numero_famille()][0] = 0; // on nettoie les scores de cette famille
	  (*score).second[cell->numero_famille()][1] = 0; // on nettoie les scores de cette famille
	  joueur_fami_libres[cell->proprietaire()].push(cell->numero_famille()); // le numéro de cette famille devient libre (il sera ré-utilisé en dernier grâce à la file)
	}
    }
  // suppression du pointeur
  delete cell; 
  return true;			// tout est ok
};
  // commentaire sur cette fonction: le prblm c'est que si la cellule un attribut rang_dans_cell alors quand on supprime une cellule, toutes les cellules plus loins dans le vecteur
  // sont décalées donc il faudrait mettre à jour l'attribut de ces cellules: c'est aussi très lourd
  // le seul moyen: trouver une structure qui préserve les clés (par exemple un map identifiant_unique_de_la_cellule -> pointeur vers la cellule) avec un attribut dans la cellule


// -- Suppression d'une famille de cellule -- // 
void Env::rm_famille(int proprietaire, int famille) {
  // WARNING OPTIM au lieu de continuer, s'arrêter quand taille famille == 1
  // On cherche les cellules conernées (prop et famille) dans les_cell
  log_er << "Début de rm_famille.\n";
  for (int i=0; i < (int)les_cell.size(); i++)
    {
      if ((les_cell[i]->proprietaire() == proprietaire) && (les_cell[i]->numero_famille() == famille))
	{
	  map_pos.erase(les_cell[i]->pos());
	  if ((les_cell[i]->taille_famille()) == 1) // si la cellule est la dernière de sa famille
	    {
	      if (joueur_fami_libres[les_cell[i]->proprietaire()].size() == nb_familles_par_proprietaire - 1) // si le propriétaire n'avait plus qu'une famille
		{
		  joueur_fami_libres.erase(les_cell[i]->proprietaire()); // on supprime cette famille du maping
		  joueur_scores.erase(les_cell[i]->proprietaire()); // on supprime cette famille du maping
		}
	      else
		{
		  // il faut faire plusieurs choses: supprimer cette famille de joueur_scores mais en libérant ce numéro de famille
		  map<int,int**>::iterator score = joueur_scores.find(les_cell[i]->proprietaire());
		  (*score).second[les_cell[i]->numero_famille()][0] = 0; // on nettoie les scores de cette famille
		  (*score).second[les_cell[i]->numero_famille()][1] = 0; // on nettoie les scores de cette famille
		  joueur_fami_libres[les_cell[i]->proprietaire()].push(les_cell[i]->numero_famille()); // le numéro de cette famille devient libre (il sera ré-utilisé en dernier grâce à la file)
		}
	    }
	  // suppression du pointeur
	  delete les_cell[i]; 
	  les_cell.erase(les_cell.begin() + i); // WARNING : delete + erase ??
	  i --;			// car on a erase cet élément dans les_cell
	}
    }
  log_er.rm();
};

// -- Suppression de toutes les cellules -- //
void Env::clear_all_cell() {
  while (not(les_cell.empty()))
    {
      rm_cell(les_cell[0], 1);	// on supprime vraiment car sinon le while ne termine pas !!
    }
};

// -- donne l'adresse de la cellule d'une case ou NULL -- //
Cell* Env::findCell(Position p) {
  map<Position,Cell*>::iterator val = map_pos.find(p);
  if (val != map_pos.end()) {
    return ((*val).second);
  }
  else {
    return NULL;
  }
};

void Env::update_pos() {
  // Rappel de la règle: quand il y a un conflit (plrs cellules dans un vecteur en valeur d'une clé position de next_pos) alors
  // on prend celle qui  a le plus d'nrj, les autres meurt. En cas d'égalité, elles meurent toutes !
  // ATTENTION: on sppose que la position de next_pos est != de l'ancienne !!
  map<Position,vect_de_cell_2>::iterator it; // l'itérateur qui nous permettra de balayer next_pos
  map<Position,Cell*>::iterator it_pos; // l'itérateur qui nous permettra de travailler dans map_pos
  while(not(next_pos.empty()))
    {
      it = next_pos.begin();
      // Termine car à la fin du while on erase au niveau de it !
      // on rajoute la cellule (potentielle) qui se trouve sur la pos aux prétendants SEULEMENT SI ELLE NE BOUGE PAS      
      it_pos = map_pos.find((*it).first); // on cherche une cellule se trouvant à la posituon *it
      if (it_pos != map_pos.end()) {
	if (not((*it_pos).second->a_bouge())) { // si elle ne bouge pas alors c'est une prétendante à la position *it.first sinon on ne la prend pas en compte ici 
	  // alors on rajoute le prétendant à la position *it dans next_pos TODO: faire ça plus efficacement : si y'en a qu'un alors on modifie son truc et c'est tt
	  (*it).second.push_back((*it_pos).second);
	}
      }
      int taille = (*it).second.size();
      if (taille == 1)
	{
	  map_pos.erase(((*it).second[0])->pos());      	                 // on supprime l'ancienne position connue par l'environnement de la cellule  (on suppose que la nouvelle pos est !=)
	  ((*it).second[0])->pos() = (*it).first;                                // on met à jour la position de cet unique élément dans lui même
	  ((*it).second[0])->a_bouge() = 0;                                      // on met à jour le flag de depalacement
	  map_pos.insert(pair<Position,Cell*>(((*it).first), (*it).second[0]));  // on met à jour la position de cet unique élément dans l'env
	}
      else if (taille > 1)
	{
	  // --Gestion des conflits -- //
	  int max_nrj = 0;
	  int rang_meilleur = 0;
	  int flag_egalite = 0;	// si il passe à 1 et qu'il y reste c'est qu'il y a égalité: tout le monde meurt 
	  for (int j = 0; j < (int)(*it).second.size(); j++)
	    {
	      int nrj = (*it).second[j]->nrj();
	      if (max_nrj < nrj) { 
		max_nrj = nrj;
		rang_meilleur = j;
		flag_egalite = 0;		// il n'y a plus d'égalité
	      }
	      else if (max_nrj == nrj) {
		flag_egalite = 1;		// peut-être une égalité ?
	      }
	    }
	  if (flag_egalite) {		// il faut aussi tuer la "meilleure" cellule !
	    // on comptabilise cette mort comme une mort non-naturel pour la famille de la cellule
	    joueur_scores[(*it).second[rang_meilleur]->proprietaire()][(*it).second[rang_meilleur]->numero_famille()][1] ++;
	    rm_cell((*it).second[rang_meilleur], 1); // on supprime l'élement de les_cell (flag = 1)
	  }
	  else {			// sinon alors il faut mettre à jour la position de la gagnante (dans la cellule et au niveau de l'env))
	    map_pos.erase((*it).second[rang_meilleur]->pos());	                                  // on supprime l'ancienne position connue par l'environnement de la cellule
	    ((*it).second[rang_meilleur])->a_bouge() = 0;                                         // on met à jour le flag de depalacement
	    (*it).second[rang_meilleur]->pos() = (*it).first;                   	          // on met à jour la position de cet unique élément dans lui même
	    map_pos.insert(pair<Position,Cell*>((*it).first, (*it).second[rang_meilleur]));       // on met à jour la position de cet unique élément dans l'env
	  }
	  int nrj_recup = 0;
	  for (int j = 0; j < (int)(*it).second.size(); j++) // il faut tuer toutes les autres cellules et récupérer une partie de leur nrj
	    {
	      if (j != rang_meilleur) { // MEURT ^^
		nrj_recup += max(0,(*it).second[j]->nrj()); // recup de l'nrj
		// on comptabilise cette mort comme une mort non-naturel pour la famille de la cellule
		joueur_scores[(*it).second[j]->proprietaire()][(*it).second[j]->numero_famille()][1] ++;
		rm_cell((*it).second[j], 1);	     // mort de la cellule,  on supprime l'élement de les_cell (flag = 1)
	      }
	    }
	  if (not(flag_egalite)) {
	    nrj_recup = (nrj_recup * (nrj_recup_pourcentage_)) / 100;   // warning faire gaffe au max_int ici
	    (*it).second[rang_meilleur]->nrj_receive(nrj_recup);        // on donne cette nrj à la gagnante
	    (*it).second[rang_meilleur]->nrj_update();		        // !! on lui donne TOUT de suite (et pas au tour suivant car update_nrj est appelé avant update_pos) !!
	    // on met à jour le nombre de cellules tués par rang_meilleur
	    (*it).second[rang_meilleur]->tue((*it).second.size() - 1); // tout le monde sauf rang_meilleur
	  }
	}
      next_pos.erase(next_pos.begin());  // on supprime cette position de next_pos (plus rien à faire)
    }
  next_pos.clear();		// Est-ce vraiment utile ? todo enfin c'est pas trop grave
};


// -- faire calculer toutes les cellules --//
void Env::calc_tour() {
  // on calcule tous les ADNS des cellules
  int nombre_a_traiter = (int)les_cell.size(); // on retient cette taille car le vecteur peut potentiellement croître à cause de la duplication mais on ne veut pas déjà exécuter ces nouvelles cellules
  // Bien sûr il ne faut pas non plus que l'on supprime des éléments du vecteur au milieu de la boucle for (or rm_cell ne fait que remplacer le pointeur par NULL, donc tout est ok)
  // A ce moment là, il ne faut pas qu'il y ait de NULL dans les_cell donc de rm_cell depuis le dernier nettoyage de les_cell !!
  //   Pas de problème car update_pos utilise rm_cell avec flag à 1 et avant nettoyage à la volée
  for (int i=0; i < nombre_a_traiter; i++)
    {
      if (les_cell[i]->age() >= p_A_mort || les_cell[i]->nrj() < p_E_mort) // si trop grande ou pas assez d'nrj
	{
	  log_er << "Dans calc_tour, suppression de la cellule " << i << "\n";
	  // C'est une mort naturelle (ne pas comptabiliser dans les scores)
	  rm_cell(les_cell[i], 0); // flag = 0: pas de décalage mais apparition de cases à NULL
	  log_er.rm();
	}
      else
	{
	  log_er << "Dans calc_tour, calcul la cellule " << i << "\n";
	  (les_cell[i])->action(*this);
	  (les_cell[i])->grow(p_E_sol);   // !! grow()  "ensoleil" déjà la cellule (+ E_soleil)
	  log_er.rm();
	}
    }
  log_er.rm();			// on supprime le log_er (on commence une nouvelle phase)
  // pour chaque cellule:
  log_er << "Dans calc_tour, on met à jour les messages et l'énergie ou supprime (message pour ce cas)" << "\n";
  for (int i=0; i < (int)les_cell.size(); i++)
    {
      if (les_cell[i])		// si le pointeur est un bien un pointeur vers une cellule et non NULL
	{
	  // on met à jour les "boîtes eux lettres" (les messages envoyés vont dans la boite de réception)
	  les_cell[i]->mess_update_end();
	  // on met à jour l'nrj (reçu)) 
	  les_cell[i]->nrj_update();
	}
      else
	{
	  log_er << "Cas où il faut faire de la place dans les_cell à la suite d'un rm_cell pour " << i << "\n";
	  les_cell.erase(les_cell.begin() + i);
	  i --;			// on décrémente i car du coup, on a un nouvel élément en i (à cause du erase)
	}
    }
  log_er.rm();
  log_er << "Dans calc_tour, début de update_pos." << "\n";
  // on gère les conflits (plusieurs cellules sur une même case) et fait bouger les cellules
  update_pos();
  // On laisse next_pos propre (vide) et cohérence entre map_pos et les pos des cell
  // update_pos n'appel rm_cell qu'avec flag = 1 donc laisse  les_cell tout propre
  // -- Désormais, on compte le spoints -- //
  // on initialise les scores à 0 + points perdues à cause des cellules mortes (todo c'est lourd, réfléchir à mieux)
  log_er.rm();
  log_er << "Dans calc_tour, début du calcul de score." << "\n";
  map<int,int**>::iterator score = joueur_scores.begin();
  while (score != joueur_scores.end()) // pour chaque propriétaire
    {
      for (int i =0; i < nb_familles_par_proprietaire; i++) // pour chaque famille de ce propriétaire
	{
	  (*score).second[i][0] = (*score).second[i][1] * coeff_morte; // score = perte dues aux cellules mortes (le coeff est <0)
	}
      score++;
    }
  log_er << "Mainenant, mise à jour des scores en balayant les_cell." << "\n";
  // on calcule les scores en balayant encore une fois les_cell
  for (int i = 0; i < (int)les_cell.size(); i++)
    {
      joueur_scores[les_cell[i]->proprietaire()][les_cell[i]->numero_famille()][0] +=
	les_cell[i]->nrj() * coeff_nrj + les_cell[i]->gen() * coeff_gene + les_cell[i]->a_tue() * coeff_tue + coeff_nb; // on a une cellule donc + coeff_nb
    }
  log_er.rm();
};

// -- Méthodes pour la sauvegarde/chargement de l'environnement -- //
// --------------------------------------------------------------- //
void Env::save_etat (char* fichier_sauvegarde) {
  fstream file;
  file.open (fichier_sauvegarde, fstream::out);
  if ((not(file.is_open()))) 
    {
      cerr << "Sauvegarde échouée: problème à l'ouverture du fichier." << endl;
      log_er << "Sauvegarde échouée: problème à l'ouverture du fichier." << "\n";
      raise(SIGSEGV);
    }
  // On sauvegarde dans l'ordre de les_cell: proprietaire, position actuelle, niveau d'nrj, nom_fichier en ne gardant qu'une cellule par famille.  
  // Comme ça, on re-implantera une cellule souche par famille.
  // On se sert de joueur_scores pour cocher les familles déjà sauvcegardées (-1 dans la première case)
  for (int i = 0; i < (int)les_cell.size(); i++)
    {
      int famille = les_cell[i]->numero_famille();
      int proprietaire = les_cell[i]->proprietaire();
      if (joueur_scores[proprietaire][famille][1] != -1) // on peut utiliser [] dans le map car on sait que l'entrée existe !
	{
	  // On doit sauvegarder cette cellule
	  file << proprietaire << " " << les_cell[i]->pos_x() << " " << les_cell[i]->pos_y() << " " << les_cell[i]->nrj() << " " << les_cell[i]->get_nom_so();
	  file << endl;
	  joueur_scores[proprietaire][famille][1] = -1; // On ne sauvegardera plus de cellules de cette famille
	}
    }
  file.close();
};

void Env::load_etat (char* fichier_sauvegarde) {
  fstream file;
  file.open (fichier_sauvegarde, fstream::in);
  if ((not(file.is_open()))) 
    {
      cerr << "Sauvegarde échouée: problème à l'ouverture du fichier." << endl;
      log_er << "Sauvegarde échouée: problème à l'ouverture du fichier." << "\n";
      raise(SIGSEGV);
    }
  // On charge: proprietaire, position actuelle, niveau d'nrj, il faut swaper la position et donner l'ancienne quantité d'nrj EN PLUS.
  // On fait ça tant qu'il y a du texte.
  string line;
  while(getline(file, line)) // tant qu'on peut lire l'input d'une cellule
    {
      stringstream linestream(line);
      int prop, x, y, nrj;
      char* nom_so = new char[100]; // ATTENTION, ce char n'est jamais free Mais normalement on ne fait qu'un load donc pas de fuite mémoire !!
      linestream >> prop >> x >> y >> nrj;
      // on doit lire après l'espace qui sut l'nrj !
      linestream.seekg(1, ios_base::cur);
      linestream.get(nom_so, 100);
      Position pos(x,y);
      if (add_cell_auto((char*)nom_so, prop)) // si l'ajout s'est déroulé correctement alors on sait que la cellule se trouve dans la dernière case de les_cell
	{
	  swap_pos(les_cell[les_cell.size() - 1], pos);
	  les_cell[les_cell.size() - 1]->nrj_receive(nrj);
	  les_cell[les_cell.size() - 1]->nrj_update();
	}
      else
	{
	  log_er << "Dans load_etat..... Erreur dans le chargement de l'état: ajout de cellule impossible. Prop = " << prop << " pos = " << x << "," << y << ".\n";
	  cerr << "Erreur dans le chargement de l'état: ajout de cellule impossible. Prop = " << prop << " pos = " << x << "," << y << ".\n";
	}
    }
  file.close();
};


// -- Recherche de cellule en une case -- //
void Env::voir(Cell* myself, bool tab_vide[]) {
  // Attention: quand on appel cette méthode, tab_vide doit être de taille 8 !!
  for (int i = 0; i< 8; i++)
    {
      if (findCell((myself->pos() + ((Direction) i))))
	tab_vide[i] = true;
      else
	tab_vide[i] = false;
    }
};

int Env::see(Cell* myself){
  int res;
  bool *tmp = new bool [8];
  voir(myself, tmp);
  res = dir_vect_to_int(tmp);
  return res;
}

// -- Constantes de l'environnement -- //
int Env::E_init() {return(p_E_init);}; // "énergie initiale"
int Env::E_sol() {return(p_E_sol);}; // "énergie du soleil"
int Env::E_mort() {return(p_E_mort);}; // seuil minimal
int Env::A_mort() {return(p_A_mort);}; //âge maximal
void Env::fix_A_mort(int new_A) {
  p_A_mort = new_A;
};

// -- les fonctions de couts des actions -- //
// ---------------------------------------- //
int Env::costSendMessage(bool vect_dir[8], Message mess) {
  int nb_direction = 0;		// le nombre de directions selectionnées
  for (int j = 0; j<8; j++)
    {
      if (vect_dir[j]) nb_direction ++ ;
    }
  int taille_mess = (int)(log10 (1+mess));
  return((int)(costSendMessage_mess_taille_ * (float)(nb_direction *  taille_mess)) + costSendMessage_cste_);
};

int Env::costSendEnergy(bool vect_dir[8], int quantite_nrj) {
  int nb_direction = 0;		// le nombre de directions selectionnées
  for (int j = 0; j<8; j++)
    {
      if (vect_dir[j]) nb_direction ++ ;
    }
  return((int)(costSendEnergy_nrj_quantite_ * (float)(nb_direction * quantite_nrj)) + costSendEnergy_cste_);
};

int Env::costDuplicate(int taille_fct, int fwEnergy) {
  return((int)(costDuplicate_nrj_quantite_*(float)fwEnergy) + (int)(costDuplicate_taille_code_*(float)taille_fct) + costDuplicate_cste_);
};

int Env::costMove() {		/* c'est une cste */
  return(costMove_cste_);
};

// -- les fonctions réalisant (si c'est possible) les actions demandés par les cellules */
// ------------------------------------------------------------------------------------ //

/* Il faut donner myself en argument car l'environnement doit vérifier que la
   cellule peut effectuer l'action et auquel cas, modifier ses variables */
void Env::appelPreambule(Cell *myself, assign ass) { // todo pas testé
  if (ass.size() != 0) {
    if (INT_MIN == (myself->mem(ass[0].real()))) // Si la case n'avait pas été déjà assignée, alors (TRES TRES MOCHE todo )
      {
	for (int i=0; i < (int)ass.size(); i++)
	  {
	    myself->mem(ass[i].real()) = ass[i].imag();
	  }
      }
  }
};

void Env::sendMessage(Cell *myself, bool vect_dir[8], Message message) { // todo: pas testé
  if (myself->nrj_load(costSendMessage(vect_dir, message))) {
    // On a fait perdre l'nrj à la cellule, on peut accéder à sa requête
    for (int i = 0; i<8; i++)
      {
	if (vect_dir[i]) {
	  Cell *voisin = findCell(myself->pos() + (Direction) i); // todo: besoin de freeze ? non
	  if (voisin)
	    voisin->mess_store(message, (Direction)(7 - i)); // !!! 7 - i est la direction opposé à i avec nos conventions !!!
	}
      }
  }
  // Si, il n'y a pas assez d'nrj: ne rien faire! (nrj_load lui a déjà fait perdre une petite quantité d'nrj (cf CONFIG de cell.cpp))
};

void Env::sendEnergy(Cell *myself, bool vect_dir[8], int quantite_nrj) { // todo; pas très bien testé
  if (myself->nrj_load(costSendEnergy(vect_dir, quantite_nrj))) {
    // On a fait perdre l'nrj à la cellule, on peut accéder à sa requête
    if (quantite_nrj > 0)	// sinon alors on ne fait rien (mauvais comportement !)
      {
	for (int i = 0; i<8; i++)
	  {
	    if (vect_dir[i]) {
	      Cell *voisin = findCell(myself->pos() + (Direction) i); // todo: besoin de freeze ? non
	      if (voisin)
		voisin->nrj_receive(quantite_nrj);
	    }
	  }
      }
  }
  // Si, il n'y a pas assez d'nrj: ne rien faire! (nrj_load lui a déjà fait perdre une petite quantité d'nrj (cf CONFIG de cell.cpp))
};

void Env::Duplication(Cell *myself, char* nom_ss_fct, int taille_ss_fct, Direction dir, int fwEnergy, int *tableau_valeurs_args, int *tableau_indices_vars, int debut, assign ass) {
  if (myself->nrj_load(costDuplicate(taille_ss_fct, fwEnergy)))
    {
      // On a fait perdre l'nrj à la cellule, on peut accéder à sa requête
      // si jamais la position est occupée alors le mec perd l'nrj mais ne se duplique pas (TOUT est dans add_cell_pos !!)
      if (fwEnergy >= p_E_mort)	// si l'énergie est assez grande pour faire vivre une cellule alors c'est bon
	{
	  Position pos = myself->pos() + dir;
	  add_cell_pos(myself, nom_ss_fct, tableau_valeurs_args, tableau_indices_vars, debut, ass, taille_ss_fct, fwEnergy, pos);
	  // tout est dans add_cell_pos bien sûr
	}
    }
  // Si, il n'y a pas assez d'nrj: ne rien faire! (nrj_load lui a déjà fait perdre une petite quantité d'nrj (cf CONFIG de cell.cpp))
};

void Env::Deplacement(Cell *myself, Direction dir) {  
  if (myself->nrj_load(costMove())) {
    // On a fait perdre l'nrj à la cellule, on peut accéder à sa requête
    Position new_pos = (myself->pos()) + dir;
    if (next_pos.find(new_pos) == next_pos.end()) {
      next_pos.insert(pair<Position,vect_de_cell_2>(new_pos,vector<Cell*> (1,myself))); // on ajoute un vecteur de taille 1 contenant le pointeur aux préntendants à cette position
    }
    else {
      next_pos[new_pos].push_back(myself); // on rajoute cette cellule aux prétendants à cette position
    }
    myself->a_bouge() = 1;	// on exprime ici que la cellule veut bouger (utile dans update_pos )
  }
  // Si, il n'y a pas assez d'nrj: ne rien faire! (nrj_load lui a déjà fait perdre une petite quantité d'nrj (cf CONFIG de cell.cpp))
};


// -- Affichage de l'environnement -- //
// ---------------------------------- //

// -- Ecriture du terrain (pour l'affichage sur le client) -- //
ostream& affiche_terrain(ostream& output, Env& e) {
  log_er  << "Début de affiche_terrain.\n";
  // Affichage du terrain
  int nb_cel = e.les_cell.size();
  output << nb_cel << "#" << endl;
  for (int i = 0; i < nb_cel; i++)
    {
      output << e.les_cell[i]->proprietaire() << " " << e.les_cell[i]->numero_famille() << " " << e.les_cell[i]->nrj() << " ";
      output << e.les_cell[i]->pos_x() << " " << e.les_cell[i]->pos_y();
      output << endl;
    }
  // Affichage des scores
  int nb_propis = e.joueur_scores.size();
  map<int,int**>::iterator score = (e.joueur_scores).begin();
  output << nb_propis << " " << nb_familles_par_proprietaire << endl;
  while (score != e.joueur_scores.end())
    {
      output << get_client_by_id((*score).first) << endl;
      for (int i = 0; i < nb_familles_par_proprietaire; i++) 
	{
	  output << (*score).second[i][0] << " ";
	}
      output << endl;
      score ++;
    }
  log_er.rm();
  return output;
}

// -- Fonctions pour afficher pour debugger -- //
// affiche_cell_decalage (et affichage_decalage se trouve dans cell.cpp) //
ostream& affichage_decalage_2(ostream& output, vector<int>* nb_decalage, int* dec) {
  string decalage = "    ";
  if (0 < nb_decalage->size()) {
    for (int i=1; i<(int)nb_decalage->size(); i++)
      {
	int dec = (*nb_decalage)[i] - (*nb_decalage)[i-1];
	for (int j=0; j<dec; j++)
	  {
	    cout << decalage;
	  }
	cout << "|";
      }
    for (int i=0; i < (*dec) - (*nb_decalage)[(int)(*nb_decalage).size() - 1]; i++)
      {
	cout << decalage;
      }  
  }
  else {
    for (int i=0; i < (*dec); i++)
      {
	cout << decalage;
      }
  }
  return output;
};

ostream& affiche_orga_rec(ostream& output, Cell* cell, int* nb_dec, vector<int>* nb_decalage, int flag) {
  // le flag: =1, la cellule est la dernière fille de sa mère et 0 sinon
  affichage_decalage_2(output, nb_decalage, nb_dec);
  if (flag) {
    cout << "\\--> ";
  }
  else {
    cout << "--->";
  }
  cout << endl;
  (*nb_dec) += 1;
  affiche_cell_decalage(output, &(*cell), nb_decalage, nb_dec, flag);
  if (0 < cell->nb_filles()) {
    affichage_decalage_2(output, nb_decalage, nb_dec);
    cout << "|____" << endl;
    (*nb_dec) += 1;
    affichage_decalage_2(output, nb_decalage, nb_dec);
    cout << "|" << endl;
    if (1 < cell->nb_filles()) {
      nb_decalage->push_back(*nb_dec);
      affiche_orga_rec(output, cell->fille(0), nb_dec, nb_decalage, 0);
      for (int i=1; i<(int)cell->nb_filles() -1 ; i++)
	{
	  affichage_decalage_2(output, nb_decalage, nb_dec);
	  cout << endl;
	  affiche_orga_rec(output, cell->fille(i), nb_dec, nb_decalage, 0);
	}
      nb_decalage->pop_back();	// on ne veut plus afficher de | quand on affiche la dernière fille !
    }
    int cond  = cell->nb_filles() > 1;
    affiche_orga_rec(output, cell->fille(cell->nb_filles()-1), nb_dec, nb_decalage, cond);
    (*nb_dec) -= 1;		// on est revenu à une profondeur + petite
  }
  (*nb_dec) -= 1;
  return output;
};

ostream& operator<< (ostream& output, const Env& envi)
{
  string decalage = "    ";
  Cell* cell;
  cout 	<< "// ************************* Environnement ************************* \\\\" << endl
        << "// ***************************************************************** \\\\" << endl;
  // Affichage des caractéristiques de l'environnement
  // todo si on veut
  cout << endl;
  cout 	<< "// ************************* Les organismes ************************** \\\\" << endl;
  cout 	<< "// ******************************************************************* \\\\" << endl;
  cout << endl;
  for (int i=0; i<(int)envi.les_cell.size(); i++)
    {
      cell = envi.les_cell[i];
      if (not(cell->mere())) {
	vector<int> *nb_decalages = new vector<int>;
	(*nb_decalages).push_back(0);
	int *nb_dec = new int;
	*nb_dec = 0;
	cout << endl;
	cout 	<< "//__________________________Un organisme____________________________\\\\" << endl;
	affiche_orga_rec(output, cell, nb_dec, nb_decalages, 0);
	cout 	<< "\\__________________________________________________________________////" << endl;
	delete nb_decalages;
	delete nb_dec;
      }
    }
  cout	<< "\\\\ ***************************************************************** //" << endl
	<< "\\\\ ***************************************************************** //" << endl;
  
  return output;
};


bool Env::swap_pos(Cell* cell, Position posi) {
  if (map_pos.find(posi) != map_pos.end()) {
    map_pos.erase(posi);
  }
  if (map_pos.find(cell->pos()) != map_pos.end()) {
    map_pos.erase(cell->pos());
  }
  cell->pos() = posi;
  map_pos.insert(pair<Position,Cell*>(posi,cell));
  return true;
};
