#include <iostream>
#include <fstream>
#include <cstdlib>
#include <dlfcn.h>
#include <ctime>
#include "env.h"
#include "Log.h"
using namespace std;

fstream filestr;
Log log_er;

int main()
{
        init_path();
	Env my_env;
	char nom_fichier4[] = "code_compile4.so";
	int nb_add = 500;
	int nb_tours = 20;
	int nb_tours2 = 2000;
	time_t start, end;
	double diff;

	// Test pour la compilation
	cout << endl << "##### Ajout d'une seule cellule:" << endl;
	cout << "code_comil4.cpp OK ?:";
	if (my_env.add_cell_auto(nom_fichier4, 0)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << " FIN DU TEST d'ajout d'une cellule compiée. Voici le terrain:" << endl;
	affiche_terrain(cout, my_env);
	
	// test de calculs
	cout << endl << "##### Test de quelques calculs (nb = " << nb_tours << " ):" << endl;
	for (int i =0; i < nb_tours; i++)
	  {
	    cout << "TOUR " << i << endl;
	    affiche_terrain(cout,my_env);
	    my_env.calc_tour();
	  }
	cout << "FIN" << endl;
       
	// Test de pleins de cellules
	cout << endl << "##### Ajout de pleins de cellules: nb d'ajouts = " << nb_add << endl;
	for (int i = 0; i< nb_add; i++)
	  if (my_env.add_cell_auto(nom_fichier4, i % (nb_add / 4))) cout << "OK"; else cout << "NON" << endl;
	cout << endl << " FIN DES TESTS d'ajout des cellules compiées. Nombre de cellule dans l'env = "<< my_env.les_cell.size() << endl;

	// test de calcul
	cout << endl << "##### Beaucoup de calculs (nb = " << nb_tours2 << " ):" << endl;
	cout << "On augmente la durée de vie des cellules au nombre de tours + 50" << endl;
	my_env.fix_A_mort(nb_tours2 + 50);
	time(&start);
	for (int i =0; i < nb_tours2; i++)
	  {
	    my_env.calc_tour();
	  }
	time(&end);
	diff = difftime(end,start);       
	cout << "FIN, nb de cellules = " << my_env.les_cell.size() << endl;
	cout << "le terrain: " << endl;
	affiche_terrain(cout, my_env);
	cout << "Temps de calcul (ms): " << (diff*(double)1000) << endl;
	cout << "Temps de calcul par tour (ms): " << (diff*(double)1000)/((double)nb_tours2)<< endl;
	
	return 0;
}
