#include <iostream>
#include <fstream>
#include <cstdlib>
#include <dlfcn.h>
#include <ctime>
#include "env.h"
#include "Log.h"
using namespace std;

fstream filestr;
Log log_er;

int main()
{
        init_path();
	char nom_fichier[] = "code_compile.so";
	char nom_fichier2[] = "code_compile2.so";
	char nom_fichier3[] = "code_compile3.so";
	char nom_fichier4[] = "code_compile4.so";
	char nom_fichier5[] = "code_compile5.so";
	char nom_fichier6[] = "code_compile6.so";

	Env my_env; //pour le test sans utiliser les environnements

	// Test pour la compilation
	cout << " tests de compil" << endl;
	cout << "test num rien (code_compile.cpp) ?:";
	if (my_env.add_cell_auto(nom_fichier, 0)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << "test num 2 (code_compile2.cpp) ?:";
	if (my_env.add_cell_auto(nom_fichier2, -2)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << "test num 3 ?:";
	if (my_env.add_cell_auto(nom_fichier3, -3)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << "test num 4 ?:";
	if (my_env.add_cell_auto(nom_fichier4, -4)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << "test num 5 ?:";
	if (my_env.add_cell_auto(nom_fichier5, -5)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << "test num 6 ?:";
	if (my_env.add_cell_auto(nom_fichier6, -6)) cout << "OK" << endl; else cout << "NON" << endl;
	cout << " FIN DES TESTS d'ajout de cellules compiées. Voici le terrain:" << endl;
	affiche_terrain(cout, my_env);

	// TEST DE SAVE/CHARGEMENT
	cout << "TEST DE SAVE/CHARGEMENT" << endl;
	cout << "Add de cellules pour le prop 0 puis 4 calculs." << endl;
	if (my_env.add_cell_auto(nom_fichier, 0)) cout << "OK" << endl; else cout << "NON" << endl;
	if (my_env.add_cell_auto(nom_fichier, 0)) cout << "OK" << endl; else cout << "NON" << endl;
	if (my_env.add_cell_auto(nom_fichier, 0)) cout << "OK" << endl; else cout << "NON" << endl;
	if (my_env.add_cell_auto(nom_fichier, 0)) cout << "OK" << endl; else cout << "NON" << endl;
	my_env.calc_tour();
	my_env.calc_tour();
	my_env.calc_tour();
	my_env.calc_tour();
	cout << "sauvegarde..." << endl;
	my_env.save_etat((char*)"save");
	cout << " terrain:" << endl;
	affiche_terrain(cout, my_env);
	cout << endl << "suppressions de toutes les cellules.." << endl;
	my_env.clear_all_cell();
	cout << " terrain:" << endl;
	affiche_terrain(cout, my_env);
	cout << endl << "chargement..." << endl;
	my_env.load_etat((char*)"save");
	cout << " terrain:" << endl;
	affiche_terrain(cout, my_env);
	cout << "Un calcul" << endl;
	my_env.calc_tour();
	cout << " terrain:" << endl;
	affiche_terrain(cout, my_env);
	cout << "REUSSI" << endl;

	if (my_env.add_cell_auto(nom_fichier6, -6)) cout << "OK" << endl; else cout << "NON" << endl;

	// TEST DE RM_FAMILLE
	cout << endl << "TEST DE RM_FAMILLE" << endl;
	cout << "rm tout ..." << endl;
	my_env.clear_all_cell();
	cout << "ok " << endl;
	filestr << "Clear_all OKKKK" << endl;
	for (int i = 0; i < 4; i++)
	  {
	    my_env.add_cell_auto(nom_fichier2, i % 2);
	  } 
	my_env.calc_tour();
	affiche_terrain(cout,my_env);
	cout << endl << "rm de 0,0" << endl;
	my_env.rm_famille(0,0);
	affiche_terrain(cout,my_env);
	cout << endl << "rm de 0,1" << endl;
	my_env.rm_famille(0,1);
	affiche_terrain(cout,my_env);
	cout << endl << "rm de 1,1" << endl;
	my_env.rm_famille(1,1);
	affiche_terrain(cout,my_env);

	
	// test de envoie message et nrj (si c'est ok alors qqu a bcp d'nrj !!)
	cout << endl << "TEST DE DUPLICATION" << endl;
	for (int i = 0; i < 2; i++)
	  {
	    my_env.add_cell_auto(nom_fichier3, i); 
	  }
	for (int i = 0; i< 20 ; i++)
	  {
	    cout << "TOUR NUMERO " << i << endl;
	    affiche_terrain(cout,my_env);
	    cout << endl;
	    my_env.calc_tour();
	  }

	my_env.clear_all_cell();
	cout << endl << "TEST DE l'ACTION ENVOIE NRJ ET ENVOIE DE MESSAGE" << endl;
	for (int i = 0; i < 20; i++)
	  {
	    my_env.add_cell_auto(nom_fichier, 2 + i % 5); 
	  }
	Position posi(20,20);
	for (int i = 0; i < 5; i ++)
	  {
	    posi += E;
	    my_env.swap_pos(my_env.les_cell[i], posi);
	  }
	posi += S;
	for (int i = 5; i < 10; i ++)
	  {
	    posi += O;
	    my_env.swap_pos(my_env.les_cell[i], posi);
	  }
	for (int i = 0; i< 10 ; i++)
	  {
	    cout << "TOUR NUMERO " << i << endl;
	    affiche_terrain(cout,my_env);
	    cout << endl;
	    my_env.calc_tour();
	  }


	// test de deplacement
	cout << endl << "TEST DE l'ACTION DEPLACEMENT + ATTAQUE + CONFLITS" << endl;
	my_env.clear_all_cell();
	for (int i = 0; i < 10; i++)
	  {
	    my_env.add_cell_auto(nom_fichier2, 6 + (i % 4)); 
	  }
	Position posi2(10,20);
	for (int i = 0; i < 5; i ++)
	  {
	    posi2 += E;
	    my_env.swap_pos(my_env.les_cell[i], posi2);
	  }
	posi2 += S;
	for (int i = 5; i < 10; i ++)
	  {
	    posi2 += O;
	    my_env.swap_pos(my_env.les_cell[i], posi2);
	  }
	for (int i = 0; i< 10 ; i++)
	  {
	    cout << "TOUR NUMERO " << i << endl;
	    affiche_terrain(cout,my_env);
	    cout << endl;
	    my_env.calc_tour();
	  }

	// Test de affiche terrain !
	cout << "TEST DE AFFICHE TERRAIN" << endl;
	affiche_terrain(cout, my_env) << endl;

	// TEST DE rapidité (pour le moment debbug du passage à l'échelle de deplacement):
	cout << endl << "CRASH TEST: Saturation du terrain en cellules + deplacements aléatoires" << endl;
	cout << " les cellules ont une durée A_mort = 100 ! " << endl;
	int nb = 388;
	int nb_pas = 50;
	int nb_cel_add = 500;
	cout << " Nombre de cellules ajoutés: "<< nb + 12 << " Nombres de pas de calculs: " << nb_pas << endl;
	cout << "on essaye à chaque tour d'ajouter " << nb_cel_add << " cellules en plus, dòu la longueur" << endl;
	for (int j=0; j<nb; j++)
	  {
	    my_env.add_cell_auto(nom_fichier2, j + 40);
	  }
	cout << endl;
	time_t start, end;
	double diff;
	time(&start);
	cout << "ok chargement" << endl;
	for (int j=0; j<nb_pas; j++)
	  {
	    cout << "TOUR " << j << endl << endl;
	    //cout << "deb calc" << endl;
	    //if (j == 11) affiche_terrain(cout, my_env);
	    my_env.calc_tour();
	    //cout << "calc ok   " << endl;
	    for (int j = 0; j < nb_cel_add; j++)
	      {
		//if (my_env.add_cell_auto(nom_fichier2)) cout << "o";
		my_env.add_cell_auto(nom_fichier2, nb + 1 + j);
	      }
	    //	    cout << " fin add" << endl;
	    cout << "nb de cells: " << my_env.les_cell.size() << endl;
	  }
	cout << "reussi" << endl;

	cout << "TEST DE SEUVGERADE MASSIVE" << endl;
	cout << "save..." << endl;
	my_env.save_etat((char*)"save2");
	cout << "nb de cells: " << my_env.les_cell.size() << endl;
	cout << "clear_all.." << endl;
	my_env.clear_all_cell();
	cout << "nb de cells: " << my_env.les_cell.size() << endl;
	cout << "LOAD..." << endl;
	my_env.load_etat((char*)"save2");
	cout << "nb de cells: " << my_env.les_cell.size() << endl;


	cout << endl << "TEST DE RAPIDITE:" << endl;
	cout << endl << "fix_A_mort(100000) : les cellules peuvent vivre plus longtemps (pour le benchmark) et clear_all_cell() : on repart à 0" << endl;
	my_env.fix_A_mort(1000000);
	my_env.clear_all_cell();
	nb_pas = 3000;
	nb = 5000;
	int comp = 0;
	for (int j=0; j<nb/2; j++)
	  {
	    if(my_env.add_cell_auto(nom_fichier3, 2 * nb_cel_add + j)) comp ++;
	    if(my_env.add_cell_auto(nom_fichier2, 3*nb_cel_add + j)) comp ++;
	  }
	cout << " Nombre de cellules ajoutés: "<< comp << " Nombres de pas de calculs: " << nb_pas << endl;
	cout << "nb de cells: " << my_env.les_cell.size() << endl;
	cout << "ok chargement, depart..." << endl;
	my_env.fix_A_mort(300);
	time(&start);
	for (int j=0; j<nb_pas; j++)
	  {
	    my_env.calc_tour();
	  }
	time(&end);
	cout << "reussi" << endl;
	diff = difftime(end,start);
	cout << "nb de cells: " << my_env.les_cell.size() << endl;
	cout << "Nombre de mili-secondes en tout: " << diff*(double)1000  << endl;
	cout << "Nombre de mili-secondes par tour: " << diff*(double)((double)1000/(double)nb_pas)  << endl;
	cout << "nb de cells: " << my_env.les_cell.size() << endl;

	my_env.fix_A_mort(100);
	for (int j=0; j<500; j++)
	  {
	    my_env.calc_tour();
	  }
	cout << "nb de cells: " << my_env.les_cell.size() << " et enfin un calcul ;) " << endl;
	
	my_env.calc_tour();

	/*
	  Ce qui suit est un test de l'affichage de forêt, a enlever quand on supprime des cellules
	  
	  Cell cell_1(nom_fichier, 50); // 50 à remplacer par E_init dans env
	  my_env.add_cell(&cell_1, Position(12, 35));
	  //Duplication de cell_1 sur la droite :
	  cell_1.nrj_load(nrj);
	  assign ass (11,complex<int> (0,10));
	  // on créer un bon assignement (rappel: si on ne parle pas d'un indice: il est à NULL !!)
	  for (int i=1; i<= 10; i++)
	  {
	  ass[i].real() = i;
	  }
	  int tabl[] = {0,1,2,3,4,5,6,7,8,9,10};
	  //	ass.push();
	  Cell cell_2(&cell_1, "depart", tabl, tabl,  ass, 1, 50);
	  my_env.add_cell(&cell_2, (cell_1.pos() + O));
	  //Déplacement de cell_2 avec comme sous-code : depart vers la diagonale haut/droite :
	  cell_2.pos() += N_O;
	  //Duplication de cell_2 avec comme sous-code: depart vers le bas :
	  cell_2.nrj_load(nrj);
	  Cell cell_3(&cell_2, "depart", tabl, tabl, ass, 1, 50);
	  my_env.add_cell(&cell_3, cell_2.pos() + S);
	  Cell cell_5(&cell_2, "depart", tabl, tabl, ass, 1, 50);
	  my_env.add_cell(&cell_5, cell_2.pos() + N);
	  // test de add_cell_auto
	  for (int j=0; j<2; j++)
	  {
	  my_env.add_cell_auto(nom_fichier);
	  }
	  Cell cell_4(nom_fichier, 30), cell_6(nom_fichier, 60);
	  my_env.add_cell(&cell_4, cell_2.pos() + N+N+N);
	  my_env.add_cell(&cell_6, cell_2.pos() + N+N+N+E);
	  // test de << pour env
	  cout << endl << endl;
	  cout << my_env;
	  // test de calcul:
	  cout << endl << endl;
	  cout << " ________ TEST DES CALCULS ________" << endl;
	  cout << cell_1 << endl;
	  for(int i = 0 ; i < 2 ; ++i)
	  {
	  cout << "$$$$$$$$$$$$$$ Tour numéro: " << i << " $$$$$$$$$$$$$$" << endl;
	  my_env.calc_tour();
	  cout << my_env ;
	  }
	*/
	
	return 0;
}
