#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "config_comm.h"

int main (int argc, char *argv[]){
    
    struct hostent *hostinfo = NULL;
    struct sockaddr_in addr;
    int length, format; // adresse du serveur
//    int port, port_first_log;
//    const char *hostname = "localhost"; // adresse du serveur
//    char *first_log = "Premiere connexion";
    char buffer[TAILLE_BUFF];
    FILE *stream;
	int sock = socket(PF_INET, SOCK_STREAM, 0);

	if(argc == 1)
	{
		fprintf(stderr, "Usage : %s [nom_du_fichier]\n", argv[0]);
		exit(EXIT_FAILURE);
	}
    
    memset(& addr, 0, sizeof(struct sockaddr_in));
    
//    hostinfo = gethostbyname(hostname); // A remplacer probablement par la ligne du dessous 
//      hostinfo = gethostbyaddr(ADRESSE_SERVEUR, strlen(ADRESSE_SERVEUR), FORMAT);
	addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
  
/*    if (hostinfo == NULL){
	fprintf (stderr, "Unknown host %s.\n", ADRESSE_SERVEUR);
	exit(EXIT_FAILURE);
    }
  */  
  //  addr.sin_addr = *(struct in_addr *) hostinfo->h_addr; 

    // Un fichier à envoyer : on choisit le port de reception du serveur
	addr.sin_port = htons(PORT_SERVEUR);

    addr.sin_family = AF_INET;
    
    if(connect(sock,(struct sockaddr *) &addr, sizeof(struct sockaddr))){
	fprintf (stderr, "Connexion failed.\n");
	exit(EXIT_FAILURE);
    }
    
    // Argument 1 : chemin du fichier à envoyer
	stream = fopen(argv[1], "r");
	while(fgets(buffer, TAILLE_BUFF, stream)){
	    if(send(sock, buffer, strlen(buffer), 0) < 0){
		fprintf (stderr, "Sending failed.\n");
		exit(EXIT_FAILURE);
	    }
	}
	fclose(stream);

    close(sock);
    return 0;
}
