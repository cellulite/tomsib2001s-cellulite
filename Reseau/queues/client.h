#ifndef DEF_CLIENT
#define DEF_CLIENT

struct _client
{
	int id;
	int socket;
};
typedef struct _client* client;

client new_client(int socket);

void print_client(client);

#endif //guard
