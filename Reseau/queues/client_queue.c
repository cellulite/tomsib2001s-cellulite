#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "client_queue.h"

struct _client_queue{
	client value;
	struct _client_queue *succ;
	struct _client_queue *prev;
};
typedef struct _client_queue *client_queue;

static pthread_mutex_t Client_mutex, Nouveau_mutex;
static pthread_mutex_t Client_null_mutex;
static pthread_cond_t Client_empty;
static client_queue Client_queue, Nouveau_queue;

void init_client_queues()
{
	pthread_mutex_init(&Client_mutex, NULL);
	pthread_mutex_init(&Nouveau_mutex, NULL);
	pthread_mutex_init(&Client_null_mutex, NULL);
	pthread_cond_init(&Client_empty, NULL);
//	Client_empty = PTHREAD_COND_INITIALIZER;
	Client_queue = NULL;
	Nouveau_queue = NULL;
}

static client pop(client_queue *q)
{
	client res = NULL;
	client_queue succ;
	if(!(*q))
		return NULL;
	res = (*q)->value;
	succ = (*q)->succ;
	if(succ == (*q)) //file à un seul élément
	{
		free(*q);
		*q = NULL;
	}
	else
	{
		succ->prev = (*q)->prev;
		(*q)->prev->succ = succ;
		free(*q);
		*q = succ;
	}
	return res;
}

client pop_client()
{
	client res = NULL;
	pthread_cond_wait(&Client_empty, &Client_null_mutex);
	if(pthread_mutex_trylock(&Client_mutex))
	{
		return NULL;
	}
	res = pop(&Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	return res;
}

client pop_nouveau()
{
	client res = NULL;
	if(pthread_mutex_trylock(&Nouveau_mutex))
	{
		return NULL;
	}
	res = pop(&Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);
	return res;
}

static void push(client c, client_queue *q)
{
	client_queue res = malloc(sizeof(*res));
	res->value = c;
	if(!(*q)) //file vide
	{
		res->succ = res->prev = res;
		*q = res;
	}
	else
	{
		res->succ = *q;
		res->prev = (*q)->prev;
		(*q)->prev->succ = res;
		(*q)->prev = res;
	}
}

void push_client(client c)
{
	pthread_mutex_lock(&Client_mutex);
	push(c, &Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	pthread_cond_broadcast (&Client_empty);
}

void push_nouveau(client c)
{
	pthread_mutex_lock(&Nouveau_mutex);
	push(c, &Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);
	pthread_cond_broadcast (&Client_empty);
}

static void print_queue(client_queue q)
{
	client_queue debut = q;
	if(!q)
	{
		fprintf(stderr, "File vide\n");
		return;
	}
	do
	{
		print_client(q->value);
		q = q->succ;
	}while(q != debut);
}

void print()
{
	pthread_mutex_lock(&Client_mutex);
	fprintf(stderr, "Client queue :\n");
	print_queue(Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	pthread_mutex_lock(&Nouveau_mutex);
	fprintf(stderr, "Nouveau queue :\n");
	print_queue(Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);
}

