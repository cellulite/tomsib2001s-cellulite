//:client_queue.c

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include "client_queue.h"

//les files sont implémentées par des listes circulaires dont le "premier" 
//élément (auquel on a accès immédiatement) est la tête de la file
struct _client_queue{
	client value;
	struct _client_queue *succ;
	struct _client_queue *prev;
};
typedef struct _client_queue *client_queue;

//déclarations des variables du fichier:
//les files
//les mutex pour reglementer l'accès aux files
//la condidition pour réduire la consommation du CPU et rendre le 
//pop_client partiellement blocant
static pthread_mutex_t Client_mutex, Nouveau_mutex;
static pthread_mutex_t Client_null_mutex;
static pthread_cond_t Client_empty;
static client_queue Client_queue, Nouveau_queue;

void init_client_queues()
{
	pthread_mutex_init(&Client_mutex, NULL);
	pthread_mutex_init(&Nouveau_mutex, NULL);
	pthread_mutex_init(&Client_null_mutex, NULL);
	pthread_cond_init(&Client_empty, NULL);
	Client_queue = NULL;
	Nouveau_queue = NULL;
}

//fonction classique de pop
static client pop(client_queue *q)
{
	client res = NULL;
	client_queue succ;
	if(!(*q))
		return NULL;
	res = (*q)->value;
	succ = (*q)->succ;
	if(succ == (*q)) //file à un seul élément
	{
		free(*q);
		*q = NULL;
	}
	else
	{
		succ->prev = (*q)->prev;
		(*q)->prev->succ = succ;
		free(*q);
		*q = succ;
	}
	return res;
}

client pop_client()
{
	client res = NULL;
	pthread_cond_wait(&Client_empty, &Client_null_mutex); //à moitié blocant
	if(pthread_mutex_trylock(&Client_mutex)) //à moitié non blocant
	{
		return NULL;
	}
	res = pop(&Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	return res;
}

client pop_nouveau()
{
	client res = NULL;
	if(pthread_mutex_trylock(&Nouveau_mutex)) //pop non blocant
	{
		return NULL;
	}
	res = pop(&Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);
	return res;
}

//fonction classique de push
static void push(client c, client_queue *q)
{
	client_queue res = malloc(sizeof(*res));
	res->value = c;
	if(!(*q)) //file vide
	{
		res->succ = res->prev = res;
		*q = res;
	}
	else
	{
		res->succ = *q;
		res->prev = (*q)->prev;
		(*q)->prev->succ = res;
		(*q)->prev = res;
	}
}

void push_client(client c)
{
	pthread_mutex_lock(&Client_mutex); //push blocant
	push(c, &Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	pthread_cond_broadcast (&Client_empty); //signalisation du push
}

void push_nouveau(client c)
{
	pthread_mutex_lock(&Nouveau_mutex); //push blocant
	push(c, &Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);
	pthread_cond_broadcast (&Client_empty); //signalisation du push
}

static void libere_client(client c)
{
	if(c)
	{
		shutdown(c->socket, 2); //ferme la socket
		free(c); //libère la mémoire
	}
}

static void libere_file(client_queue *q)
{
	client_queue tmp;
	if(q)
	{
		(*q)->prev->succ = NULL;
		while(*q)
		{
			libere_client((*q)->value);
			tmp = (*q)->succ;
			free(*q);
			*q = tmp;
		}
	}
}

void term_client_queues()
{
	pthread_mutex_lock(&Client_mutex);
	libere_file(&Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	pthread_mutex_lock(&Nouveau_mutex);
	libere_file(&Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);

	pthread_mutex_destroy(&Client_mutex);
	pthread_mutex_destroy(&Nouveau_mutex);
	pthread_mutex_destroy(&Client_null_mutex);
	pthread_cond_destroy(&Client_empty);
}

//print classique
static void print_queue(client_queue q)
{
	client_queue debut = q;
	if(!q)
	{
		fprintf(stderr, "File vide\n");
		return;
	}
	do
	{
		print_client(q->value);
		q = q->succ;
	}while(q != debut);
}

//print "multithreadé"
void print()
{
	pthread_mutex_lock(&Client_mutex);
	fprintf(stderr, "Client queue :\n");
	print_queue(Client_queue);
	pthread_mutex_unlock(&Client_mutex);
	pthread_mutex_lock(&Nouveau_mutex);
	fprintf(stderr, "Nouveau queue :\n");
	print_queue(Nouveau_queue);
	pthread_mutex_unlock(&Nouveau_mutex);
}

