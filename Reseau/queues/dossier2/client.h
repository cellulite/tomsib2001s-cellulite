//:client.h
/***********************************************************************\
 * Définition des informations utiles pour le traitement d'un client :
 * - la socket qui permet de communiquer avec lui
 * et des fonctions utiles pour manipuler cette structure
\***********************************************************************/

#ifndef DEF_CLIENT
#define DEF_CLIENT

struct _client
{
	int id; //pas réellement utile mais pratique pour les tests
	int socket; //correspond au canal de communication avec un client
};
typedef struct _client* client; //on ne manipule que des pointeurs, c'est la loi ^^

client new_client(int socket); //création d'un client en précisant la socket

void print_client(client); //encore une fois, plus pratique qu'utile : affiche simplement le couple (id, socket)

#endif //guard
