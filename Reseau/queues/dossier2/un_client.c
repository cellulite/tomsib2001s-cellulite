#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include "interface_client.h"

int main(int argc, char *argv[])
{
	int i;
	char fichier[30];
	sigset_t set;
	sigaddset(&set, SIGPIPE);
	sigprocmask(SIG_BLOCK, &set, NULL);

	if(connecte_serveur())
	{
		fprintf(stderr, "Veuillez nous excuser pour l'indisponibilité du serveur. Vous pouvez vous reporter à notre site internet pour plus d'informations.\n");
		return 1;
	}
#ifdef VERBOSE_MODE
	fprintf(stdout,"CLIENT> connection\n");
#endif
	for(i = 0 ; i < 50 ; ++i)
	{
		sprintf(fichier, "repertoire/fichier_%d", i);
		if(recoit_carte(fichier) == -1)
		{
			return -1;
		}
	}
	deconnecte_serveur();
#ifdef VERBOSE_MODE
	fprintf(stdout,"CLIENT> déconnection\n");
#endif
	return 0;
}
