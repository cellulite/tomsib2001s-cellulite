#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#include "config.h"
#include "client.h"
#include "client_queue.h"

pthread_t id[NB_THREADS];

int transfertFichier(char buffer[], char* fichier, FILE* destination){
	FILE* source;
	source = fopen(fichier, "r");
	if(!source)
		return 0;
	if(!destination){
		return 0;
	}
//	fprintf(destination, "Debut\n");
	while(fgets(buffer, TAILLE_BUFF, source)){
		fprintf(destination, "%s", buffer);
		fflush(destination);
	}
//	fprintf(destination, "Fin\n");
	fclose(source);
	return 1;
}

void *Threads_waiting(void *arg)
{
	int my_id = (int)arg;
	int erreur = 0;
	client mon_client;
	fprintf(stdout, "thread %d ready\n", my_id);
    
	while(1)
	{

		if(mon_client = pop_nouveau())
		{
//			TRAITEMENT
			fprintf(stdout, "P:%d --> %d découvert\n", my_id, mon_client->id);
			push_client(mon_client);
		}
		if(mon_client = pop_client())
		{
//			TRAITEMENT
			fprintf(stdout, "P:%d --> %d traité\n", my_id, mon_client->id);
			if(!erreur)
				push_client(mon_client);
		}
//		sleep(5);
	}
	pthread_exit(EXIT_SUCCESS);
}



int main()
{  
/*
	struct timespec timeout;
	timeout.tv_sec = 2;
	timeout.tv_nsec = 0;
*/
//Initialisation du serveur
	int my_id = NB_THREADS;
	struct sockaddr_in addr;
	int listen_fd;
	int one = 1;
	int client_id;
	size_t length;
	struct sockaddr_in* client_addr = NULL;
	int i;
	pthread_attr_t attr;

	listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, & one, sizeof(int));
	memset(& addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1234);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	fprintf(stdout, "Adresse : %u\n", addr.sin_addr.s_addr);
	bind(listen_fd, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	listen(listen_fd, 15);

	init_client_queues();

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(i = 0; i<NB_THREADS; i++)
	{
		pthread_create(&id[i], NULL, Threads_waiting, (void*)i);
	}
	i = 0;
	while(1)
//	while(i < 40)
	{
		client_addr = malloc(sizeof(struct sockaddr_in));
		client_id = accept(listen_fd, (struct sockaddr*) client_addr, &length);
		push_nouveau(new_client(client_id, client_addr, length));
//		push_nouveau(new_client(i++, NULL, 0));
//		print();
//		sleep(1);
	}
/*	fprintf(stdout, "Tout les clients sont arrivés.\n");
	while(1)
		sleep(20);
*/	return 0;
}
