#include <stdio.h>
#include <stdlib.h>
#include "client.h"

client new_client(int socket)
{
	static int id = 0;
	client res = malloc(sizeof(*res));
	res->id = id++;
	res->socket = socket;
	return res;
}

void print_client(client c)
{
	if(c)
	{
		fprintf(stderr, "(%d, %d)\n", c->id, c->socket);
		return;
	}
	fprintf(stderr, "Client NULL !\n");
}
