(*Début Antoine*)

(*
val pos_mem : string -> (e_type * int) (* renvoie la position absolue d'une variable dans la mémoire (dépend du contexte) *)
val add_mem : string -> (e_type * int) -> unit (* ajoute une variable dans la mémoire à la position donnée *)
val rem_mem : string -> unit (* déclare l'inexistence d'une ancienne variable *)

val size_mem : ref int (* pour se balader dans l'arbre, début de la mémoire disponible *)
val make_var : adn -> Ast2.adn (* dernière étape avant le c++*)
(*Fin Antoine*)
*)
open Ast
open Anastat
open Ast2
open Error

(* Fonctions d'affichage des ast2 *)

let affiche = print_string

let p_op (o : Ast.op) =
  match o with
    | And -> affiche "and"
    | Or -> affiche "or"
    | Plus -> affiche "+"
    | Times -> affiche "*"
    | Minus -> affiche "-"
    | Div -> affiche "/"
    | Greater -> affiche ">"
    | Geq -> affiche ">="
    | Low -> affiche "<"
    | Leq -> affiche "<="
    | Eq -> affiche "="
    | Neq -> affiche "!="
    | Mod -> affiche "%"
    | Band -> affiche "&"
    | Bor -> affiche "|"

      
let p_addr = function
	| Rel(i) -> affiche "MEM[+";
		print_int i;
		affiche "]"
	| Abs(i) -> affiche "MEM[";
		print_int i;
		affiche "]"
	| Arg(i) -> affiche "ARG[";
		print_int i;
		affiche "]"
	| PVar(i) -> affiche "VAR[";
		print_int i;
		affiche "]"
let p_list p_type = function
	| [] -> affiche "[]"
	| expr :: queue -> begin
		let rec aux = function
			| [] -> affiche "]"
			| expr :: queue -> affiche ", ";
				p_type expr;
				aux queue in
		affiche "[";
		p_type expr;
		aux queue
		end
;;

let rec p_expr = function
  | Not(e) -> affiche "Not(";  p_expr e; affiche ")"
  | Var(var_name) -> affiche var_name
  | Refvar(addr) -> p_addr addr
  | Int(n) -> print_int n
  | Const(var_name) -> affiche var_name
  | Op_expr(op, expr1, expr2) -> affiche "(";
    p_expr expr1;
    affiche " ";
    p_op op;
    affiche " ";
    p_expr expr2;
    affiche ")"
  | Function(name, expr_list) -> affiche name;
    affiche "(";
    p_list p_expr expr_list;
    affiche ")"
  | DIR_MESS(mess, cond) -> affiche "DIR_MESS(";
    affiche mess;
    affiche ", ";
    p_expr cond;
    affiche ")"
  | SEE -> affiche "SEE()";
  | Sub_expr(mess, expr1, expr2) -> affiche "(";
    p_expr mess;
    affiche ")";
    affiche "[";
    p_expr expr1;
    affiche "..";
    p_expr expr2;
    affiche "]"
  | Concat(l) -> begin
    let rec aux = function
      | [] -> ()
      | [(e, t)] -> affiche "(";
	p_expr e;
	affiche ", ";
	print_int t;
	affiche ")"
      | (e, t) :: q -> affiche "(";
	p_expr e;
	affiche ", ";
	print_int t;
	affiche ").";
	aux q in
    aux l
  end
;;

let p_assign = function
  | Assign(expr1, expr2) -> affiche "[|";
    p_expr expr1;
    affiche ", ";
    p_expr expr2;
    affiche "|]"
;;

let p_action = function
  | Move(expr) -> affiche "MOVE(";
    p_expr expr;
    affiche ")"
  | Send_M(mess, expr) -> affiche "SEND(";
    p_expr mess;
    affiche ", ";
    p_expr expr;
    affiche ")"
	| Send_E(expr1, expr2) -> affiche "SEND(";
	  p_expr expr1;
	  affiche ", ";
	  p_expr expr2;
	  affiche")"
	| Duplicate(e_nrj, e_dir, nom_prog, vars, args, taille) -> affiche "DUPLICATE(";
	  p_expr e_nrj;
	  affiche ", ";
	  p_expr e_dir;
	  affiche (", " ^ nom_prog ^ ", ");
	  p_list p_expr vars;
	  affiche ", ";
	  p_list p_expr args;
	  affiche ", ";
	  print_int(taille);
	  affiche ")"
	| Call(nom, vars, args, init, debut) -> affiche ("CALL(" ^ nom ^ ", ");
	  p_list p_addr vars;
	  affiche ", ";
	  p_list p_expr args;
	  affiche ", ";
	  p_list p_assign init;
	  affiche ", ";
	  print_int debut;
	  affiche ")"
	| Skip -> affiche "SKIP"
;;

let rec p_prog = function
  | IFTE(cond, prog1, prog2) -> affiche "IF(";
    p_expr cond;
    affiche ")\nTHEN{\n";
    p_prog prog1;
    affiche "}\nELSE{\n";
    p_prog prog2;
    affiche "}\n"
  | IFTE_M(mess, dir, cond, prog1, prog2) -> affiche ("IF EXISTS("^mess^", "^dir^", ");
    p_expr cond;
    affiche ")\nTHEN{\n";
    p_prog prog1;
    affiche "}\nELSE{\n";
    p_prog prog2;
    affiche "}\n"
  | LETIN(nom_var, expr, prog) -> affiche ("LET " ^ nom_var ^ " = ");
    p_expr expr;
    affiche " IN\n{\n";
    p_prog prog;
    affiche "}\n"
  | Action_c(Assign(expr1, expr2), prog) -> p_expr expr1;
    affiche "=";
    p_expr expr2;
    affiche " ;\n";
    p_prog prog
  | Action(action) -> p_action action;
    affiche "\n"
  | Progseq(string, prog) -> affiche (string^":\n");
    p_prog prog
;;

let p_dec_prog = function
  | DecProg(nom, liste, prog) -> affiche (nom ^ " :\n");
    List.iter (fun (x, y) -> affiche "("; p_addr x; affiche (", "^y^")\n")) liste;
    p_prog prog
;;

let p_adn = function
  | ADN(dec_list, main, var_list,arg_list,code_size) -> p_list p_dec_prog dec_list;
    affiche ("\nMain : " ^ main ^ "\n");
    p_list p_expr var_list;
    p_list p_expr arg_list;
    affiche "\nTaille : ";
    print_int code_size;
    affiche "\n\n"
;;

(* Fin des fonctions d'affichage *)

let taille_hash = 251
and coeff_hash = 13
and p_mem = ref 0
and num_label = ref 0
and list_label = ref []

module Var = 
struct
  type t = string
  let equal s1 s2 = ((String.compare s1 s2) = 0)
  let hash s = 
    let res = ref 0 in
    for i = 0 to (String.length s - 1) do
      res := (!res + coeff_hash * (int_of_char s.[i])) mod taille_hash;
    done;
    !res
end
  
module AdrHtbl = Hashtbl.Make(Var)
  
let tab_var = AdrHtbl.create taille_hash;;
let find_var = AdrHtbl.find tab_var
and add_var = AdrHtbl.add tab_var
and remove_var = AdrHtbl.remove tab_var
;;

(*val trad_expr : Ast.expr -> Ast2.expr*)
let rec trad_expr = function
  | Ast.Not(e) -> Op_expr(Eq,trad_expr e,Int(0))
  | Ast.Var(var_name) -> begin
    try
      let adr = find_var var_name in
      Refvar(adr)
    with
      | Not_found -> Var(var_name)
  end
  | Ast.Refvar(var_name) -> begin
    try
      let adr = find_var var_name in
      Refvar(adr)
    with
      | Not_found -> affiche ("Erreur dans trad_expr : " ^ var_name ^ " non trouvée\n");
	Var(var_name)
  end
  | Ast.Int(n) -> Int(n)
  | Ast.Const(nom_var) -> Const(nom_var)
  | Ast.Op_expr(op, expr1, expr2) -> 
    Op_expr(op, trad_expr expr1, trad_expr expr2)
  | Ast.Function("costDuplicate", expr_list) -> 
    (match expr_list with
      | [a;Ast.Var fonc] ->(* print_string ("Voici la taille des dépendances de "^(fonc)^" : "^(string_of_int(code_size(dependencies(fonc))))^". \n") ;*)  Function("costDuplicate",[trad_expr a;Int(code_size(dependencies(fonc)))])
      | _ -> raise (Anastat.BadArgument("costDuplicate")))
  | Ast.Function(name, expr_list) ->
    Function(name, List.map trad_expr expr_list)
  | Ast.DIR_MESS(mess, cond) -> DIR_MESS(mess, trad_expr cond)
  | Ast.SEE -> SEE(*begin
		    try
		    let adr = find_var var_name in
		    SEE(Refvar(adr))
		    with
		    | Not_found -> SEE(Var(var_name))
		    end*)
  | Ast.Sub_expr(mess, expr1, expr2) -> 
    Sub_expr(trad_expr mess, trad_expr expr1, trad_expr expr2)
  | Ast.Concat(l) -> 
    Concat(List.map (fun (e, t) -> (trad_expr e, t)) l)
      
let rec trad_action = function
  | Ast.Move(e) -> Move(trad_expr e)
  | Ast.Send_M(mess, e) -> Send_M(trad_expr mess, trad_expr e)
  | Ast.Send_E(nrj, dir) -> Send_E(trad_expr nrj, trad_expr dir)
  | Ast.Duplicate(nrj, dir, n_fun, vars, args) -> 
    Duplicate(trad_expr nrj, trad_expr dir, n_fun, List.map trad_expr vars, List.map trad_expr args, Anastat.code_size(Anastat.dependencies(n_fun)))
  | Ast.Call(n_fun, e_a_list1, e_list2) -> (**)begin
    let rec aux el1 asl debut = function
      | [] -> p_mem := !p_mem + (Anastat.mem_size n_fun);
	Call(n_fun, List.rev el1, List.map trad_expr e_list2, List.rev asl, debut)
      | Ast.Addr(Ast.Var(nom)) :: q -> begin
	try
	  let addr = find_var nom in
	  p_mem := !p_mem - 1;
	  aux (addr :: el1) asl debut q
	with
	  | Not_found -> affiche ("Erreur dans trad_action : " ^ nom ^ " non trouvée\n");
	    aux el1 asl debut q
      end
      | Ast.Exp(expr) :: q -> let addr = Rel(debut) in
			      aux (addr :: el1) (Assign(Refvar(addr), trad_expr expr) :: asl) (debut + 1) q 
      | _ -> print_string "Erreur dans trad_action (Call) : ce cas ne devrait jamais arriver\n" ; raise Error.Compilation_Aborted  in
    aux [] [] (!p_mem) e_a_list1
  end
	(**) 
	(*		Skip*)
  | Ast.Skip -> Skip
;;

let rec trad_prog = function
  | Ast.IFTE(cond, prog1, prog2) -> 
    IFTE(trad_expr cond, trad_prog prog1, trad_prog prog2)
  | Ast.IFTE_M(mess, dir, cond, prog1, prog2) -> 
    IFTE_M(mess, dir, trad_expr cond, trad_prog prog1, trad_prog prog2)
  | Ast.LETIN(nom_var, expr, prog) -> LETIN(nom_var, trad_expr expr, trad_prog prog)
  | Ast.Action_c(Ast.Assign(nom_var, expr2), prog) -> 
    Action_c(Assign(trad_expr (Ast.Var(nom_var)), trad_expr expr2), trad_prog prog)
  | Ast.Action(action) -> Action(trad_action action)
  | Ast.Seq(prog_list) -> begin
    let label = "label"^(string_of_int !num_label) in
    incr num_label;
    let address = Rel(!p_mem) in
    incr p_mem;
    list_label := (address, label) :: (!list_label);
    let rec aux n = function
      | [] -> raise Error.Compilation_Aborted
      | t :: [] -> let n_prog = trad_prog t in
		   IFTE(Op_expr(Eq, Refvar(address), Int(n)), Action_c(Assign(Refvar(address), Int(0)), n_prog), Action(Skip))
      | t :: q ->let n_prog = trad_prog t in
		 let end_prog = aux (n + 1) q in
		 IFTE(Op_expr(Eq, Refvar(address), Int(n)), Action_c(Assign(Refvar(address), Int(n+1)), n_prog), end_prog) in
    let prog = aux 0 prog_list in
    Progseq(label, prog)
  end
;;

let rec trad_dec_prog = function
  | Ast.DecProg(nom, vars, args, prog) ->
    p_mem := 0;
    num_label := 0;
    list_label := [];
		(* add all to hash table *)
    let rec aux_var num = function
      | [] -> ()
      | Ast.Var(nom) :: q -> add_var nom (PVar(num)); aux_var (num + 1) q
      | _ -> affiche "Error in trad_dec_prog : aux_var\n" in
    aux_var 0 vars;
    let rec aux_arg num = function
      | [] -> ()
      | Ast.Var(nom) :: q -> add_var nom (Arg(num)); aux_arg (num + 1) q
      | _ -> affiche "Error in trad_dec_prog : aux_arg\n" in
    aux_arg 0 args;
    
    let n_prog = trad_prog prog in
    
		(* remove all from hash tables *)
    let aux = function
      | Ast.Var(nom) -> remove_var nom
      | _ -> affiche "Error in trad_dec_prog : rem_var\n" in
    List.iter aux vars;
    List.iter aux args;
    
    DecProg(nom, !list_label, n_prog)
;;

let trad_adn = function
  | Ast.ADN(dec_prog_list, name, expr_list1, expr_list2) ->
    ADN(List.map trad_dec_prog dec_prog_list, name, List.map trad_expr expr_list1, List.map trad_expr expr_list2, Anastat.code_size (Anastat.dependencies name))
      
(*
  val trad_dec_prog : Ast.dec_prog -> Ast2.dec_prog
  val trad_adn : Ast.adn -> Ast2.adn
*)
(*
  let test_expr1 = 
  Ast.Op_expr(
  Plus, 
  Ast.Op_expr(
  Times, 
  Ast.Var("x"), 
  Ast.Refvar("y") 
  ), 
  Ast.Op_expr(
  Minus, 
  Ast.Var("alpha"), 
  Ast.Const("age")
  )
  )
  
  and test_expr2 = Ast.SEE("u")
  and test_expr3 = 
  Ast.Function(
  "Cost_", 
  Ast.Var("dir") ::
  Ast.Var("nrj") :: 
  Ast.Op_expr(
  Div,
  Ast.Int(4),
  Ast.Refvar("z")
  ) ::
  []
  )
  ;;
  
  let test_e2 = Var("salut") :: SEE(Refvar(Rel(5))) :: []
  ;;
  
(*let test_cond = Ast.Var_cond("u")
;;*)
  
  let test_action1 = Ast.Move(test_expr2)
  and test_action2 = 
  Ast.Duplicate(
  test_expr3,
  Ast.Dir_int(5),
  "main",
  Ast.Var("u") :: Ast.Refvar("z") :: Ast.Int(5) :: [],
  []
  )
  and test_action3 = Ast.Send_M(Ast.Mess("Salut"), Ast.Dir_int(7))
  ;;
  
  let test_prog1 = 
  Ast.Action_c(
  Ast.Assign(
  "x", 
  Ast.Op_expr(
  Plus,
  Ast.Int(5),
  Ast.Var("y")
  )
  ),
  Ast.Action(test_action3)
  )
  ;;
  let test_prog2 =
  Ast.LETIN(
  "var1", 
  test_expr1,
  Ast.Action_c(
  Ast.Assign(
  "var1",
  test_expr2
  ),
  test_prog1
  )
  )
  ;;
  let test_prog3 = 
  Ast.IFTE(
  test_cond,
  test_prog1,
  test_prog2
  )
  ;;
  
  let test_dec_prog1 = Ast.DecProg("fun1", [Ast.Var("x") ; Ast.Var("y") ; Ast.Var("z")], [Ast.Var("t") ; Ast.Var("u")], test_prog3)
  ;;
  
(*
  add_var "x" (Rel(5));;
  add_var "y" (Abs(2));;
  add_var "z" (Rel(2));;
  add_var "t" (Abs(3));;
  add_var "u" (Abs(4));;
*)
  
(*
  p_prog (trad_prog test_prog3)
  ;;
  affiche "\n"
  ;;
  
  p_dec_prog (trad_dec_prog test_dec_prog1)
  ;;
  affiche "\n"
  ;;*)
*)
