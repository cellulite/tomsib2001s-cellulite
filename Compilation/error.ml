(* Ce fichier sert à écrire toutes les erreurs de rattrapages d'exception qui se produisent lors de la compilation, pour informer le programmeur de ce qui ne va pas *)

(* Puisque la compilation se fait sur un serveur distant, il faudra écrire tous ces messages d'erreurs - et tous les messages en général - dans un fichier qui sera alors transmis par l'interface de connexion réseau. On va donc utiliser une fonction affiche *)

open Parsing
open Ast
open Ast2
open Anastat
open Lexer

let p = print_string (* provisoire donc *)
let pl x = print_string x; print_newline()
let pn = print_newline()
let soi = string_of_int


exception BadType of string * Anastat.e_type * Anastat.e_type (* ici donné comme exemple, string est la variable, puis le type attendu, puis le type obtenu *)
exception Parse_error of int*int;;
exception Compilation_Aborted

let testErreurs f =
try f() with
  | BadType(s,t1,t2) -> p (("Typing Error, ")^s^(" is of type ")^("[print_type function to be added here] ")^("while it should have the type  ")^("[print_type function to be added here]"))
  | Parse_error(ligne,caractere) ->  pl ("Syntax Error : at line "^(soi(ligne))^", caracter "^(soi(caractere)))
  | Anastat.RecursiveCall(f) -> pl ("Error : function"^(f)^" is involved in a recursion")
  | Anastat.BadCall(f) -> pl ("Error : function "^f^"does not exist")
  | Anastat.BadConstante(s) -> pl ("Error : constant "^s^"does not exist")
  | Anastat.BadFunctionCost(s) -> pl ("Error : built-in operator "^s^" could not be found")
  | Anastat.BadDuplication(s) ->  pl ("Error : attempt to duplicate with main function "^s^" which does not exist")
  | Anastat.BadArite(s,i,j,k,l) -> pl ("Error : the arities of function "^(s)^" are "^(soi(i))^" and "^(soi(j))^", not "^(soi(k))^" and  "^(soi(l)))
  | Anastat.BadAriteFonc(s,i,j) -> pl ("Error : the arity of function"^s^" is "^(soi(i))^" and not "^(soi(j)))
  | Anastat.BadUseVar(s) -> pl ("Error : variable \""^s^"\" is used out of its scope or does not exist")
  | Anastat.BadArgument(s) -> pl ("Error : function or operator"^s^" was given the wrong type of argument")
  | Anastat.BadLetIn(s) -> pl ("Error : variable"^s^" cannot be used here (\"let in\" structure)")
  | Compilation_Aborted -> pl "Error : Sorry, this is a bug on the part of the compiler. Please file a report and be sure to attach your source code. The graindecell is sorry for the inconvenience and will be working very soon to repair this"
  | Ast.Generic_Error(s) -> pl (s^"\n"^"Error : Sorry, this is a bug on the part of the compiler. Please file a report and be sure to attach your source code. The graindecell is sorry for the inconvenience and will be working very soon to repair this")
  | e -> p "encore du travail à faire..." ; raise e
