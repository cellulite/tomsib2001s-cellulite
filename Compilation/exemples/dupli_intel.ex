dupli : vars (dir,etat,compt_pas) args (niv_nrj,nb_pas)
  if (etat = 0)
  then
    if (energy > 2*niv_nrj)
    then {
      dir := NextDir(dir);
      etat := 1;
      Duplicate(niv_nrj, dir, dupli(PrevDir(dir),1,nb_pas)(niv_nrj,nb_pas))
    }
    else
      Skip
  else
    if (etat = 1)
    then
      if (compt_pas = 0)
      then {
	etat := 0;
	Skip
      }
      else {
	compt_pas := compt_pas - 1;	
	Move(dir)
      }
    else
    Skip
;;

@dupli(0,0,0)(100,3)
