principale : vars (dejaDup, dir) args(cmp)
  if (dejaDup = 0) then
    {
      if (energy > 200) then
	{
	  dejaDup := 1;
	  if(cmp = 3) then
	    let x = nextDir(dir) in
	    Duplicate(energy / 2, x, principale(0, x)(0))
	  else
	    Duplicate(energy / 2, dir, principale(0, dir)(cmp + 1))
	}
      else
	Skip
    }
  else
    Skip
;;
@principale(0, N)(0).
  
