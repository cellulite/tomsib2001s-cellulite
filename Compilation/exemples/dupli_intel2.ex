dupli : vars (dir,etat,compt_pas,compt_dupli) args (niv_nrj,nb_pas,nb_dupli,nb_skip)
  (*
    VARS: tous à 0
    ARGS: 
     niv_nrj : quantité d'nrj cédée (quand on en a deux fois plus
     nb_pas : nombre de pas entre deux duplications (augmente de 1 à chaque génération
     nb_dupli : nombre de duplications avant de se reoposer nb_skip tours
     nb_skip : cf n_dupli
  *)
  (*
    etat 0 => duplication
    etat 1 => se déplacer
    etat 2 => se reposer
  *)
  if (etat = 0)
  then
    if (compt_dupli = nb_dupli)
    then {
      compt_dupli := 0;
      etat := 3;
      compt_pas := 0;
      Skip
    }
    else
      if (energy > 2*niv_nrj)
      then {
	let next_dir = NextDir(dir) in	(* sens horaire *)
	if not (next_dir band See()) 	(* si il n'y a personne sur next_dir: *)
	then {
	  dir := next_dir;
	  etat := 1;
	  compt_dupli := compt_dupli + 1;
          Duplicate(niv_nrj, dir, dupli(dir,1,nb_pas,0)(niv_nrj,nb_pas+1,nb_dupli,nb_skip))
	}
	else  
	  Skip
      }
      else
	Skip
  else
    if (etat = 1)
    then
      if (compt_pas = 0)
      then {
	etat := 0;
	Skip
      }
      else {
	compt_pas := compt_pas - 1;	
	Move(dir)
      }
    else
      if compt_pas = nb_skip
      then {
	etat := 0;
	Skip
      }
      else Skip
;;

@dupli(0,0,0,0)(100,1,3,3)
