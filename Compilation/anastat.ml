open Ast;;

exception RecursiveCall of string;;
exception BadCall of string;;
exception BadConstante of string;;
exception BadFunctionCost of string;;
exception BadDuplication of string;;
exception BadArite of string*int*int*int*int;; (* nom de la fonction, vraie arité, arité trouvée *)
exception BadAriteFonc of string*int*int (* nom de la fonction, vraie arité, arité trouvée pour les fonctions built-in *)
exception BadUseVar of string;;
exception BadArgument of string;;
exception BadLetIn of string;;


let adn_courant = ADN([],"test",[],[]) ;; 

let ht_dependencies_main = Hashtbl.create 0;; 

let ht_dependencies_string = Hashtbl.create 0;;

let ht_mem_size_main = Hashtbl.create 0;; 

type e_type =  INT | DIR | MESS;; 

(* **************************** Toolkit ******************************* *)

let rec enlever_doublons l = match l with
  |[] -> []
  |x::xs -> if List.mem x xs then enlever_doublons xs else x::(enlever_doublons xs)
;;

let projection_adn (a : adn) = match a with
  |ADN(a,_,_,_) -> a 
;; 

let projection_decl (dp : decl_prog) = match dp with
  |DecProg(_,_,_,p) -> p
;;

(* Permet d'obtenir la liste des noms des programmes *)
let get_names (a : adn) = 
  List.map (fun a -> match a with |DecProg(n,_,_,_) -> n) (projection_adn a)
;;


(* mêle chose mais avec les arites aussi désormais *)
let get_names_arites (a : Ast.adn) = 
  List.map (fun a -> match a with |DecProg(n,e1,e2,_) -> (n,List.length e1, List.length e2)) (projection_adn a)
;;


(* ************************ Fin Toolkit ******************************** *) 


(* ****************** Début de la gestion des dépendances ************************ *) 


let create_ht_dependencies (a : adn) =
  let ht_origine = Hashtbl.create 0 in       (* ce que l'on veut à la fin *)
  let ht_main = Hashtbl.create 0 in          (* dépendances sous forme string -> string list *)
  let ht_directe = Hashtbl.create 0 in       (* dépendances directes string -> string list *)
  let ht_prog = Hashtbl.create 0 in          (* string -> prog list *)
  let liste_prog = ref [] in                 (* la liste des programmes sous forme string list *)
  
  let rec direct_dependencies (p : prog) = match p with
    | IFTE(_,p1,p2) -> (direct_dependencies p1) @ (direct_dependencies p2)
    | IFTE_M(_,_,_,p1,p2) -> (direct_dependencies p1) @ (direct_dependencies p2)
    | LETIN(_,_,p1) -> direct_dependencies p1
    | Action_c(_,p1) -> direct_dependencies p1
    | Action(Call(s,_,_)) -> [s]
    | Action(_) -> []
    | Seq(p_l) -> begin 
      let rec aux l acc= match l with
	|[] -> acc
	|x::xs -> aux xs ((direct_dependencies x)@acc)
      in 
      aux p_l [] 
    end 
  in 
      

  let rec remplir_ht_prog_directe (dl : decl_prog list) = match dl with
    |[] -> () 
    |DecProg(s,_,_,p)::xs -> liste_prog := s::(!liste_prog) ; Hashtbl.add ht_prog s p; let l = direct_dependencies p in Hashtbl.add ht_directe s l; remplir_ht_prog_directe xs 
  in 

remplir_ht_prog_directe (projection_adn a); 


  let rec remplir_ht_main (deja_traite : string list) (resultat : string list) (file_attente : string list) = match file_attente with
    |[] -> resultat
    |x::xs when (List.mem x deja_traite) -> remplir_ht_main deja_traite resultat xs
    |x::xs -> remplir_ht_main (x::deja_traite) (enlever_doublons (resultat@(Hashtbl.find ht_directe x))) (enlever_doublons (file_attente@(Hashtbl.find ht_directe x)))
  in

(*
  let rec remplir_ht_main (deja_traite : string list) (file_attente : string list) = match file_attente with
    |[] -> deja_traite 
    |x::xs when (List.mem x deja_traite) -> remplir_ht_main deja_traite xs 
    |x::xs -> remplir_ht_main (enlever_doublons (deja_traite@(Hashtbl.find ht_directe x))) (enlever_doublons (file_attente@(Hashtbl.find ht_directe x))) 
  in 
*)  
  List.iter (fun s -> Hashtbl.add ht_main s (remplir_ht_main [] (Hashtbl.find ht_directe s) (Hashtbl.find ht_directe s))) (!liste_prog); 
  
  Hashtbl.iter (fun a b -> (Hashtbl.add ht_origine a (List.map (fun x -> Hashtbl.find ht_prog x) b ) ) ) ht_main;

  (ht_main,ht_origine) 
;; 



(* let ht_dependencies = create_ht_dependencies adn_courant;; *) 
 
let init_dependencies (a : adn) = let (ht1,ht2) = create_ht_dependencies a in 
				  Hashtbl.iter (fun a b -> Hashtbl.add ht_dependencies_main a b) ht2;
				  Hashtbl.iter (fun a b -> Hashtbl.add ht_dependencies_string a b) ht1
;; 
 
let dependencies (s : string) = Hashtbl.find ht_dependencies_main s;; 

(* ******************* Fin de la gestion des dépendances *****************************)



(* ****************** Début de la fonction qui renvoie la taille du code ************************ *)


let rec prog_size (p : prog) = match p with 
  | IFTE(_,p1,p2) -> 1 +(prog_size p1) + (prog_size p2) 
  | IFTE_M(_,_,_,p1,p2) -> 1 +(prog_size p1) + (prog_size p2) 
  | LETIN(_,_,p1) -> 1 +(prog_size p1) 
  | Action_c(_,p1) -> 1 + (prog_size p1) 
  | Action(_) -> 1  
  | Seq(pl) -> 1 + (List.fold_left (fun acc x -> acc + (prog_size x)) 0 pl) 
;; 

(* TODO : Antoine ou Guillaume, quand vous calculez code_size pl, il manque le code de la fonction dont pl est l'ensemble des dépendances, de sorte que l'on obtient 0 sur l'exemple 7. Il faut donc propager cette information et l'ajouter au résultat à un endroit ou un autre *)
let rec code_size (pl : prog list) = match pl with 
  |[] -> 0 
  |p::xs -> (prog_size p) + (code_size xs) 
;; 

(* ****************** Fin de la fonction qui renvoie la taille du code ************************ *) 


(* ******************************** Fonctions mem_size **************************************** *) 


let create_ht_mem_size (a : adn) =  

  let ht_mem = Hashtbl.create 0 in      (* ce que l'on veut à la fin *) 
  let ht_decl_prog = Hashtbl.create 0 in         (* string -> decl_prog list *) 

  List.iter (fun x -> match x with |DecProg(s,_,_,_) -> Hashtbl.add ht_decl_prog s x) (projection_adn a); 

  let rec nombre_reference liste = match liste with
    |[] -> 0 
    |Addr(_)::xs -> 1 + (nombre_reference xs) 
    |x::xs -> nombre_reference xs 
  in 

  let rec remplir_ht_mem (dl : decl_prog) = match dl with 
    |DecProg(s,l,_,p) when (Hashtbl.mem ht_mem s) -> () 
    |DecProg(s,l,_,p) -> Hashtbl.add ht_mem s ((List.length l) + (taille_call p)) 

  and taille_call (p : prog) = match p with 
    | IFTE(_,p1,p2) -> taille_call p1 + taille_call p2 
    | IFTE_M(_,_,_,p1,p2) -> taille_call p1 + taille_call p2 
    | LETIN(_,_,p1) -> taille_call p1 
    | Action_c(_,p1) -> taille_call p1 
    | Action(Call(nom,exp_add_l,_)) when Hashtbl.mem ht_mem nom -> (Hashtbl.find ht_mem nom) - (nombre_reference exp_add_l) 
    | Action(Call(nom,exp_add_l,_)) -> remplir_ht_mem (Hashtbl.find ht_decl_prog nom); (Hashtbl.find ht_mem nom) - (nombre_reference exp_add_l)  
    | Action(_) -> 0 
    | Seq(pl) -> List.fold_left (fun acc x -> acc + taille_call x) 1 pl 
  in 

  List.iter (fun x -> remplir_ht_mem x) (projection_adn a); 

  ht_mem 
;; 



(* let ht_mem_size = create_ht_mem_size adn_courant ;; *) 


let init_mem_size (a : adn) = let ht = create_ht_mem_size a in 

				 Hashtbl.iter (fun a b -> Hashtbl.add ht_mem_size_main a b) ht 
;;

let mem_size (s : string) = Hashtbl.find ht_mem_size_main s;;


(* ******************************* Fin mem_size ********************************************** *)



(* *********************************** Début du typage *************************************** *)
(* Finalement, DIR et VECT_DIR sont des entier, et MESS des strings *)

(* Cahier des charges  |-| -> signifie "fait"
   |-| On n'autorise pas les appels récursifs : il faut juste checker avec dépendencies si pas d'appels à soi-même
   |-| Appels de fonctions uniquement de fonctions qui existent
   Utilisation des variables seulement si déjà affectées auparavant
   Vérification des typages des expressions (pas de DIR + INT... ou de choses moches du genre...
   ... et par extensions, vérifications du typage des programmes, de l'adn... 
   Vérifier l'arité des fonctions, des calls et des duplications  (bon nombre d'arguments toutça toutça...)
   |-| Vérifier qu'on appelle pas de fonction primitive (energie toutça...) qui n'existe pas, et avec la bonne arités + Qu'on apelle bien des trucs qui existent quand on se duplique
   Vérifier les trucs du genre LETIN s = e IN prog et Action_c of assign * prog <- premier cas, s n'a pas besoin d'être déclaré avant


*)



(* ht_dependencies_main *)

(* Vérification qu'il n'y a pas d'appels récursif *)

let check_recursive_call (a : Ast.adn) = 
  let ln = get_names a in
  List.iter (fun x ->  if List.mem x (Hashtbl.find ht_dependencies_string x) then raise (RecursiveCall x) else ()) ln
;;


(* On vérifie maintenant qu'on apelle que des trucs qui existent *)

let check_appels_qui_existent (a : Ast.adn) = 
  let l_n = get_names a in
  let l = List.map (fun x -> match x with |DecProg(_,_,_,p) -> p) (projection_adn a) in
  let rec aux (p : prog) = match p with
    | IFTE(_,p1,p2) -> aux p1; aux p2
    | IFTE_M(_,_,_,p1,p2) ->  aux p1; aux p2
    | LETIN(_,_,p1) -> aux p1
    | Action_c(_,p1) -> aux p1
    | Action(Call(n,_,_)) -> if  (List.mem n l_n) then () else raise (BadCall n)
    | Action(_) -> ()
    | Seq(pl) -> List.iter (fun x -> aux x) pl
  in

  List.iter (fun x -> aux x) l
;;


(* ******************** Vérification fonctions primitives (nom + arité) + constantes primitives (nom)  ******************** *)
(* ******************** Plus vérification que lorsque l'on se duplique, on apelle bien un truc qui existe ***************** *)
let check_fonc_primitive (a : adn) = 
  let l = List.map (fun x -> match x with |DecProg(_,_,_,p) -> p) (projection_adn a) in
  let ln = get_names a in
(* la fonction aux_const rentre en conflit avec mon built_in du lexer, il faudra en choisir un des deux *)
  let aux_const (s : string) = match s with
    |"energy" -> ()
    |"age" -> ()
    |"gen" -> ()
    |"Age_death" -> ()
    |_ -> raise (BadConstante s)
  in

  let check_fonc_arite s i x = if x <> i then (raise (BadAriteFonc(s,i,x))) else () in

  let aux_fonc (s : string) (n : int) = match (s,n) with
    |("costDuplicate",x) -> check_fonc_arite "costDuplicate" 2 x
    |("costSendMessage",x) -> check_fonc_arite "costSendMessage" 2 x
    |("costSendEnergy",x) -> check_fonc_arite "costSendEnergy" 2 x
    |("costMove",x) -> check_fonc_arite "costMove" 0 x
    |("NextDir",x) -> check_fonc_arite "NextDir" 1 x
    |("PrevDir",x) -> check_fonc_arite "Prevdir" 1 x
    |("OpDir",x) -> check_fonc_arite "OpDir" 1 x
    |("rand",x) -> check_fonc_arite "rand" 0 x
    |_ -> raise (BadFunctionCost s)
  in
  

  let rec aux_exp (e : expr) = match e with
    (*|Bool of bool : On est en C++, pas de booléens, tout le monde est un entier (Thomas) *) 
    |Const(s) -> aux_const s
      
    |Op_expr(_,e1,e2) -> aux_exp e1; aux_exp e2
      
    |Function(s,e_l) -> aux_fonc s (List.length e_l); List.iter (fun x -> aux_exp x) e_l
    (*    |Dir_vect(e_l) -> List.iter (fun x -> aux_exp x) e_l *)
    |DIR_MESS(_,e1) -> aux_exp e1
    |Sub_expr(_,e1,e2) -> aux_exp e1; aux_exp e2 
(*    |In_cond(e1,_) -> aux_exp e1 *)
    |Concat(liste_ee) -> List.iter (fun x -> match x with (a,b) -> aux_exp a) liste_ee
    |_ -> ()
  in

  let aux_action (a : action) = match a with
    |Move(e) -> aux_exp e
    |Send_M(_,e) -> aux_exp e
    |Send_E(e1,e2) -> aux_exp e1; aux_exp e2
    |Duplicate(e1,e2,s,el1,el2) -> 
      if (List.mem s ln) 
      then 
	begin 
	  aux_exp e1; aux_exp e2; List.iter (fun x -> aux_exp x) el1; List.iter (fun x -> aux_exp x) el1
	end
      else
	raise (BadDuplication s)
	
    |Call(_,eadrel,el) ->  List.iter (fun x -> match x with |Exp(e) -> aux_exp e |Addr(e) -> aux_exp e) eadrel; List.iter (fun x -> aux_exp x) el
    |Skip -> ()
  in

  let rec aux (p : prog) = match p with
    | IFTE(e,p1,p2) -> aux_exp e ;aux p1; aux p2
    | IFTE_M(_,_,e,p1,p2) ->  aux_exp e; aux p1; aux p2
    | LETIN(_,e,p1) -> aux_exp e; aux p1
    | Action_c(Assign(_,e),p1) -> aux_exp e; aux p1
    | Action(a) -> aux_action a
    | Seq(pl) -> List.iter (fun x -> aux x) pl
  in
  List.iter (fun x -> aux x) l
;;

(* ******************* Fin vérif fonctions primitives ************************ *)


(* ************* Vérification de l'arité des prog, call, fonction ************ *)


let check_arite_call_dupli (a : Ast.adn) =
  
  let ht_arite = Hashtbl.create 0 in      (* du type string -> n1,n2 avec n1,n2 le nombre de variables et d'arguments *)

  let liste_names_arites = get_names_arites a in

(* on crée la table de hachage *)
  List.iter (fun x -> match x with (a,b,c) -> Hashtbl.add ht_arite a (b,c)) liste_names_arites;
  
(* on récupère la liste de toutes les duplications et call, avec le nombre d'arguments qui sont données *)

  let rec get_arite (p : prog) = match p with
    | IFTE(_,p1,p2) -> (get_arite p1) @ (get_arite p2)
    | IFTE_M(_,_,_,p1,p2) -> (get_arite p1) @ (get_arite p2)
    | LETIN(_,_,p1) -> get_arite p1
    | Action_c(_,p1) -> get_arite p1
    | Action(Call(s,e1,e2)) -> [(s,List.length e1,List.length e2)]
    | Action(Duplicate(_,_,s,e1,e2)) -> [(s,List.length e1,List.length e2)] 
    | Action(_) -> []
    | Seq(listep) -> 
      let rec aux l acc = match l with
	|[] -> acc
	|x::xs -> aux xs (acc@get_arite(x))
      in
      aux listep []
  in
  
  (* l_g_a une liste de liste des différents appels (call ou diplicate) avec arités *)
  let l_g_a = List.map (fun x -> get_arite (projection_decl x)) (projection_adn a) in
  
  List.iter (fun l -> List.iter (fun x -> match x with |(a,b,c) -> let (u,v) = (Hashtbl.find ht_arite a) in if (u,v)  = (b,c) then () else raise (BadArite (a,b,c,u,v))) l) l_g_a
    
;;





(* ************* Fin de la vérification des différentes arités *************** *)



(* ******************************** Typage ************************************ *)



(* Rien finalement *)
(* Enfin si finalement, Faut pas oublier de checker si on utilise pas de variable qui ont pas été affecté ou quoi que ce soit *)

(*la fonction suivante permet de rajouter les variables nécessaires au contexte d'une fonction built_in particulère si besoin est *)

let personalized_context_function adn = function
  | "costDuplicate" -> let ADN(dec_prog_list,_,_,_) = adn in List.map (function DecProg(s,_,_,_) -> s) dec_prog_list
  | _ -> []

let check_typage (a : Ast.adn) = 
  let rec check_exp (e : Ast.expr) (contexte : string list) = match e with
    | Var(s) -> if List.mem s contexte then () else raise (BadUseVar s)
    | Refvar(s) -> if List.mem s contexte then () else raise (BadUseVar s)	
    |Op_expr(_,e1,e2) -> check_exp e1 contexte; check_exp e2 contexte
	
    | Function(f,el) -> List.iter (fun x -> check_exp x ((personalized_context_function a f )@contexte)) el
    | DIR_MESS(s,e) -> check_exp e contexte;if List.mem s contexte then () else raise (BadUseVar s) 
    | SEE -> () (* *************Warning faudra peut-être changer ça !!!!!! ****************** *)
    | Sub_expr(m,e1,e2) -> check_exp m contexte; check_exp e1 contexte; check_exp e2 contexte
    | Concat(eil) -> List.iter (fun x -> match x with |(a,b) -> check_exp a contexte) eil  
    | _ -> ()
  in

  let check_expaddr (ea : expr_addr) (contexte : string list) = match ea with
    |Exp(e) -> check_exp e contexte
    |Addr(e) -> check_exp e contexte
  in
  
  let rec check_action (a : Ast.action) (contexte : string list) = match a with
    |Move(e1) -> check_exp e1 contexte
    |Send_M(e1,e2) ->  check_exp e1 contexte; check_exp e2 contexte
    |Send_E(e1,e2) -> check_exp e1 contexte; check_exp e2 contexte
    |Duplicate(e1,e2,_,e1l,e2l) -> check_exp e1 contexte; check_exp e2 contexte; List.iter (fun x -> check_exp x contexte) (e1l@e2l)
    |Call(_,eal,el) -> List.iter (fun x -> check_expaddr x contexte) eal; List.iter (fun x -> check_exp x contexte) el
    |Skip -> ()
  in

  let rec check_type_prog (p : Ast.prog) (contexte : string list) = match p with
    | IFTE(e1,p1,p2) -> check_exp e1 contexte; check_type_prog p1 contexte; check_type_prog p2 contexte
    | IFTE_M(s1,dir,e1,p1,p2) -> check_exp e1 (s1::dir::contexte); check_type_prog p1 (s1::dir::contexte); check_type_prog p2 (contexte) (* message apparait dans le nouveau contexte, direction apparait dans le nouveau contexte *)
    | LETIN(s,e1,p1) -> if (List.mem s contexte) then raise (BadLetIn s) else begin check_exp e1 contexte; check_type_prog p1 (enlever_doublons (s::contexte)) end
    | Action_c(Assign(s,e1),p1) -> if (List.mem s contexte) then begin check_exp e1 contexte; check_type_prog p1 (enlever_doublons (s::contexte)) end else raise (BadUseVar s)
    | Action(a) -> check_action a contexte
    | Seq(pl) ->  List.iter (fun x -> check_type_prog x contexte) pl
  in

  let rec get_var f (el : expr list) = match el with
    |[] -> []
    |Var(s)::xs -> s::(get_var f xs)
    |_ -> raise (BadArgument(f))
  in

  List.iter (fun x -> match x with |DecProg(f,e1,e2,p) -> let l1 = get_var f  e1 in let l2 = get_var f  e2 in check_type_prog p (l1@l2)) (projection_adn a)

 
 

(* ******************************** Fin typage ******************************** *)



(* ******************************* Initialisation ******************************************** *)


let check (a : Ast.adn) = 
  check_recursive_call a; 
  check_appels_qui_existent a; 
  check_fonc_primitive a;
  check_arite_call_dupli a;
  check_typage a
;;
  



let init (a : Ast.adn) = 
  init_dependencies a;   
  (*init_mem_size a;*)
  check a;
  init_mem_size a
;;

(* ****************************** Fin initialisation **************************************** *)

let check_type_expr x = true;;
let check_type_adn x = true ;;

(* Provisoire, à décommenter par la suite; en attendant de se mettre d'accord sur le type de MESS, DIR, etc... *)

(*

(* Vérification qu'il n'y a pas de conflicts de noms de prog, et pas de call de quelque chose qui n'existe pas *)

let rec noms_decl_list (l : decl_prog list) = match l with
  |[] -> []
  |DecProg(nom,_,_,_)::xs -> nom::(noms_decl_list xs)
;;

let noms_prog (a : adn) = match a with
  |ADN(d_list,nom_main,_,_) -> (nom_main)::(noms_decl_list d_list)
;;
    
let rec check_doublons_nom_prog (l : string list) = match l with
  |[] -> ()
  |x::xs -> if List.mem x xs then failwith "deux noms de programmes identiques" else check_doublons_nom_prog xs
;;

let projection_adn (a : adn) = match a with
  |ADN(l,_,_,_) -> l
;;

let rec projection_decl_list l = match l with
	|[] -> []
	|DecProg(s,_,_,p)::xs -> p::(projection_decl_list xs)

let rec check_call_prog (p : prog) (l : string list) = match p with
  | IFTE(_,p1,p2) -> check_call_prog p1 l; check_call_prog p2 l;
  | IFTE_M(_,_,_,p1,p2) -> check_call_prog p1 l; check_call_prog p2 l
  | LETIN(_,_,p) -> check_call_prog p l;
  | Action_c(_,p) -> check_call_prog p l;
  | Action(Call(s,_,_)) -> if List.mem s l then () else failwith "Call vers une fonction qui n'existe pas"
  | Seq(p_l) -> List.iter (fun x -> check_call_prog x l) p_l
  |_ -> ()

(* Fin de vérifications des noms de programmes *)

(* Vérification des types *)

let check_type_condition c ht = ();;

let check_type_assignement c ht = ();;
let check_type_action c ht = ();;

let rec type_exp e ht = match e with
  |Var(s) -> if Hashtbl.mem (!ht) s then Hashtbl.find (!ht) s else failwith "utilisation d'une variable non affecté"

  |Refvar(s) -> if Hashtbl.mem (!ht) s then Hashtbl.find (!ht) s else failwith "utilisation d'une variable non affecté"

  |Int(_) -> INT

  |Const(_) -> INT
      
(* Pour op, on peut être un peu moins permissif si on veut... à vérifier... *)
  |Op_expr(op,e1,e2) -> let t1 = type_exp e1 ht in let t2 = type_exp e2 ht in  if (t1 = t2) then t1 else failwith "probleme de type"
      
  |Function(_,e_list) -> let rec aux l = match l with
      |[] -> INT
      |x::xs -> if type_exp x ht = INT then aux xs else failwith "probleme de type"
			 in
			 aux e_list
			 
  |Dir_int(_) -> DIR

  |Dir_vect(e_list) ->  let rec aux l = match l with
      |[] -> INT
      |x::xs -> if type_exp x ht = INT then aux xs else failwith "probleme de type"
			 in
			 aux e_list

  |DIR_MESS(s,c) -> if Hashtbl.mem (!ht) s then begin if (Hashtbl.find (!ht) s) = DIR then begin check_type_condition c ht; DIR end else failwith "prob" end else failwith "prob"

  |SEE(s) -> if Hashtbl.mem (!ht) s then begin if ((Hashtbl.find (!ht) s) <> DIR) then failwith "probl" else DIR end else begin Hashtbl.add (!ht) s DIR; DIR end


  |Message(_) -> MESS

  |Message_cut


let rec check_type_prog (p : prog) ht = match p with
  | IFTE(c,p1,p2) -> check_type_condition c ht; check_type_prog p1 ht; check_type_prog p2 ht

  | IFTE_M(s,s2,c,p1,p2) -> check_type_condition c ht; check_type_prog p1 ht; check_type_prog p2 ht

  | LETIN(s,e,p) -> let t = type_exp e ht in if Hashtbl.mem (!ht) s then begin if (Hashtbl.find (!ht) s = t) then () else failwith "probleme de type" end else Hashtbl.add (!ht) s t ;check_type_prog p ht

  | Action_c(a,p) -> check_type_assignement a ht; check_type_prog p ht

  | Action(a) -> check_type_action a ht

  | Seq(proglist) -> List.iter (fun x -> check_type_prog x ht) proglist


let check_type_adn_aux (a : adn) (ht (* ref de hashtbl *)) = match a with
  |ADN(d_list,_,_,_) -> List.iter (fun x -> check_type_prog x ht) (projection_decl_list d_list)
;;

(* Vérification de l'arité des fonctions *)

let rec check_arite_exp (e : expr) = match e with      
  |Op_expr(_,e1,e2) -> check_arite_exp e1; check_arite_exp e2

(* Ce sera à remplacer par les bonnes valeurs pour chaque fonction *)      
  |Function(nom,e_list) -> List.iter (fun x -> check_arite_exp x) e_list;
    begin match nom with
      |_ -> ()
    end
      
  |Dir_vect(e_list) -> List.iter (fun x -> check_arite_exp x) e_list;

  |Message_cut(_,e1,e2) -> check_arite_exp e1; check_arite_exp e2
      
let rec check_arite_action (a : action) = match a with
(*  |Move(e) of expr (* déplacement *)
  |Send_M(_,e) of message * expr 
  |Send_E(e1,e2) of expr * expr
  |Duplicate(e1,e2,_,_,_) of expr * expr * string * expr list * expr list (* modification due à l'ajout d'arguments distincts des variables *)
  |Call(_,_,_) -> of string * expr_addr list * expr list (* appel de fonction *) *)
  |Skip -> ()



let rec check_arite_prog (p : prog) = match p with
  | IFTE(_,p1,p2) -> check_arite_prog p1; check_arite_prog p2
  | IFTE_M(_,_,_,p1,p2) -> check_arite_prog p1; check_arite_prog p2
  | LETIN(_,_,p) -> check_arite_prog p 
  | Action_c(_,p) -> check_arite_prog p
  | Action(a) -> check_arite_action a
  | Seq(prog_list) -> List.iter (fun x -> check_arite_prog x) prog_list


(*
let check_type_adn (a : adn) =
  let liste_noms_prog = noms_prog a in 
  check_doublons_nom_prog liste_noms_prog;
  List.iter (fun x -> check_call_prog x liste_noms_prog) (projection_adn a);
(* On vient de finir de regarder si deux fonctions ont même noms, et si on n'appelle pas une fonction qui n'existe pas *)
(* On regarde maintenant les éventuels problèmes d'arité *)
  List.iter (fun x -> check_arite_prog x liste_noms_prog) (projection_adn a);  

(* On passe maintenant à la vérifiction du typage proprement dit *)
  let ht = ref (Hashtbl.create 0) in
  check_type_adn_aux a ht;
*)



*)

(* ***************** Fin du typage **********************)
