open Ast2

type destination (* peut-être unit si on garde print_string, peut-être string sinon *)

val affiche : destination -> string

val default_dest : destination

val concat_dest : destination -> destination -> destination (* pour simuler List.fold *)

val write_header : unit -> destination

val write_code_size : int -> destination

val write_nom_main : string -> destination

val write_mem_init : int -> Ast2.expr list -> destination

val write_action : Ast2.action -> destination

val write_assignment : Ast2.assignement -> destination

val write_expr : Ast2.expr -> destination

val write_cond : Ast2.expr -> destination

val write_prog : Ast2.prog -> destination

val write_decl_prog : Ast2.decl_prog -> destination

val find_main : string -> Ast2.prog list -> Ast2.prog

val transform : Ast2.adn -> destination (*Convertit vers le c++*)
