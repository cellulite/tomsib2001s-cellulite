(* l'ast *)

exception Generic_Error of string
      
let vraiNom = function
  | "E_init" -> "env.E_init()"
  | "E_sun" -> "env.E_sol()"
  | "E_death" -> "env.E_mort()"
  | "Age_death" -> "env.A_mort()"
  | "age" -> "cell->age()"
  | "gen" -> "cell->gen()"
  | "energy" -> "cell->nrj()"
  | "NextDir" -> "NextDir("
  | "OpDir" -> "OpDir("
  | "PrevDir" -> "PrevDir("
  | "rand" -> "rand("
  | "costDuplicate" -> "env.costDuplicate("
  | "costSendMessage" -> "costSendMessage(env, "
  | "costSendEnergy" -> "costSendEnergy(env, "
  | "costMove" -> "env.costMove("
  | s -> s (* ceci a un potentiel pour des erreurs. L'analyse statique doit se débrouiller pour que cela ne soit pas le cas *)
	  
type op = 
  |Plus 
  |Times
  |Minus
  |Div
  |Greater
  |Geq
  |Low
  |Leq
  |Eq
  |Neq
  |Mod
  |Or
  |And
  |Band
  |Bor


(*type ichar = Regexp.ichar
type regexp = Regexp.regexp*)
(* Je réutilise le type du td de compilation de cette année pour les regexp :  *)
(* type ichar = char * int;;

type regexp = 
  | Epsilon
  | Character of ichar
  | Union of regexp * regexp
  | Concat of regexp * regexp
  | Star of regexp;; *)
      
      
(*
  type cond =
  Var_cond of string
  |Op_cond of op * expr * expr
  |In_cond of string * regexp
*)
      

type message = expr
and
expr =
  | Var of string
  | Refvar of string (* pour les appels par référence *)
  | Int of int
  (*|Bool of bool : On est en C++, pas de booléens, tout le monde est un entier (Thomas) *) 
  | Const of string (* Constantes : âge, énergie ...*)
      
  | Op_expr of op*expr*expr
  | Not of expr    
  | Function of string * expr list (* fonctions d'énergie, etc... *)
  (*|Dir_int of int
  |Dir_vect of expr list*)
  | DIR_MESS of string * expr (* le deuxième est une condition *)
  | SEE (*(*of string*) On a enlevé le string parce qu'on trouvait ça débile *)
  | Sub_expr of message * expr * expr (* Message[i:j] : attention aux segfault *)
  | Concat of (expr*int) list (* Concat([(m1,t1);(m2,t2)]) est le message formé des t1 premiers bits de m1 suivi des t2 premiers bits de m2 *) 
type expr_addr =
  | Exp of expr
  | Addr of expr (* où l'expr ne peut être QUE une variable, cf. ../Documents/regles_langage/regles_langage.pdf *)
      
type assignement = 
    Assign of string * expr

type action = 
  |Move of expr (* déplacement *)
  |Send_M of message * expr
  |Send_E of expr * expr
  |Duplicate of expr * expr * string * expr list * expr list (* modification due à l'ajout d'arguments distincts des variables *)
  |Call of string * expr_addr list * expr list (* appel de fonction *)
  |Skip
      
      
type prog =
  | IFTE of expr * prog * prog
  | IFTE_M of string * string * expr * prog * prog (* message, direction, condition, then,else *)
  | LETIN of string * expr * prog (* string n'a pas besoin d'être déclarée avant *)
  | Action_c of assignement * prog (* assignement d'un truc déjà déclaré *)
  | Action of action
  | Seq of prog list (* et pas action liste, qui serait plus restrictif...On est d'accord? *) (* Ajouté à la suite de la modification de la grammaire *)

type decl_prog = 
    DecProg of string * expr list (* les variables *) * expr list (* les arguments *)  * prog (* à vérifier dans l'AS : l'expr list n'est pas faite avec n'importe quelle expr, mais avec des variables (et pas des messages par exemple) *)


type adn = 
    ADN of decl_prog list * string * expr list * expr list (* le nom du main *)
