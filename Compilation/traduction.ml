(* Ce fichier implémente la traduction vers le C++ *)
open Ast
open Anastat
open Ast2

let p x = x
and pl x = (x)^("\n")
and pi = string_of_int
and pn = "\n"

let rec exp_rap x = function
  | 0 -> 1
  | 1 -> x
  | n -> let u = exp_rap x (n/2) in u*u*(match (n mod 2) with |0 -> 1 | _ (* Pattern matching is exhaustive *) -> x)

type destination = string

let affiche x = x

let default_dest = ""
let concat_dest (d1 : destination) (d2 : destination) = (d1^d2) 
let soi = string_of_int
let ios = int_of_string

let rec map_dest = function
  |[] -> ""
  |h::t -> concat_dest h (map_dest t)


let startBloc = pl "{"
and endBloc = pl "}\n"

let myself = "cell" (* le texte qui va désigner la cellule auprès d'elle-même dans les appels aux fonctions correspondant aux actions, je ne sais pas encore quoi mettre*)
let env = "env" (* Que faut-il mettre? *)

let compt_assign = ref 0 (* compteur pour les listes d'assignements avant les duplications, pour ne pas avoir de collisions de noms *)

(* en-tête du fichier *)
let rec write_header () = 
  begin
    map_dest
    [
      pl "#include <iostream>";
      pl "#include <time.h>";
      pl "#include <stdio.h>";
      pl "#include <stdlib.h>";
      pl "#include \"../environnement/env.h\"";
      pl "#include \"../environnement/dir_managing.h\"";
      pl "using namespace std;";
      pn
    ]
  end

and
    write_code_size size =
  map_dest
    [
      pl "extern \"C\"";
      startBloc;
      p "int taille_code = " ; pi size ; p ";"; pn;
      endBloc;
      pn
    ]
    
and
    write_nom_main sName =
  map_dest
[
  pl "extern \"C\"";
  startBloc;
  pl (("  char nom_main[] = \"")^sName^("\";"));
  endBloc;
  pn
]

and
    declare_debut debut =
  map_dest
[
  pl "extern \"C\"";
  startBloc;
  pl (("int debut = ")^soi(debut)^(";")); (* 0 est provisoire... *)
  endBloc;
  pn
]    

and
    write_op (op,e1,e2) = 
  match e2 with
    |Int(0) when (op = Div) ->  p "(env.rm_cell(cell, 0))"; (* on tue la cellule si elle essaye de diviser par zéro, après tout c'est un péché capital *)
    | _ -> match op with
	| Div ->
	  begin
	    incr(compt_assign);
	    map_dest
	      [
		p "((";
		write_expr e2;
		p ")";
		p " == 0)?";
		p "(env.rm_cell(cell, 0))"; (* il faudrait peut-être lui envoyer un mail pour lui dire qu'il a divisé par zéro *)
		p " :";
		p "((";
		write_expr e1;
		p ")";
		p " / ";
		p "(";
		write_expr e2;
		p "))"
	      ]
	  end
	| _ -> 
	  begin
	    map_dest
	      [
		p "(";
		write_expr e1;
		p ")";
		p (string_op op);
		p "(";
		write_expr e2;
		p ")"
	      ]
	  end
	    
and
    subst newExpr expr = function
      | Op_expr(o,e1,e2) -> Op_expr(o,subst newExpr expr e1,subst newExpr expr e2)
      | DIR_MESS(s,e1) -> DIR_MESS(s,subst newExpr expr e1)
      | Sub_expr(m,e1,e2) -> Sub_expr(m,subst newExpr expr e1,subst newExpr expr e2)
      | Concat(l) -> Concat(List.map (function (y,z) -> (subst newExpr expr y,z)) l)
      | y -> if (y=expr) then newExpr else y

and
    write_expr = function (* ne met pas de ";" après les instructions *)
      | Not(e) -> 
	map_dest
	  [
	    p "!(";
	    write_expr e;
	    p ")";
	  ]
      | Var(sName) -> p sName; (* c'est forcément une variable de let in *)
      | Refvar(aAddr) -> write_addr(aAddr)
      | Int(i) -> pi i
      | Const(s) -> p ((Ast.vraiNom s))
      | Op_expr(op,e1,e2) -> write_op(op,e1,e2)
      | Function("costDuplicate", nrj :: Var(nomFonc) :: _) -> 
    begin
      map_dest
        [
          p "env.costDuplicate(";
          pi (code_size (dependencies nomFonc));
          p ", ";
          write_expr nrj;
          p ")"
        ]
    end
      | Function(sName,arglist) ->
	begin 
	  map_dest
	    [
	      p (Ast.vraiNom(sName));
(*	      p "(";*)
	      (
		match arglist with
		  | [] -> default_dest
		  | head::tail -> 
		    begin
		      map_dest
			[
			  write_expr head;
			  List.fold_right (fun exp dest -> concat_dest (p " , ") (concat_dest (write_expr exp) (dest))) (tail) default_dest; (* écrit la suite des arguments *)
			]
		    end
	      );
	      p ")"
	    ]
	end
     (* | Dir_int(i) -> pi i;
	| Dir_vect(l) -> default_dest (* en suspens *)*)
      | DIR_MESS(sMess,cond) -> 
	map_dest
	  [
	    (* ici il faut déclarer un vecteur de directions *)
	    p "(";
	    let rec aux = function
	      | 0 -> map_dest
		[
		  p "((int) ";
		  write_expr (subst ((Var ("cell->mess_load((Direction) "^(soi(0))^")"))) (Var sMess) cond);
		  p ")* pow(2,";
		  pi 0;
		  p ") + ";
		]
	      | i -> map_dest
		[
		  p "((int) ";
		  write_expr (subst (Var ("cell->mess_load((Direction) "^(soi(i))^")")) (Var sMess) cond);
		  p ")* pow(2,";
		  pi i;
		  p ") + ";
		  aux(i-1)
		]
	    in aux(7)
	  ]
      | SEE -> 
	p "env.see(cell)";
	
(*
	    pl "bool* dir_tmp = int_to_dir_vect(); ";
	    pl "bool* x_tmp = new bool [8]; "; (* réfléchir plus tard s'il faut prendre la peine d'indexer x_tmp par un compteur, je pense que non mais je pourrais me tromper *)
	    pl "env.voir(cell,x_tmp);";
	    write_expr e;
	    pl ("= dir_vect_to_int(x_tmp);");
	  ]*)
      | Sub_expr(exprMess,eDeb,eLength) -> 
	map_dest
	  [
	    p "(((";
	    write_expr eDeb;
	    p " ) >= 0) && ((";
	    write_expr eLength;
	    p " ) >= 0 ) && (((";
	    write_expr eDeb;
	    p ") + (";
	    write_expr eLength;
	    p "))))?"; (* on vérifie que tout ça est légitime *) 
	    (* then *)
	    p "((";
	    write_expr exprMess;
	    p ") >> (";
	    write_expr eDeb;
	    p "))";
	    p "% (";
	    p "1 << (";
	    write_expr eLength;
	    p ")):";
	    (* else *)
	    p "-1"; (* si tu ne sais pas borner tes bornes tu te prends un -1 *)
	  ]
      | Concat(l) -> let rec aux = function
	  | ([],_) -> p ""
	  | ([(x,y)],n) -> 
	    map_dest
	      [
		p "((";
		write_expr x;
		p ") % ";
		pi (exp_rap 2 y);
		p ") * ";
		pi (exp_rap 2 n)
	      ]
	  | ((x,y)::t,n) -> 
	    map_dest
	      [
		p "((";
		write_expr x;
		p ") % ";
		pi (exp_rap 2 y);
		p ") * ";
		pi (exp_rap 2 n);
		p " + ";
		(aux(t,n+y));
	      ]
		     in aux(l,0) 

and
    write_mem_init mem_size assign_list=  
  let write_assignment_mem index = function (* on en a besoin car les assignements de la mémoire sont des assignements de tableaux *)
    | e -> 
      map_dest
	[
	  p "mem"; 
	  p "[" ; 
	  pi index ; 
	  p "] = " ; 
	  write_expr e;
	  p ";";
	  pn
	]
  in
  begin
    map_dest
      [
	pl "extern \"C\"";
	startBloc;
	pl "void mem_init(Memoire& mem, Cell* cell)";
	startBloc;
	pl (("mem.set_taille(")^(soi mem_size)^(");"));
	(let rec aux = function
	  | ([],_) -> default_dest
	  | (h::t,k) -> concat_dest (write_assignment_mem k h) (aux(t,k+1))
	in aux(assign_list,0));
	endBloc;
	endBloc
      ]
  end

and
    write_action = function (* fait les ";" et les \n *)
      | Move(e) -> 
	begin
	  map_dest
	    [
	      p "env.Deplacement(";
	      p myself;
	      p " , (Direction) ";
	      p "int_to_dir(";
	      write_expr e;
	      p ")";
	      pl ");"
	    ]
	end
   (*   | Send_M(Mess(s),e) -> map_dest
	[
	  p "sendMessage(cell,";
	  p "int_to_dir_vect(";
	  write_expr e;
	  p ")";
	  p ",";
	  p "\"";
	  p s;
	  p "\"";
	  pl ");";
	]
      | Send_E(e1,e2) -> (*rappel : énergie, direction *)
	map_dest
	  [
	    p "sendEnergy(cell,";
	    p "int_to_dir_vect(";
	    write_expr e2;
	    p "),";
	    write_expr e1;
	    pl ");";
	]
   *)
      | Send_M(s,e) -> map_dest
	[
	  startBloc;
	  p "if (is_dir(";
	  write_expr e;
	  pl"))";
	  startBloc;
	  p ("bool* dir_tmp = int_to_dir_vect("); (* faut il indexer?*)
	  write_expr e;
	  pl "); "; 
	  p "env.sendMessage(cell,dir_tmp,";
	  write_expr s; (* ceci devra être changé si on veut implémenter int_of_string et string_of_int *)
	  pl ");";
	  pl "delete [] dir_tmp;";
	  endBloc;
	  pl "else";
	  p "return;";
	  endBloc;
	]
      | Send_E(e1,e2) -> 
	map_dest
	  [
	    p "if (is_dir(";
	    write_expr e2;
	    pl"))";
	    startBloc;
	    p ("bool* dir_tmp = int_to_dir_vect(");
	    write_expr e2;
	    pl ");";
	    p "env.sendEnergy(cell,dir_tmp,";
	    write_expr e1;
	    pl ");";
	    pl "delete [] dir_tmp;"; 
	    endBloc;
	    pl "else";
	    p "return;";
	  ]
      | Duplicate(exprEnergy,exprDir,sNewMain,varList,argList,codeSize) ->
	let val_compt = !compt_assign in
	begin
	  incr(compt_assign);
	  let rec aux15 n = function
		| [] -> default_dest
		| t::q -> concat_dest (map_dest [p "assignment__"; pi val_compt; p ".push_back(complex<int> ("; pi n; p ", "; write_expr t; pl "));"]) (aux15 (n + 1) q) in
	  map_dest
	    [
	      p "assign assignment"; p "__"; pi val_compt;  pl ";";
	      aux15 0 varList;
    	      (let rec aux k = function
    		| [] -> default_dest
    		| [h] -> 
		  map_dest 
		    [
		      p "args[";
		      pi k;
		      p "] = ";
		      write_expr h;
		      pl ";";
		    ]
    		| h::t ->  concat_dest 
		  (map_dest 
		     (
		       [
			 p "args[";
			 pi k;
			 p "] = ";
			 write_expr h;
			 pl ";";
		       ]
		     )
		  ) 
		  (aux (k+1) (t))
    	       in map_dest 
	       [
		 p "int* args = new int[";
		 pi (List.length argList);
		 pl "];";
		 aux 0 argList;
	       ]);
	      (let rec aux n = function
		| [] -> default_dest
		| [t] -> 
		  map_dest
		    [
		      p "vars[";
		      pi n;
		      p "] = ";
		      pi n;
		      pl ";";
		    ]
		| t::q -> concat_dest 
		  (
		    map_dest 
		      [
			p "vars[";
			pi n;
			p "] = ";
			pi n;
			pl ";";
		      ]
		  ) 
		  (aux (n + 1) q)
	       in map_dest [
		 p "int* vars = new int[";
		 pi (List.length varList);
		 pl "];";
		 aux 0 varList;]);
	      p "if (is_dir(";
	      write_expr exprDir;
	      pl "))";
	      startBloc;
	      p "env.Duplication(";
	      p myself;
	      p " , (char*) ";
	      p "\"";
	      p sNewMain;
	      p "\"";
	      p " , ";
	      pi codeSize;
	      p " , (Direction) int_to_dir( ";
	      write_expr exprDir;
	      p ") , ";
	      write_expr exprEnergy;
	      p " ,args, vars, ";
	      pi (List.length varList);
	      p ", assignment"; p "__";  pi val_compt; pl ");";
	      endBloc;
	      pl "else";
	      p "return;";
	      pn;
	    ]
	end
      | Call(sNameFun,addr_list,arg_list,assign_liste,iDebut) -> 
	let rec aux_addr = function (* pour remplir les tableaux d'arguments et d'indices en parcourant la liste à l'aide d'une définition du type int t[] = {x_1, ...,x_n}; *)
	  | [] -> default_dest
	  | [h] -> write_addr_var_call h
	  | h::t -> concat_dest (concat_dest (write_addr_var_call h) (p ", ")) (aux_addr(t))
	in
	let rec aux_addr_expr = function (* pour remplir les tableaux d'arguments et d'indices en parcourant la liste à l'aide d'une définition du type int t[] = {x_1, ...,x_n}; *)
	  | [] -> default_dest
	  | [h] -> write_expr h
	  | h::t -> concat_dest (concat_dest (write_expr h) (p ", ")) (aux_addr_expr(t))
	in
	let val_compt = !compt_assign in (* pour donner un nom injectif à l'assignement*)
	map_dest
	  [
	    concat_dest  (p (("int tableau_indices_vars[")^(soi(List.length addr_list))^("] = {"))) (concat_dest (aux_addr(addr_list)) (p "} ; \n"));
	    concat_dest  (p (("int tableau_valeurs_args[")^(soi(List.length arg_list))^("] = {"))) (concat_dest (aux_addr_expr(arg_list)) (p "} ;\n"));
	    ( let rec aux15 n = function
	      | [] -> default_dest
	      | t::q -> concat_dest (map_dest [p "assignment__"; pi val_compt; p ".push_back(complex<int> ("; pi n; p "+ debut , "; write_expr t; pl "));"]) (aux15 (n + 1) q) in
	      (map_dest
		 [
		   p "assign assignment"; p "__"; pi val_compt;  pl ";";
		   aux15 0 arg_list;
    		 ]
	      )
	    );    
	    pl ("env.appelPreambule(cell,assignment__"^(soi(val_compt))^");");
	    p sNameFun; 
	    p "("; 
	    p myself; 
	    p " , ";
	    p env;
	    p " , ";
	    p "tableau_indices_vars, tableau_valeurs_args,";
	    pi iDebut;
	    p "+debut);";
	  ]
      | Skip -> pl "return;"	

and
    write_assignment = function (* fait les ";" et les \n *)
      | Assign(eVar,e) -> 
	begin
	  map_dest
	    [
	      write_expr eVar ; 
	      p " = "; 
	      write_expr e;
	      p ";";
	      pn
	    ]
	end

and
    string_op (op : Ast.op) = match op with
      | Or -> " || "
      | And -> " && "
      | Plus -> " + "
      | Times -> " * "
      | Minus -> " - "
      | Div -> " / "
      | Greater -> " > "
      | Geq -> " >= "
      | Low -> " < "
      | Leq -> " <= "
      | Eq -> " == "
      | Neq -> " != "
      | Mod -> " % "
      | Band -> " & " (* TODO *)
      | Bor -> " | " (* TODO *)

and
    write_cond x = write_expr x
  (* function
      | Var_cond(eVar) -> write_expr eVar (*il me semble qu'on est forcément dans un let in, donc sVar existe en C++ *)
      | Op_cond(op,c1,c2) ->  write_op(op,c1,c2) 
      | In_cond(sMess,regexp) -> default_dest (* aux calendes grecques celui-ci *)
  *)		

and 
    write_prog = function
      | IFTE(cond,p1,p2) -> 
	begin
	  map_dest
	    [
	      p "if(" ;
	      write_cond(cond); (* ou  faut-il la précalculer dans une variable pour alléger le code ? *)
	      pl ")";
	      startBloc;
	      write_prog p1;
	      endBloc;
	      pl "else";
	      startBloc;
	      write_prog p2;
	      endBloc;
	    ]
	end
      | IFTE_M(sMess,dirExpr,cond,pThen,pElse) -> (* On doit chercher s'il existe un message sMess et une direction dirExpr (qui doit être une variable) vérifie la condition cond, puis appliquer le résultat correspondant en donnant une portée aux variables si ça existe *)
	map_dest
	  [
	    pl "bool __b = false;";
	    pl "int __i = 0;";
	    pl "while(!__b && __i<8)";
	    startBloc;
	    (* on regarde le message dans la direction i *)
	    p "Message "; 
	    p sMess;
	    pl "= cell->mess_load((Direction) __i );";
	    p "if((!INT_MIN == "^(sMess)^") && ( "; 
	    write_expr ((subst (Var ("cell->mess_load((Direction) __i ) "))) (Var sMess) (subst (Var("__i")) (Var dirExpr) cond)); (* la condition dans laquelle on a effectué les substitutions de sMess et dirExpr par les valeurs courantes dépendant de i *) (* il faudra rajouter la condition : "on a effecivement reçu un message dans cette direction" *)
	    pl "))";
	    startBloc;
	    pl "__b = true;";
	    pl ("int "^(dirExpr)^" = __i;");
	    write_prog pThen;
	    endBloc;
	    p "++__i;";
	    endBloc;
	    pl "if (!__b)";
	    startBloc;
	    write_prog pElse;
	    endBloc;
	    pl "else";
	    pl "return;";
	  ]
      | LETIN(s,e1,prog) -> 
	begin
	  map_dest
	    [
	      startBloc;
	      p ((" int ")^s^(" = "));
	      write_expr e1; 
	      pl ";";
		  write_prog prog;
	      endBloc
	    ]
	end
      | Action_c(assignment,p1) -> 
	begin
	  map_dest
	    [
	      write_assignment assignment;
	      write_prog p1
	    ]
	end
      | Action(action) -> write_action action
      | Progseq(s,prog) -> 
	map_dest
	  [
	    p s; pl ":";
	    write_prog prog
	  ]
	

and
    write_addr_var_call = function(* pour écrire l'adresse *)
      | Rel(k) -> 
	map_dest
	  [
	    p "(debut +";
	    pi k;
	    p ")";
	  ]
      |  Arg(k) -> 
	map_dest
	  [
	    p "(debut +";
	    pi k;
	    p ")";
	  ]
      |  PVar(k) -> 
	map_dest
	  [
	    p "tableau_indices_vars[";
	    pi k;
	    p "]";
	  ]
      | Abs(k) -> pi k

and
    write_addr = function (* pour écrire la valeur contenue à l'adresse ciblée  *)
      | Rel(k) -> 
	begin
	  map_dest
	    [
	      p "(";
	      p myself; 
	      p "->mem(";
	      p ((soi(k))^"+ debut");
	      p "))";
	    ]
	end
      | Arg(k) -> 
	begin
	  map_dest
	    [
	      p "(";
	      p "t_valeurs_args[";
	      pi k;
	      p "]";
	      p ")"
	    ]
	end
      | PVar(k) ->
	begin
	  map_dest
	    [
	      p "(";
	      p myself; 
	      p "->mem(t_indices_vars[";
	      pi k;
	      p "]";
	      p "))";
	    ]
	end
      | _ -> raise (Generic_Error("Bug : une adresse absolue a été utilisée trop bas dans la compilation"))

and
    write_decl_prog = function
      | DecProg(sName,addr_string_l,prog) -> 
	let rec aux = function
	  | [] -> default_dest
	  | (a,s)::t -> 
	    (
	      map_dest
		[
		  p "if ";
		  p "(";
		  write_addr a;
		  pl ")";
		  startBloc;
		  p "goto ";
		  p s;
		  p ";";
		  endBloc;
		  p "else";
		  startBloc;
		  aux(t);
		  endBloc;
		]
	    )
	in
	begin
	  map_dest
	    [
	      pl "extern \"C\"";
	      startBloc;
	      p "void ";
	      p sName;
	      pl "(Cell* cell, Env& env, int * t_indices_vars, int * t_valeurs_args, int debut)";
	      startBloc;
	      aux(addr_string_l);
	      write_prog prog;
	      p "\nreturn ;";
	      endBloc;
	      endBloc
	    ]
	     end	  



and
    find_main s = function
      | [] -> raise Not_found
      | h::t -> h
(* grosse fonction *)

	
and write_tableaux var_list arg_list = 
map_dest
[
  pl "extern \"C\"";
  startBloc;
  p "int tableau_valeurs_args[] = {";
    (let rec aux = function
      | [] -> default_dest
      | [h] -> write_expr h
      | h::t ->  concat_dest (map_dest ([write_expr h; p ","])) (aux(t))
     in aux arg_list);
  pl "};\n";
  endBloc;
  pn;
  pn;  
  pl "extern \"C\"";
  startBloc;
  p "int tableau_indices_vars[] = {";
   (let rec aux n = function
      | [] -> default_dest
      | [h] -> pi n
      | h::t ->  concat_dest (map_dest ([pi n; p ","])) (aux (n + 1) t)
    in aux 0 var_list);
  p "};\n";
  endBloc;
  pn;
]

and
    transform = function
      | ADN(prog_list,sMain,var_list,arg_list,size) -> 
   (map_dest
      [
	write_header();
(*	map_dest
	  (  List.map (function DecProg(x,_,_) -> pl ("void* "^(x)^"(Cell* cell, Env& env, int * t_indices_vars, int * t_valeurs_args, int debut);\n")) prog_list)
	;*)
	write_code_size size;
	write_nom_main sMain;
	declare_debut (List.length var_list);
	write_mem_init (*List.length(var_list)*) (List.fold_right (fun x y -> match x with | DecProg(name, _, _) -> max (Anastat.mem_size name) y) prog_list(*sMain*) 0) (var_list);
	List.fold_right (fun x y -> concat_dest (concat_dest (write_decl_prog x) pn) y) prog_list default_dest;
	write_tableaux var_list arg_list;
	(*write_mem_init (size) var_list  (* n'importe quoi? *)*)
      ]
   )
