%{
  open Ast
  let built_in = ["E_init";"E_sun";"E_death";"Age_death";"age";"energy";"gen"]
  let is_built_in x = List.mem x built_in (* pour les constantes *)
%}
  

%token <int> INT  
%token <string> IDENT
%token <string> WORD
%token IF THEN ELSE
%token LT GT LE GE EQ NEQ MOD PLUS MINUS TIMES DIV CONC
%token LPAREN RPAREN LBRACKET RBRACKET COMMA COLON SEMICOLON DOT LBRACE RBRACE AROBASE BEGIN END
%token AND OR NOT BOR BAND
%token SEE DIR_MESSAGE 
%token MOVE SEND_MESSAGE SEND_ENERGY DUPLICATE SKIP EXISTS
%token LET IN SUCC REF ARGS VARS
%token EOL

%nonassoc IN SEMICOLON /*on raccroche le plus long programme après un let .. in ou une assignation*/
%right SUCC /*pour spécifier (peut-être qu'on pourrait le faire en assoc) l'ordre de construction des seq et peut-être plus ? */

/*Opérateurs booléens */
%left AND
%left OR 
%left BOR
%left BAND
%left LT 
%left GT
%left LE
%left GE
%left EQ
%left NEQ
%left CONC
%left ASSIGN
%nonassoc NOT


%left PLUS MINUS
%nonassoc PLUS_R
%left DIV MOD
%left TIMES
%nonassoc SMINUS
/* 
Pour éviter le dangling else
*/
%nonassoc THEN
%nonassoc ELSE
%nonassoc LBRACKET /*RBRACKET*/
%nonassoc LBRACE
%nonassoc BEGIN

/* "start" signale le point d'entrée */
%start main             
/* on _doit_ donner le type du point d'entrée */
%type <Ast.adn> main   

%%
/* --- début des règles de grammaire --- */
  
  main: 
			 | def_progs AROBASE IDENT LPAREN expr_list RPAREN LPAREN expr_list RPAREN  { ADN($1,$3,$5,$8) }
  ;


  def_prog:
| IDENT COLON  VARS def_var ARGS def_var prog SEMICOLON SEMICOLON  {DecProg($1,$4,$6,$7)}
;

def_progs:
| def_prog {[$1]}
| def_prog def_progs {($1::$2)}
;


assign:
| IDENT COLON EQ expr {Assign($1,$4)}

/* Pas encore utilisées */
/*
assignliste:
| /* Empty  * / { [] }
| assign COMMA assignliste {($1::$3)}
*/

/*
  non_empty_def_prog_list:
| def_prog {[$1]}
| def_prog SEMICOLON non_empty_def_prog_list {($1::$3)} 
  ;
*/  
  def_var:
| LPAREN RPAREN            {[]} /* plus intuitif à écrire */
| LPAREN IDENT RPAREN      {[Var($2)]}
| LPAREN IDENT COMMA def_var_list RPAREN {(Var($2)::$4)}
  ;
  def_var_list:
| IDENT {[Var($1)]}
| IDENT COMMA def_var_list {(Var($1)::$3)}
  ;
  

  prog:
| LBRACE prog RBRACE {$2}
| BEGIN prog END {$2}
| LPAREN prog RPAREN {$2}
| IF cond THEN prog ELSE prog {IFTE($2,$4,$6)}
| LET IDENT EQ expr IN prog {LETIN($2,$4,$6)}
| IF EXISTS LPAREN IDENT COMMA IDENT COMMA cond  RPAREN THEN prog ELSE prog {IFTE_M($4,$6,$8,$11,$13)}
| action {Action($1)}
| assign SEMICOLON prog {Action_c($1,$3)}
| prog_seq {Seq($1)}
 ;
 
prog_seq:
| prog SUCC prog     {[$1;$3]}/* final */

  action:
| MOVE expr {Move($2)}
| SEND_MESSAGE LPAREN expr COMMA expr  RPAREN {Send_M($3,$5)}
| SEND_ENERGY LPAREN expr COMMA expr RPAREN {Send_E($3,$5)}
| DUPLICATE LPAREN expr COMMA expr COMMA IDENT LPAREN expr_list RPAREN LPAREN expr_list  RPAREN RPAREN {Duplicate($3,$5,$7,$9,$12)}
| IDENT LPAREN expr_addr_list RPAREN LPAREN expr_list RPAREN {Call($1,$3,$6)}
| SKIP {Skip}
  ;

  cond:
expr    {$1}
  

    expr:
| LPAREN expr RPAREN {$2}
| IDENT {if (is_built_in($1)) then Const($1) else Var($1)} /*où is_built_in est une fonction qui détermine si la variable est définie dans le programme où si c'est une variable en lecture seule*/
| INT   {Int($1)}
| expr OR expr    {Op_expr(Or,$1,$3)}
| expr AND expr   {Op_expr(And,$1,$3)}
| expr BOR expr   {Op_expr(Bor,$1,$3)}
| expr BAND expr  {Op_expr(Band,$1,$3)}
| expr LT expr    {Op_expr(Low,$1,$3)}
| expr GT expr    {Op_expr(Greater,$1,$3)}
| expr LE expr    {Op_expr(Leq,$1,$3)}
| expr GE expr    {Op_expr(Geq,$1,$3)}
| expr EQ expr    {Op_expr(Eq,$1,$3)}
| expr NEQ expr   {Op_expr(Neq,$1,$3)}
| expr MOD expr   {Op_expr(Mod,$1,$3)}
| expr PLUS expr  {Op_expr(Plus,$1,$3)}
| expr MINUS expr {Op_expr(Minus,$1,$3)}
| expr TIMES expr {Op_expr(Times,$1,$3)}
| expr DIV expr   {Op_expr(Div,$1,$3)}
| NOT expr        {Not($2)}
| MINUS expr %prec SMINUS {Op_expr(Minus,Int(0),$2)}
| IDENT LPAREN expr_list RPAREN {Function($1,$3)}
| DIR_MESSAGE LPAREN IDENT  COMMA cond  RPAREN {DIR_MESS($3,$5)}
| SEE LPAREN RPAREN {SEE}
| expr LBRACKET expr COMMA expr RBRACKET {Sub_expr($1,$3,$5)}
| expr LBRACKET expr RBRACKET {Sub_expr($1,$3,$3)}
| CONC LPAREN non_empty_conc_list RPAREN  {Concat($3)}
| CONC LPAREN RPAREN {raise(Ast.Generic_Error("You cannot concatenate an empty set of messages, come on..."))}
  ;

non_empty_conc_list:
| LPAREN expr COMMA INT RPAREN  {[($2,$4)]}
| LPAREN expr COMMA INT RPAREN COMMA non_empty_conc_list {($2,$4)::$7}

expr_addr:
| expr              {Exp($1)}
| REF IDENT         {Addr(Var($2))}  

expr_list: 
| /* Empty */    {[]}
| non_empty_expr_list   {$1}

non_empty_expr_list:
| expr           {[$1]}
| expr COMMA non_empty_expr_list  {$1::$3}

expr_addr_list:
| /* Empty */    {[]}
| non_empty_expr_addr_list {$1}

non_empty_expr_addr_list:
| expr_addr {[$1]}
| expr_addr COMMA non_empty_expr_addr_list {$1::$3}


